<?php
Route::post('auth', 'Auth\AuthController@login');

Route::middleware(['jwt.auth'])->group(static function () {

	Route::prefix('auth')->namespace('Auth')->group(static function () {
		Route::get('', 'AuthController@verify');
		Route::patch('', 'AuthController@refresh');
		Route::delete('', 'AuthController@destroy');
	});

	Route::prefix('planner')->namespace('Planner')->group(static function () {
		Route::get('', 'PlannerController@index');
		Route::put('', 'PlannerController@store');
		Route::patch('', 'PlannerController@update');
		Route::get('resources', 'PlannerController@resources');
		Route::get('options', 'PlannerController@options');
		Route::prefix('users')->group(static function () {
			Route::post('', 'PlannerUserController@index');
		});
		Route::prefix('scheduler/{schedule}')->group(static function () {
			Route::get('', 'PlannerSchedulerController@index');
			Route::patch('', 'PlannerSchedulerController@update');
			Route::delete('', 'PlannerSchedulerController@delete');
		});
		Route::prefix('settings')->group(static function () {
			Route::get('', 'PlannerSettingsController@index');
			Route::patch('', 'PlannerSettingsController@update');
		});
	});

	Route::prefix('patients')->namespace('Patient')->group(static function () {
		Route::get('', 'PatientController@index');
		Route::get('create', 'PatientController@create');
		Route::post('', 'PatientController@store');
		Route::prefix('{patient}')->group(static function () {
			Route::get('', 'PatientDetailController@index');
			Route::patch('', 'PatientDetailController@update');
			Route::prefix('settings')->group(static function () {
				Route::delete('', 'PatientSettingsController@destroy');
				Route::get('', 'PatientSettingsController@index');
				Route::patch('', 'PatientSettingsController@update');
			});
			Route::prefix('dashboard')->namespace('Dashboard')->group(static function () {
				Route::get('appointments', 'PatientDashboardAppointmentController@index');
			});
			Route::prefix('tests')->namespace('Tests')->group(static function () {
				Route::get('', 'PatientTestsController@show');
				Route::post('', 'PatientTestsController@store');
				Route::get('available', 'PatientTestsController@availableTests');
				Route::prefix('{uuid2}')->group(static function () {
					Route::delete('', 'PatientTestsDetailsController@destroy');
					Route::get('', 'PatientTestsDetailsController@index');
					Route::patch('', 'PatientTestsDetailsController@update');
					Route::patch('approve', 'PatientTestsDetailsController@approve');
				});
			});
			
			Route::prefix('header')->namespace('Header')->group(static function () {
				Route::prefix('payments')->group(static function () {
					Route::get('{uuid3}', 'HeaderPaymentController@index');
					Route::post('', 'HeaderPaymentController@store');
				});
			});

			Route::prefix('invoices')->namespace('Invoices')->group(static function () {
				Route::get('', 'PatientInvoiceController@index');
				Route::post('', 'PatientInvoiceController@store');
				Route::prefix('{invoice}')->group(static function () {
					Route::get('', 'PatientInvoiceDetailController@show');
					Route::patch('', 'PatientInvoiceDetailController@update');
					Route::prefix('methods')->group(static function () {
						Route::get('', 'PatientInvoiceMethodController@show');
						Route::post('', 'PatientInvoiceMethodController@store');
						Route::patch('', 'PatientInvoiceMethodController@update');
						Route::prefix('{uuid3}')->group(static function () {
							Route::delete('', 'PatientInvoiceMethodModalController@destroy');
							Route::get('', 'PatientInvoiceMethodModalController@show');
							Route::post('', 'PatientInvoiceMethodModalController@store');
							Route::patch('', 'PatientInvoiceMethodModalController@update');
							Route::get('pdf', 'PatientInvoiceMethodModalController@print');
						});
					});
				});
			});
		});
	});

	Route::prefix('dashboard')->group(static function () {
		Route::patch('appointment/{appointment}', 'DashboardController@updateAppointmentStatus');
		Route::get('appointment', 'DashboardController@appointments');
		Route::post('statistics', 'DashboardController@getStatsWidget');
	});

	Route::prefix('reports')->namespace('Reports')->group(static function () {
		Route::prefix('income')->namespace('Income')->group(static function () {
			Route::get('', 'ReportsIncomeSidebarController@index');
			Route::get('users', 'ReportsIncomeUsersController@index');
			Route::get('rooms', 'ReportsIncomeRoomsController@index');
		});

		Route::prefix('workload')->namespace('Workload')->group(static function () {
			Route::get('', 'ReportsWorkloadSidebarController@index');
			Route::get('users', 'ReportsWorkloadUsersController@index');
			Route::get('rooms', 'ReportsWorkloadRoomsController@index');
		});
	});

	Route::prefix('settings')->namespace('Settings')->group(static function () {
		Route::prefix('branch')->namespace('Branch')->group(static function () {
			Route::get('', 'BranchController@index');
			Route::post('', 'BranchController@store');
			Route::prefix('{branch}')->group(static function () {
				Route::get('', 'BranchDetailController@show');
				Route::patch('', 'BranchDetailController@update');
				Route::prefix('working_hours')->group(static function () {
					Route::get('', 'BranchWorkingTimeController@index');
					Route::patch('', 'BranchWorkingTimeController@update');
				});
				Route::prefix('rooms')->group(static function () {
					Route::get('', 'BranchRoomController@index');
					Route::patch('order', 'BranchRoomController@order');
					Route::post('', 'BranchRoomController@store');
					Route::prefix('{uuid}')->group(static function () {
						Route::delete('', 'BranchRoomController@destroy');
						Route::get('', 'BranchRoomController@view');
						Route::patch('', 'BranchRoomController@update');
					});
				});
				Route::prefix('settings')->group(static function () {
					Route::get('', 'BranchSettingsController@index');
					Route::patch('', 'BranchSettingsController@update');
					Route::delete('', 'BranchSettingsController@destroy');
				});
			});
		});
		Route::prefix('users')->namespace('Users')->group(static function () {
			Route::get('', 'UserController@index');
			Route::post('', 'UserController@store');
			Route::prefix('{user}')->group(static function () {
				Route::get('', 'UserDetailController@show');
				Route::patch('', 'UserDetailController@update');
				Route::prefix('working_time')->group(static function () {
					Route::get('', 'UserWorkingTimeController@show');
					Route::patch('', 'UserWorkingTimeController@update');
				});
				Route::prefix('settings')->group(static function () {
					Route::get('', 'UserSettingsController@show');
					Route::patch('', 'UserSettingsController@update');
					Route::delete('', 'UserSettingsController@destroy');
				});
			});
		});
	});

	Route::middleware(['admin'])->prefix('admin')->namespace('Admin')->group(static function () {
		Route::prefix('company')->namespace('AdminCompanies')->group(static function () {
			Route::get('', 'AdminCompanyController@index');
		});

		Route::prefix('users')->namespace('AdminUsers')->group(static function () {
			Route::get('', 'AdminUserController@index');
			Route::prefix('{user}')->group(static function () {
				Route::get('', 'AdminUserDetailController@show');
				Route::patch('', 'AdminUserDetailController@update');
				Route::prefix('settings')->group(static function () {
					Route::get('', 'AdminUserSettingsController@show');
					Route::patch('', 'AdminUserSettingsController@update');
				});
			});
		});
	});
});

