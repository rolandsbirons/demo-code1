const path = require('path');

const isProduction = process.env.NODE_ENV === 'production';
/*eslint-disable */
module.exports = {
	devServer          : {
		proxy           : 'https://test.rb.coco',
		disableHostCheck: true,
	},
	css                : {
		loaderOptions: {
			sass: {
				implementation: require('dart-sass')
			}
		}
	},
	lintOnSave         : isProduction,
	productionSourceMap: false,
	outputDir          : path.join(__dirname, '../public'),
	indexPath          : isProduction
		? '../resources/views/index.blade.php'
		: 'index.html',
	configureWebpack   : {
		performance : {
			hints            : false,
			maxEntrypointSize: 512000,
			maxAssetSize     : 512000
		},
		optimization: {
			minimize   : isProduction,
			splitChunks: {
				minSize: 10000,
				maxSize: 220000,
			}
		},
		plugins     : [],
		resolve     : {
			alias: {
				'~': path.resolve(__dirname, 'src/'),
				'@': path.resolve(__dirname, 'src/scss/')
			}
		},
	}
};
/*eslint-enable */
