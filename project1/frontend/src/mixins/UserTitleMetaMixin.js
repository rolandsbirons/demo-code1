import { mapGetters } from 'vuex';
import { defaultTo } from 'lodash-es';

//
export default {
	computed: {
		...mapGetters({
			getProfileMetaTitle: 'profile/getCurrentActiveTitle',
		}),
	},
	methods : {
		setProfileMetaTitle(title) {
			this.$store.commit('profile/UPDATED_PROFILE_TITLE', defaultTo(title, null));
		},
		clearProfileMetaTitle() {
			this.$store.commit('profile/UPDATED_PROFILE_TITLE');
		}
	}
}