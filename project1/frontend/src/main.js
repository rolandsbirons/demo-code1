import './style/svg-icons';
import * as svgicon from 'vue-svgicon';
import App from './App';
import auth from './config/auth';
import axios from 'axios';
import COLORS from './config/global/colors';
import Echo from 'laravel-echo';
import lang from 'element-ui/lib/locale/lang/lv';
import LANG_CONFIG from './config/language/langConfig';
import LANGUAGE_EN from './config/language/en';
import LANGUAGE_LV from './config/language/lv';
import locale from 'element-ui/lib/locale';
import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';
import router from './config/router';
import store from './config/store';
import vsNoti from './components/vsNoti/vsNoti.js';
import Vue from 'vue';
import VueAuth from '@websanova/vue-auth';
import VueAxios from 'vue-axios';
import VueI18n from 'vue-i18n';
import vuescroll from 'vuescroll';
import { Loading as ElementLoading } from 'element-ui';

Vue.router = router;
Vue.use(VueAxios, axios);
Vue.use(VueAuth, auth);
Vue.use(VueI18n);
Vue.use(ElementLoading);
Vue.use(svgicon);
Vue.use(vuescroll);
Vue.prototype.$eventHub = new Vue(); // Global event bus
Vue.prototype.$loading = ElementLoading.service;

Vue.prototype.$vuescrollConfig = {
	mode: 'pure-native',
	rail: {
		background: COLORS.SCROLLBAR,
	},
	bar : {
		background: COLORS.SCROLLBAR,
	}
};

Vue.prototype.$url = (str, replacement) => {

	if (replacement) {
		Object.keys(replacement).forEach(key => {
			str = str.replace(`{${key}}`, replacement[key])
		});
	}

	return '/api/' + str;
};


locale.use(lang);

/*eslint-disable */
axios.interceptors.response.use(
	response => response,
	(error) => {
		if (error.response.status >= 500) {
			Raven.captureException(error);
		}

		return Promise.reject(error);
	},
);
/*eslint-enable */

Raven.config(process.env.MIX_SENTRY_DSN_PUBLIC)
	.addPlugin(RavenVue, Vue)
	.setEnvironment(process.env.BUILD_ENV)
	.install();

window.io = require('socket.io-client');
const nodeEnv = process.env.BUILD_ENV;
const authToken = localStorage.getItem('auth_token');

Vue.prototype.$echo = new Echo({
	broadcaster: 'socket.io',
	auth       : {
		headers: {
			'Authorization': 'Bearer ' + authToken,
		}
	},
});

if (nodeEnv === 'production') {
	(function (h, o, t, j, a, r) {
		h.hj = h.hj || function () {
			(h.hj.q = h.hj.q || []).push(arguments)
		};
		h._hjSettings = {hjid: 1262988, hjsv: 6};
		a = o.getElementsByTagName('head')[0];
		r = o.createElement('script');
		r.async = 1;
		r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
		a.appendChild(r);
	})(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
}

const i18n = new VueI18n({
	locale  : LANG_CONFIG.DEFAULT_LANG,
	messages: {
		en: LANGUAGE_EN,
		lv: LANGUAGE_LV
	}
});

//initialization
new Vue({
	el    : '#app',
	render: (h) => {
		return h(App);
	},
	i18n,
	store,
	router
}).$mount('#root');
