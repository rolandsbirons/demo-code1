const isFloatNumberKey = ($event) => {
	let keyCode = ($event.keyCode ? $event.keyCode : $event.which);

	if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) {
		$event.preventDefault();
	}
};

const isNumberKey = ($event) => {
	let keyCode = ($event.keyCode ? $event.keyCode : $event.which);

	if ((keyCode < 48 || keyCode > 57)) {
		$event.preventDefault();
	}
};

export {
	isNumberKey,
	isFloatNumberKey,
}