import { forEach, has, isNil, isNumber } from 'lodash-es';

const calculateTotalItems = (items) => {
	let total = 0.00;

	forEach(items, (item) => {
		if (has(item, 'discount')) {
			total += parseFloat(getServicePriceWithDiscount(item.price, item.discount));
		} else {
			total += parseFloat(item.price);
		}
	});

	return floatPrice(total);
};

const getServicePriceWithDiscount = (price, discount = null) => {
	let total = 0;

	if (!isNil(price)) {
		let serviceDiscount = 0;

		if (!isNil(discount)) {
			serviceDiscount = parseInt(discount);
		}

		total = parseFloat(getPositiveNegativeDiscount(price, serviceDiscount))
	}

	let fixedTotal = floatPrice(total);

	if (fixedTotal === '-0.00') {
		fixedTotal = parseFloat('0.00').toFixed(2);
	}

	return fixedTotal;
};

const getPositiveNegativeDiscount = (total, serviceDiscount) => {
	const discount = (total / 100) * serviceDiscount;

	if (serviceDiscount < 0) {
		total = total + parseFloat(discount);
	}

	if (serviceDiscount > 100) {
		serviceDiscount = 100;
	}

	if (serviceDiscount > 0) {
		total += discount;
	}

	return total;
};

const floatPrice = (price) => {
	return parseFloat(price).toFixed(2)
};

const hasStrictDiscount = (service) => {
	return service.discount_strict && isNumber(service.discount_strict);
};

export {
	calculateTotalItems,
	floatPrice,
	getPositiveNegativeDiscount,
	getServicePriceWithDiscount,
	hasStrictDiscount,
}