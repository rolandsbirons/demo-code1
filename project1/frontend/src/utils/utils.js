import { forEach, merge } from 'lodash-es';

const filterWithKeys = (object, functionCheck) => {
	let result = {};

	forEach(object, (objectValue, objectIndex) => {
		if (functionCheck(objectValue)) {
			merge(result, {
				[objectIndex]: objectValue
			});
		}
	});

	return result;
};

export {
	filterWithKeys,
}