#!/usr/bin/env bash

for ARGUMENT in "$@"
do
    KEY=$(echo ${ARGUMENT} | cut -f1 -d=)
    VALUE=$(echo ${ARGUMENT} | cut -f2 -d=)

    case "$KEY" in
            --env)          PROJECT_ENV=${VALUE} ;;
            *)
    esac
done

generate_env() {
    if [[ ${PROJECT_ENV} == "development" ]] || [[ ${PROJECT_ENV} == "template" ]]
    then
        echo 'APP_NAME='${APP_NAME} >> .env
        echo 'APP_KEY='${DEV_APP_KEY} >> .env
        echo 'DB_HOST='${DEV_DB_HOST} >> .env
        echo 'DB_USERNAME='${DEV_DB_USER} >> .env
        echo 'DB_PASSWORD='${DEV_DB_PASSWORD} >> .env
        echo 'REDIS_HOST='${DEV_REDIS_HOST} >> .env
        echo 'JWT_SECRET='${DEV_JWT_SECRET} >> .env

        echo 'LARAVEL_ECHO_SERVER_DEBUG=true' >> .env
        echo 'LARAVEL_ECHO_SERVER_REDIS_HOST='${DEV_REDIS_HOST} >> .env
    fi

    if [[ ${PROJECT_ENV} == "development" ]]
    then
        echo 'APP_ENV=development'>> .env
        echo 'DB_DATABASE='${DEV_DB_DATABASE} >> .env
        echo 'REDIS_PREFIX=dev' >> .env
    fi

    if [[ ${PROJECT_ENV} == "template" ]]
    then
        echo 'APP_ENV=template'>> .env
        echo 'DB_DATABASE='${TEMPLATE_DB_DATABASE} >> .env
        echo 'REDIS_PREFIX=tem' >> .env
    fi

    if [[ ${PROJECT_ENV} == "master" ]]
    then
        echo 'APP_NAME='${APP_NAME} >> .env
        echo 'APP_ENV=production'>> .env
        echo 'APP_KEY='${PRODUCTION_APP_KEY} >> .env
        echo 'JWT_SECRET='${PRODUCTION_JWT_SECRET} >> .env
        echo 'DB_HOST='${PRODUCTION_DB_HOST} >> .env
        echo 'DB_DATABASE='${PRODUCTION_DB_DATABASE} >> .env
        echo 'DB_USERNAME='${PRODUCTION_DB_USER} >> .env
        echo 'DB_PASSWORD='${PRODUCTION_DB_PASSWORD} >> .env
        echo 'TEMPLATE_DB_HOST='${DEV_DB_HOST} >> .env
        echo 'TEMPLATE_DB_DATABASE='${DEV_DB_DATABASE} >> .env
        echo 'TEMPLATE_DB_USERNAME='${DEV_DB_USER} >> .env
        echo 'TEMPLATE_DB_PASSWORD='${DEV_DB_PASSWORD} >> .env
        echo 'REDIS_HOST='${PRODUCTION_REDIS_HOST} >> .env
        echo 'MAIL_HOST='${PRODUCTION_MAIL_HOST} >> .env
        echo 'MAIL_USERNAME='${PRODUCTION_MAIL_USERNAME} >> .env
        echo 'MAIL_PASSWORD='${PRODUCTION_MAIL_PASSWORD} >> .env

        echo 'LARAVEL_ECHO_SERVER_REDIS_HOST='${PRODUCTION_REDIS_HOST} >> .env
    fi

    echo 'LOG_CHANNEL=stack' >> .env
#    echo 'APP_DEBUG=false' >> .env
    echo 'APP_URL=http://localhost' >> .env
    echo 'SENTRY_DSN='${PRODUCTION_SENTRY_DSN} >> .env

    echo 'DEBUG_ADMINS='${DEBUG_ADMINS} >> .env

    echo 'LARAVEL_ECHO_SERVER_HOST=localhost' >> .env
    echo 'LARAVEL_ECHO_SERVER_PORT=8833' >> .env
    echo 'LARAVEL_ECHO_SERVER_AUTH_HOST=http://localhost' >> .env
    
    echo 'PUSHER_APP_ID=a7431a4f32a0daa2' >> .env
    echo 'PUSHER_APP_KEY=514b51df13acec7d89b80a3d9b5870f6' >> .env

    echo 'LOG_SLACK_WEBHOOK_URL='${LOG_SLACK_WEBHOOK_URL} >> .env
}

generate_env