#!/usr/bin/env bash

for ARGUMENT in "$@"
do
    KEY=$(echo ${ARGUMENT} | cut -f1 -d=)
    VALUE=$(echo ${ARGUMENT} | cut -f2 -d=)

    case "$KEY" in
            --env)          PROJECT_ENV=${VALUE} ;;
            *)
    esac
done

generate_docker() {
    if [[ ${PROJECT_ENV} == "master" ]]
    then
        PROJECT_ENV='production'
    fi

    echo "Creating docker image ${PROJECT_ENV}"

    docker build . -t core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER} -t core-${PROJECT_ENV}:latest --build-arg DOCKER_ENV_CONFIG=${PROJECT_ENV} --build-arg CACHEBUST=${DRONE_BUILD_NUMBER}
    docker tag  core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER} ${REGISTRY_ENDPOINT}/core-${PROJECT_ENV}:latest

    docker push ${REGISTRY_ENDPOINT}/core-${PROJECT_ENV}:latest
    docker tag  core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER} ${REGISTRY_ENDPOINT}/core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER}

    docker push ${REGISTRY_ENDPOINT}/core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER}
    docker rmi core-${PROJECT_ENV}:${DRONE_BUILD_NUMBER}
}

generate_docker