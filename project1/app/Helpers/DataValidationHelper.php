<?php

namespace App\Helpers;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;

class DataValidationHelper
{
	public static function validatePersonalCode($personalCode): bool
	{
		$personalCode = preg_replace('/\D/', '', $personalCode);

		//Bypass if we don't know second half
		if (self::byPassPersonalCodeValidation($personalCode)) {
			return true;
		}

		if (strlen($personalCode) !== 11) {
			return false;
		}

		$calc = 1 * $personalCode[0] + 6 * $personalCode[1] + 3 * $personalCode[2] + 7 * $personalCode[3] + 9 * $personalCode[4] + 10 * $personalCode[5] + 5 * $personalCode[6] + 8 * $personalCode[7] + 4 * $personalCode[8] + 2 * $personalCode[9];
		$checksum = (1101 - $calc) % 11;

		return $checksum === (int)$personalCode[10];
	}

	private static function byPassPersonalCodeValidation($personalCode): bool
	{
		return Str::endsWith($personalCode, ['00000']);
	}

	private static function guessPersonalCodeYear($personalCode): ?int
	{
		$personalYearCentury = (int)substr($personalCode, 6, 1);
		$personalYearShort = (int)substr($personalCode, 4, 2);
		$yearPrefix = null;

		//Hackish guess function
		if ($personalYearCentury === 0 && self::byPassPersonalCodeValidation($personalCode)) {
			$centuryLeapYear = (int)Carbon::now()->addYear()->format('y');

			return $personalYearShort < $centuryLeapYear ? 20 : 19;
		}

		if ($personalYearCentury === 0) {
			$yearPrefix = 18;
		} else if ($personalYearCentury === 1) {
			$yearPrefix = 19;
		} else if ($personalYearCentury === 2) {
			$yearPrefix = 20;
		} else {
			$yearPrefix = null;
		}

		return $yearPrefix;
	}

	public static function personalCodeGetCarbonDate($personalCode): ?Carbon
	{
		try {
			$personalCode = str_replace('-', '', $personalCode);

			if (self::validatePersonalCode($personalCode)) {
				$personDate = (int)substr($personalCode, 0, 2);
				$personMonth = (int)substr($personalCode, 2, 2);
				$personalYearShort = (int)substr($personalCode, 4, 2);

				$personalYearShort = str_pad($personalYearShort, 2, 0, STR_PAD_LEFT);

				//Ignore new LV personal codes (31 for LV, 8000 for other)
				if ($personDate > 31) {
					return null;
				}

				$yearPrefix = self::guessPersonalCodeYear($personalCode);

				$fullYear = $yearPrefix . '' . $personalYearShort;

				return Carbon::parse($fullYear . '-' . $personMonth . '-' . $personDate);
			}

			return null;
		} catch (Exception $e) {
			return null;
		}
	}
}
