<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spiritix\LadaCache\Database\LadaCacheTrait;

abstract class DenticModel extends Model
{
	use LadaCacheTrait;

	protected function asJson($value)
	{
		return json_encode($value, JSON_UNESCAPED_UNICODE);
	}
}