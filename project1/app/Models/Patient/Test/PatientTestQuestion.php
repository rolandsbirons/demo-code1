<?php

namespace App\Models\Patient\Test;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Patient\PatientTestQuestion
 *
 * @property int                                                                                     $id
 * @property string                                                                                  $uuid
 * @property int                                                                                     $company_id
 * @property int                                                                                     $question_category
 * @property int                                                                                     $test_type
 * @property array|null                                                                              $test_value
 * @property string|null                                                                             $comment
 * @property string|null                                                                             $trigger
 * @property int                                                                                     $parent_id
 * @property int                                                                                     $order
 * @property \Illuminate\Support\Carbon|null                                                         $created_at
 * @property \Illuminate\Support\Carbon|null                                                         $updated_at
 * @property string|null                                                                             $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]     $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTestQuestion[] $parent
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion isParent($parentId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion notParent()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestQuestion onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion ordered()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereQuestionCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereTestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereTestValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereTrigger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestQuestion withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestQuestion withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read int|null $activities_count
 * @property-read int|null $parent_count
 */
class PatientTestQuestion extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity, HasTranslations;

	public $translatable = ['test_value'];

	protected $guarded = ['id', 'uuid'];

	protected $table = 'patient_test_questions';

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function parent()
	{
		return $this->hasMany(PatientTestQuestion::class, 'parent_id', 'id')->ordered();
	}

	public function scopeOrdered($query)
	{
		return $query->orderBy('order', 'ASC');
	}

	public function scopeNotParent($query): void
	{
		$query->where('parent_id', 0);
	}

	public function scopeIsParent($query, $parentId): void
	{
		$query->where('parent_id', $parentId);
	}

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
