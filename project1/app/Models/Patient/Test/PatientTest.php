<?php

namespace App\Models\Patient\Test;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientTests
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int                                                                                 $company_id
 * @property int                                                                                 $patient_id
 * @property int                                                                                 $category_id
 * @property \Illuminate\Support\Carbon|null                                                     $approved_at
 * @property string|null                                                                         $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Patient\PatientTestAnswer                                          $answers
 * @property-read \App\Models\Patient\PatientTestCategory                                        $question
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTest onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTest withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTest withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read int|null $answers_count
 */
class PatientTest extends DenticModel
{
	use UuidTrait, LogsActivity, SoftDeletes;

	protected $guarded = ['id', 'uuid'];

	protected $casts = [
		'approved_at' => 'datetime',
	];

	protected static $logFillable = true;

	public function question()
	{
		return $this->belongsTo(PatientTestCategory::class, 'category_id')->withTrashed();
	}

	public function answers(): HasMany
	{
		return $this->hasMany(PatientTestAnswer::class, 'test_id', 'id');
	}
}
