<?php

namespace App\Models\Patient\Test;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Patient\PatientTestCategory
 *
 * @property int                                                                                     $id
 * @property string                                                                                  $uuid
 * @property string|null                                                                             $color
 * @property int                                                                                     $company_id
 * @property array|null                                                                              $name
 * @property \Illuminate\Support\Carbon|null                                                         $created_at
 * @property \Illuminate\Support\Carbon|null                                                         $updated_at
 * @property string|null                                                                             $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]     $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTestQuestion[] $questions
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestCategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestCategory withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTestQuestion[] $questionsAll
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestCategory query()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTestQuestion[] $questionsCategory
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read int|null $activities_count
 * @property-read int|null $questions_count
 * @property-read int|null $questions_all_count
 * @property-read int|null $questions_category_count
 */
class PatientTestCategory extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity, HasTranslations;

	public $translatable = ['name'];

	protected $table = 'patient_test_categories';

	protected $guarded = ['id', 'uuid'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function questions()
	{
		return $this->hasMany(PatientTestQuestion::class, 'question_category')->ordered();
	}

	public function questionsAll()
	{
		//->withTrashed()->notParent()
		return $this->hasMany(PatientTestQuestion::class, 'question_category')->ordered();
	}

	public function questionsParent($parent)
	{
		return $this->hasMany(PatientTestQuestion::class, 'question_category')->isParent($parent)->ordered();
	}

	public function questionsCategory()
	{
		return $this->hasMany(PatientTestQuestion::class, 'question_category')->notParent()->ordered();
	}

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
