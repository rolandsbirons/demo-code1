<?php

namespace App\Models\Patient\Test;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientTestAnswer
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property string                                                                              $test_uuid
 * @property int                                                                                 $company_id
 * @property int                                                                                 $test_id
 * @property string|null                                                                         $question_answer
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestAnswer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereQuestionAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereTestUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestAnswer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTestAnswer withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer query()
 * @property int $question_id
 * @property-read \App\Models\Patient\PatientTestQuestion $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTestAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class PatientTestAnswer extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	protected $guarded = ['id'];

	public $timestamps = false;

	protected static $logFillable = true;

	public function question(): BelongsTo
	{
		return $this->belongsTo(PatientTestQuestion::class, 'question_id', 'id');
	}
}
