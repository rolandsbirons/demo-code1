<?php

namespace App\Models\Patient\Invoice;

use App\Models\DenticModel;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethService;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientInvoice
 *
 * @property int                                                                                              $id
 * @property string                                                                                           $uuid
 * @property int                                                                                              $user_id
 * @property int                                                                                              $company_id
 * @property int                                                                                              $patient_id
 * @property string|null                                                                                      $invoice_id
 * @property int                                                                                              $payment_type
 * @property bool                                                                                             $paid
 * @property \Illuminate\Support\Carbon|null                                                                  $paid_at
 * @property int                                                                                              $discount
 * @property float                                                                                            $total_services
 * @property string|null                                                                                      $comment
 * @property \Illuminate\Support\Carbon|null                                                                  $invoice_due
 * @property string|null                                                                                      $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                                  $created_at
 * @property \Illuminate\Support\Carbon|null                                                                  $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]              $activity
 * @property-read \App\Models\Patient\Patient                                                                 $patient
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothService[]         $services
 * @property-read \App\Models\User\User                                                                       $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoice onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereInvoiceDue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice wherePaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereTotalServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoice withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property int                                                                                              $locked
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientInvoiceItems[]          $items
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\Invoice\PatientInvoiceMethod[] $methods
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoice whereLocked($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read \App\Models\Patient\Invoice\PatientInvoiceMethod $method
 * @property string $template
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoice whereTemplate($value)
 * @property-read int|null $activities_count
 * @property-read int|null $items_count
 * @property-read int|null $methods_count
 * @property-read int|null $services_count
 */
class PatientInvoice extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	protected $fillable = [
		'comment',
		'discount',
		'invoice_due',
		'invoice_id',
		'locked',
		'patient_id',
		'payment_type',
		'services_total',
	];

	protected $casts = [
		'discount'     => 'integer',
		'invoice_due'  => 'datetime',
		'locked'       => 'boolean',
		'patient_id'   => 'integer',
		'payment_type' => 'integer',
	];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	public function user(): HasOne
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function patient(): HasOne
	{
		return $this->hasOne(Patient::class, 'id', 'patient_id');
	}

	public function services(): HasMany
	{
		return $this->hasMany(PatientTeethService::class, 'invoice_id');
	}

	public function items(): hasMany
	{
		return $this->hasMany(PatientInvoiceItem::class, 'invoice_id');
	}

	public function methods(): HasMany
	{
		return $this->hasMany(PatientInvoiceMethod::class, 'invoice_id');
	}

	public function method(): hasOne
	{
		return $this->hasOne(PatientInvoiceMethod::class, 'invoice_id');
	}
}
