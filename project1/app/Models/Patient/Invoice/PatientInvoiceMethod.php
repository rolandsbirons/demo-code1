<?php

namespace App\Models\Patient\Invoice;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Patient\Invoice\PatientInvoiceMethod
 *
 * @property int                             $id
 * @property string                          $uuid
 * @property int                             $company_id
 * @property int                             $patient_id
 * @property int                             $user_id
 * @property int                             $type
 * @property int                             $invoice_id
 * @property int|null                        $invoice_payment
 * @property string|null                     $invoice_sku
 * @property float                           $amount
 * @property float                           $amount_paid
 * @property int                             $status
 * @property string|null                     $description
 * @property bool                            $locked
 * @property string|null                     $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereAmountPaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereInvoicePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereInvoiceSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereLocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Invoice\PatientInvoiceMethod withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientInvoiceItems[] $items
 * @property-read int|null $items_count
 */
class PatientInvoiceMethod extends Model
{
	use SoftDeletes, UuidTrait;

	protected $fillable = [
		'type',
		'amount',
		'amount_paid',
		'description',
		'invoice_id',
		'invoice_sku',
		'invoice_payment',
		'locked',
		'status',
	];

	protected $casts = [
		'locked' => 'boolean',
	];

//	public function getAmountAttribute()
//	{
//		return $this->amount = number_format($this->amount, 2);
//	}

//	public function getAmountPaidAttribute()
//	{
//		return $this->amount_paid = number_format($this->amount_paid, 2);
//	}

	public function items(): HasMany
	{
		return $this->hasMany(PatientInvoiceItem::class, 'method_id');
	}

	public function setAmountAttribute($value): void
	{
		$this->attributes['amount'] = str_replace(',', '', number_format($value, 2));
	}

	public function setAmountPaidAttribute($value): void
	{
		$this->attributes['amount_paid'] = str_replace(',', '', number_format($value, 2));
	}

	public function getAmountAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}
}
