<?php

namespace App\Models\Patient\Invoice;

use App\Models\Patient\Teeth\PatientTeethService;
use App\Models\Service\Service;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Patient\PatientInvoiceItems
 *
 * @property int $id
 * @property string $uuid
 * @property int $type
 * @property string|null $sku
 * @property string|null $sku_custom
 * @property string|null $sku_description
 * @property int $invoice_id
 * @property int $company_id
 * @property int $user_id
 * @property int|null $service_id
 * @property string $name
 * @property string|null $description
 * @property float|null $price
 * @property float|null $price_original
 * @property float $price_insurance
 * @property int $discount
 * @property int $count
 * @property int|null $tax
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoiceItems onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems wherePriceInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems wherePriceOriginal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereSkuCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereSkuDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoiceItems withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientInvoiceItems withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $product_id
 * @property int|null $method_id
 * @property-read \App\Models\Patient\Invoice\PatientInvoiceMethod $method
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientInvoiceItems whereProductId($value)
 * @property int|null $teeth_service_id
 * @property int|null $discount_strict
 * @property-read \App\Models\Service\Service $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceItem whereDiscountStrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Invoice\PatientInvoiceItem whereTeethServiceId($value)
 * @property-read \App\Models\Patient\Teeth\PatientTeethService|null $teethService
 */
class PatientInvoiceItem extends Model
{
	use SoftDeletes, UuidTrait;

	protected $fillable = [
		'count',
		'description',
		'discount',
		'discount_strict',
		'invoice_id',
		'locked',
		'name',
		'patient_id',
		'price',
		'price_insurance',
		'price_original',
		'product_id',
		'service_id',
		'sku',
		'sku_custom',
		'sku_description',
		'tax',
		'tax',
		'teeth_service_id',
		'type',
	];

	protected $casts = [
		'locked' => 'boolean',
	];

	public function method(): HasOne
	{
		return $this->hasOne(PatientInvoiceMethod::class, 'id', 'method_id');
	}

	public function service(): BelongsTo
	{
		return $this->belongsTo(Service::class, 'service_id', 'id');
	}

	public function teethService(): BelongsTo
	{
		return $this->belongsTo(PatientTeethService::class, 'teeth_service_id', 'id');
	}

	public function getPriceAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}

	public function getPriceInsuranceAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}
}
