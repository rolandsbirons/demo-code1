<?php

namespace App\Models\Patient;

use App\Models\Appointment;
use App\Models\Branch\Branch;
use App\Models\DenticModel;
use App\Models\Patient\Invoice\PatientInvoice;
use App\Models\Patient\Teeth\PatientTooth;
use App\Models\Patient\Teeth\PatientTeethPlan;
use App\Models\Patient\Teeth\PatientTeethService;
use App\Models\Patient\Teeth\PatientTeethStatus;
use App\Models\Patient\Test\PatientTest;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\Patient
 *
 * @property int                                                                                      $id
 * @property string                                                                                   $uuid
 * @property int                                                                                      $user_id
 * @property bool                                                                                     $tooth_map_done
 * @property int                                                                                      $company_id
 * @property int                                                                                      $branch_id
 * @property string|null                                                                              $first_name
 * @property string|null                                                                              $last_name
 * @property string|null                                                                              $personal_code
 * @property string|null                                                                              $client_card
 * @property string|null                                                                              $id_number
 * @property string|null                                                                              $email
 * @property string|null                                                                              $phone
 * @property string|null                                                                              $phone2
 * @property string|null                                                                              $address
 * @property int|null                                                                                 $gender
 * @property int                                                                                      $insurance
 * @property int|null                                                                                 $job
 * @property bool                                                                                     $email_news
 * @property string|null                                                                              $dob
 * @property int                                                                                      $user_status
 * @property string|null                                                                              $description
 * @property \Illuminate\Support\Carbon|null                                                          $created_at
 * @property \Illuminate\Support\Carbon|null                                                          $updated_at
 * @property string|null                                                                              $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]      $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[]                  $appointments
 * @property-read \App\Models\Branch\Branch                                                           $branch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[]                    $doctor
 * @property-read mixed                                                                               $full_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[]                    $helpers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientInvoice[]       $invoices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothService[] $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTest[]         $tests
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothPlan[]     $toothPlan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothStatus[]   $toothStatuses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTooth[]         $tooths
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Patient onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereClientCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereEmailNews($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereIdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereInsurance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient wherePersonalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient wherePhone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereToothMapDone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereUserStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Patient withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\Patient withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Patient query()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\Branch[]                $branches
 * @property-write mixed                                                                              $fist_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[]                    $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \App\Models\Appointment                                                             $hasAppointmentsSuccessful
 * @property-read \App\Models\Patient\PatientInvoice                                                  $invoice
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read \App\Models\Appointment $appointment
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[] $assistants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\Teeth\PatientTooth[] $teeth
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\Teeth\PatientTeethStatus[] $teethStatuses
 * @property-read int|null $activities_count
 * @property-read int|null $appointments_count
 * @property-read int|null $assistants_count
 * @property-read int|null $branches_count
 * @property-read int|null $doctor_count
 * @property-read int|null $invoices_count
 * @property-read int|null $services_count
 * @property-read int|null $teeth_count
 * @property-read int|null $teeth_statuses_count
 * @property-read int|null $tests_count
 * @property-read int|null $tooth_plan_count
 * @property-read int|null $users_count
 */
class Patient extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity;

	protected $fillable = [
		'address',
		'branch_id',
		'client_card',
		'description',
		'dob',
		'email',
		'email_news',
		'first_name',
		'gender',
		'id_number',
		'insurance',
		'job',
		'last_name',
		'personal_code',
		'phone',
		'phone2',
		'user_status',
	];

	protected $casts = [
		'dob'            => 'date',
		'email_news'     => 'boolean',
		'job'            => 'integer',
		'gender'         => 'integer',
		'tooth_map_done' => 'boolean',
	];

	protected $appends = [
		'full_name',
	];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function appointments(): HasMany
	{
		return $this->hasMany(Appointment::class);
	}

	public function appointment(): HasOne
	{
		return $this->hasOne(Appointment::class);
	}

	public function hasAppointmentsSuccessful()
	{
		return $this->hasOne(Appointment::class)->where('status', '>=', 8);
	}

	public function users(): BelongsToMany
	{
		return $this->belongsToMany(User::class, 'user_patients', 'patient_id', 'user_id');
	}

	public function doctor(): BelongsToMany
	{
		return $this->users()->wherePivot('type', 1);
	}

	public function assistants(): BelongsToMany
	{
		return $this->users()->wherePivot('type', 2);
	}

	public function branches(): BelongsToMany
	{
		return $this->belongsToMany(Branch::class, 'branch_patients');
	}

	public function tests()
	{
		return $this->hasMany(PatientTest::class)->orderBy('id', 'DESC');
	}

	public function toothPlan(): HasMany
	{
		return $this->hasMany(PatientTeethPlan::class, 'patient_id');
	}

	public function teeth(): HasMany
	{
		return $this->hasMany(PatientTooth::class, 'patient_id');
	}

	public function teethStatuses(): HasMany
	{
		return $this->hasMany(PatientTeethStatus::class, 'patient_id');
	}

	public function services(): HasMany
	{
		return $this->hasMany(PatientTeethService::class, 'patient_id');
	}

	public function invoices(): HasMany
	{
		return $this->hasMany(PatientInvoice::class, 'patient_id')->latest();
	}

	public function invoice(): HasOne
	{
		return $this->hasOne(PatientInvoice::class, 'patient_id');
	}

	public function getFullNameAttribute(): string
	{
		return $this->last_name . ' ' . $this->first_name;
	}

	public function setFistNameAttribute($value): void
	{
		$this->attributes['first_name'] = ucwords($value);
	}

	public function setLastNameAttribute($value): void
	{
		$this->attributes['last_name'] = ucwords($value);
	}
}