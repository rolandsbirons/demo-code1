<?php

namespace App\Models\Patient\Teeth;

use App\Enums\DateTypes;
use App\Models\DenticModel;
use App\Models\Service\Service;
use App\Traits\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientToothServices
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property int $patient_id
 * @property int $user_id
 * @property int $tooth_id
 * @property int $service_id
 * @property int|null $appointment_id
 * @property bool $paid
 * @property bool $done
 * @property bool $saved
 * @property int|null $invoice_id
 * @property int $discount
 * @property float|null $price
 * @property string|null $time
 * @property int $visit
 * @property \Illuminate\Support\Carbon|null $processed_at
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read mixed $price_with_discount
 * @property-read \App\Models\Service $service
 * @property-read \App\Models\Patient\PatientTooth $tooth
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothService onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereDone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereProcessedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereSaved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereToothId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService whereVisit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothService withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothService withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothService query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property array|null $meta_history
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\Teeth\PatientTeethService whereMetaHistory($value)
 * @property-read int|null $activities_count
 */
class PatientTeethService extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	protected $fillable = [
		'discount',
		'done',
		'meta_history',
		'paid',
		'price',
		'processed_at',
		'saved',
		'service_id',
		'time',
		'tooth_id',
		'visit',
	];

	protected $casts = [
		'discount'     => 'integer',
		'done'         => 'boolean',
		'meta_history' => 'json',
		'paid'         => 'boolean',
		'price'        => 'float',
		'processed_at' => 'datetime',
		'saved'        => 'boolean',
		'service_id'   => 'integer',
		'tooth_id'     => 'integer',
		'visit'        => 'integer',
	];

	protected $hidden = ['company_id'];

	protected $appends = ['price_with_discount'];

	protected static $logFillable = true;

	public function getTimeAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function service(): BelongsTo
	{
		return $this->belongsTo(Service::class, 'service_id');
	}

	public function tooth(): HasOne
	{
		return $this->hasOne(PatientTooth::class, 'id', 'tooth_id');
	}

	public function getPriceAttribute($value)
	{
		return number_format($value, 2);
	}

	public function getPriceWithDiscountAttribute()
	{
		$discount = ($this->price / 100) * $this->discount;

		return number_format($this->price + $discount, 2);
	}

}
