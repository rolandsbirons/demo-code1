<?php

namespace App\Models\Patient\Teeth;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientToothPlan
 *
 * @property int                                                                                        $id
 * @property string                                                                                     $uuid
 * @property int                                                                                        $company_id
 * @property int                                                                                        $user_id
 * @property int                                                                                        $patient_id
 * @property int|null                                                                                   $appointment_id
 * @property string                                                                                     $name
 * @property bool                                                                                       $approved
 * @property int                                                                                        $order
 * @property bool                                                                                       $can_delete
 * @property bool                                                                                       $is_default
 * @property string|null                                                                                $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                            $created_at
 * @property \Illuminate\Support\Carbon|null                                                            $updated_at
 * @property \Illuminate\Support\Carbon|null                                                            $finished_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]        $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientTooth[]           $tooths
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothPlanVisit[] $visits
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlan onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan ordered($order = 'ASC')
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereCanDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlan withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlan withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\Teeth\PatientTooth[] $teeth
 * @property-read int|null $activities_count
 * @property-read int|null $teeth_count
 * @property-read int|null $visits_count
 */
class PatientTeethPlan extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	protected $fillable = [
		'appointment_id',
		'approved',
		'can_delete',
		'finished_at',
		'is_default',
		'name',
		'order',
		'patient_id',
	];

	protected $casts = [
		'approved'    => 'boolean',
		'can_delete'  => 'boolean',
		'finished_at' => 'datetime',
		'is_default'  => 'boolean',
		'order'       => 'integer',
		'patient_id'  => 'integer',
	];

	protected static $logFillable = true;

	protected static $ignoreChangedAttributes = ['order'];

	public function scopeOrdered($query, $order = 'ASC')
	{
		return $query->orderBy('order', $order);
	}

	/**
	 * Return plan visits
	 *
	 * @return HasMany
	 */

	public function visits(): HasMany
	{
		return $this->hasMany(PatientTeethPlanVisit::class, 'plan_id');
	}

	public function teeth(): HasMany
	{
		return $this->hasMany(PatientTooth::class, 'plan_id');
	}

}
