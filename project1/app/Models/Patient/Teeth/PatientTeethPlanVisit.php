<?php

namespace App\Models\Patient\Teeth;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Patient\PatientToothPlanVisits
 *
 * @property int                                       $id
 * @property string                                    $uuid
 * @property int                                       $company_id
 * @property int                                       $plan_id
 * @property bool                                      $finished
 * @property int                                       $disabled
 * @property \Illuminate\Support\Carbon|null           $finished_at
 * @property int                                       $visit_number
 * @property string|null                               $deleted_at
 * @property \Illuminate\Support\Carbon|null           $created_at
 * @property \Illuminate\Support\Carbon|null           $updated_at
 * @property-read \App\Models\Patient\PatientToothPlan $plan
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlanVisit onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereDisabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit whereVisitNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlanVisit withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothPlanVisit withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothPlanVisit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 */
class PatientTeethPlanVisit extends DenticModel
{
	use SoftDeletes, UuidTrait;

	protected $fillable = [
		'finished',
		'finished_at',
		'plan_id',
		'visit_number',
	];

	protected $casts = [
		'finished'     => 'boolean',
		'finished_at'  => 'datetime',
		'plan_id'      => 'integer',
		'visit_number' => 'integer',
	];

	public function plan(): HasOne
	{
		return $this->hasOne(PatientTeethPlan::class, 'id', 'plan_id');
	}

}
