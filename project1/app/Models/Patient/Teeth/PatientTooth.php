<?php

namespace App\Models\Patient\Teeth;

use App\Models\DenticModel;
use App\Models\Teeth\ToothStatus;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientTooth
 *
 * @property int                                                                                      $id
 * @property string                                                                                   $uuid
 * @property int                                                                                      $company_id
 * @property int                                                                                      $patient_id
 * @property int|null                                                                                 $plan_id
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothPlan[]          $plans
 * @property int|null                                                                                 $user_id
 * @property int                                                                                      $status_id
 * @property int                                                                                      $tooth_number
 * @property bool                                                                                     $done_before
 * @property bool                                                                                     $done_after
 * @property int                                                                                      $surface1
 * @property int                                                                                      $surface2
 * @property int                                                                                      $surface3
 * @property int                                                                                      $surface4
 * @property int                                                                                      $surface5
 * @property int                                                                                      $surface6
 * @property int                                                                                      $surface7
 * @property int                                                                                      $surface8
 * @property bool                                                                                     $finished
 * @property \Illuminate\Support\Carbon|null                                                          $approved_date
 * @property \Illuminate\Support\Carbon|null                                                          $created_at
 * @property \Illuminate\Support\Carbon|null                                                          $updated_at
 * @property string|null                                                                              $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]      $activity
 * @property-read \App\Models\Patient\PatientToothPlan|null                                           $plan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothService[] $services
 * @property-read \App\Models\Teeth\ToothStatus                                                       $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\PatientToothService[] $toothServices
 * @property-read \App\Models\User\User|null                                                          $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTooth onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereApprovedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereDoneAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereDoneBefore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth wherePlans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface4($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface5($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface6($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface7($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereSurface8($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereToothNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTooth withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientTooth withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientTooth query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]      $activities
 * @property-read int|null $activities_count
 * @property-read int|null $plans_count
 * @property-read int|null $services_count
 * @property-read int|null $tooth_services_count
 */
class PatientTooth extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	protected $fillable = [
		'approved_date',
		'done_after',
		'done_before',
		'finished',
		'patient_id',
		'plan_id',
		'plans',
		'tooth_number',
		'user_id',
	];

	protected $casts = [
		'approved_date' => 'datetime',
		'done_after'    => 'boolean',
		'done_before'   => 'boolean',
		'finished'      => 'boolean',
		'plans'         => 'array',
	];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function status(): BelongsTo
	{
		return $this->belongsTo(ToothStatus::class, 'status_id');
	}

	public function services(): HasMany
	{
		return $this->hasMany(PatientTeethService::class, 'tooth_id');
	}

	public function toothServices(): HasMany
	{
		return $this->hasMany(PatientTeethService::class, 'tooth_id');
	}

	public function unFinishedServices()
	{
		return $this->services()->where('saved', false);
	}

	public function plan(): BelongsTo
	{
		return $this->belongsTo(PatientTeethPlan::class, 'plan_id');
	}

	public function plans(): HasMany
	{
		return $this->hasMany(PatientTeethPlan::class, 'plan_id');
	}

}
