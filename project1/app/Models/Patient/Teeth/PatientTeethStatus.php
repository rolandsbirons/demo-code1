<?php

namespace App\Models\Patient\Teeth;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Patient\PatientToothStatus
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int                                                                                 $company_id
 * @property int|null                                                                            $user_id
 * @property int                                                                                 $patient_id
 * @property int                                                                                 $tooth_id
 * @property int                                                                                 $tooth_status
 * @property string|null                                                                         $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothStatus onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereToothId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereToothStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothStatus withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Patient\PatientToothStatus withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Patient\PatientToothStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class PatientTeethStatus extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity;

	protected $guarded = ['id'];

	protected static $logFillable = true;
}
