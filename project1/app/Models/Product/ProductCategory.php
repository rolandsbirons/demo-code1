<?php

namespace App\Models\Product;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Product\ProductCategory
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int                                                                                 $company_id
 * @property int|null                                                                            $group_id
 * @property string|null                                                                         $sku
 * @property string|null                                                                         $sku_backend
 * @property array|null                                                                          $name
 * @property string|null                                                                         $color
 * @property bool                                                                                $deletable
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product\Product[]         $products
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\ProductCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereSkuBackend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductCategory whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\ProductCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\ProductCategory withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read int|null $activities_count
 * @property-read int|null $products_count
 */
class ProductCategory extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity, HasTranslations;

	public $translatable = ['name'];

	protected $guarded = ['id'];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	protected $fillable = [
		'color',
		'name',
	];

	protected $casts = [
		'deletable' => 'boolean',
	];

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function products(): HasMany
	{
		return $this->hasMany(Product::class, 'product_category');
	}

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
