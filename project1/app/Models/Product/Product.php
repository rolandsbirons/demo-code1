<?php

namespace App\Models\Product;

use App\Enums\DateTypes;
use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Product\Product
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int|null                                                                            $company_id
 * @property string|null                                                                         $sku
 * @property float                                                                               $costs
 * @property int                                                                                 $surface
 * @property int                                                                                 $product_category
 * @property array|null                                                                          $product_name
 * @property string|null                                                                         $product_time
 * @property float                                                                               $product_price
 * @property int|null                                                                            $product_tax
 * @property string|null                                                                         $product_unit
 * @property string|null                                                                         $description
 * @property int                                                                                 $custom
 * @property string|null                                                                         $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Product\ProductCategory                                            $category
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereProductUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereSurface($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\Product whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product\Product withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed                                                                          $translations
 * @property-read int|null $activities_count
 */
class Product extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity, HasTranslations;

	public $translatable = ['product_name'];

	protected $fillable = [
		'costs',
		'custom',
		'description',
		'product_category',
		'product_name',
		'product_price',
		'product_time',
		'sku',
		'surface',
	];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function getProductTimeAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function category(): HasOne
	{
		return $this->hasOne(ProductCategory::class, 'id', 'product_category');
	}

	public function getProductPriceAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
