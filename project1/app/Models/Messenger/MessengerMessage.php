<?php

namespace App\Models\Messenger;

use App\Models\User\User;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Messenger\MessengerMessage
 *
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property int $company_id
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereUuid($value)
 * @mixin \Eloquent
 * @property int $branch_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Messenger\MessengerMessage whereBranchId($value)
 */
class MessengerMessage extends Model
{
	use UuidTrait;

	protected $fillable = [
		'branch_id',
		'content',
	];

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}
}
