<?php

namespace App\Models\Branch;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Branch\BranchScheduleGroup
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int                                                                                 $company_id
 * @property string                                                                              $type
 * @property array                                                                               $repeat_values
 * @property string                                                                              $repeat_after
 * @property \Illuminate\Support\Carbon                                                          $date_from
 * @property \Illuminate\Support\Carbon                                                          $date_to
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchScheduleGroup onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereRepeatAfter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereRepeatValues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchScheduleGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchScheduleGroup withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchScheduleGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class BranchScheduleGroup extends DenticModel
{

	use UuidTrait, SoftDeletes, LogsActivity;

	protected $guarded = ['id'];

	public $casts = [
		'date_from'     => 'date',
		'date_to'       => 'date',
		'repeat_values' => 'array',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = [
		'company_id',
	];

	protected static $logFillable = true;

}
