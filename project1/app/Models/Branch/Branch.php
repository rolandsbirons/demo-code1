<?php

namespace App\Models\Branch;

use App\Models\DenticModel;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Branch\Branch
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property string|null                                                                         $color
 * @property int                                                                                 $company_id
 * @property string|null                                                                         $name
 * @property string|null                                                                         $email
 * @property string|null                                                                         $phone
 * @property string|null                                                                         $bank_name
 * @property string|null                                                                         $bank_code
 * @property string|null                                                                         $bank_swift
 * @property string|null                                                                         $company_name
 * @property string|null                                                                         $company_number
 * @property string|null                                                                         $address_declared
 * @property string|null                                                                         $address_actual
 * @property string|null                                                                         $logo
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchRoom[]      $rooms
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\Branch onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereAddressActual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereAddressDeclared($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereBankSwift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereCompanyNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\Branch withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\Branch withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\Branch query()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[]               $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchWorkTime[]   $workTimes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read int|null $rooms_count
 * @property-read int|null $users_count
 * @property-read int|null $work_times_count
 */
class Branch extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity;

	protected $fillable = [
		'address_actual',
		'address_declared',
		'bank_code',
		'bank_name',
		'bank_swift',
		'color',
		'company_name',
		'company_number',
		'email',
		'name',
		'phone',
	];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function workTimes(): HasMany
	{
		return $this->hasMany(BranchWorkTime::class)->orderBy('day');
	}

	public function rooms(): hasMany
	{
		return $this->hasMany(BranchRoom::class)->orderBy('order');
	}

	public function users(): belongsToMany
	{
		return $this->belongsToMany(User::class, 'branch_users', 'branch_id', 'user_id');
	}
}