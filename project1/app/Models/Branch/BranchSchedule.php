<?php

namespace App\Models\Branch;

use App\Enums\DateTypes;
use App\Models\Appointment;
use App\Models\DenticModel;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Branch\BranchSchedule
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int                                                                                 $company_id
 * @property string|null                                                                         $schedule_group
 * @property int                                                                                 $branch_id
 * @property int                                                                                 $room_id
 * @property int                                                                                 $user_id
 * @property int                                                                                 $tool_id
 * @property int|null                                                                            $appointment_id
 * @property int                                                                                 $type
 * @property bool                                                                                $protected
 * @property \Illuminate\Support\Carbon                                                          $schedule_date
 * @property string                                                                              $time_from
 * @property string                                                                              $time_to
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Branch\Branch                                                      $branch
 * @property-read \App\Models\Branch\BranchScheduleGroup                                         $group
 * @property-read \App\Models\Branch\BranchRoom                                                 $room
 * @property-read \App\Models\User\User                                                          $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchSchedule onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereAppointmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereRoomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereScheduleDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereScheduleGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereToolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchSchedule withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchSchedule withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule query()
 * @property-read \App\Models\Appointment|null                                                   $appointment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchSchedule withoutCacheCheck()
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class BranchSchedule extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	public $fillable = [
		'appointment_id',
		'branch_id',
		'protected',
		'room_id',
		'schedule_date',
		'time_from',
		'time_to',
		'tool_id',
		'type',
		'user_id',
	];

	protected $casts = [
		'appointment_id' => 'integer',
		'branch_id'      => 'integer',
		'protected'      => 'boolean',
		'room_id'        => 'integer',
		'schedule_date'  => 'date',
		'tool_id'        => 'integer',
		'user_id'        => 'integer',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $hidden = [
		'company_id',
	];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function getTimeFromAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function getTimeToAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function branch(): BelongsTo
	{
		return $this->belongsTo(Branch::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class)->withTrashed();
	}

	public function room(): BelongsTo
	{
		return $this->belongsTo(BranchRoom::class);
	}

	public function group(): HasOne
	{
		return $this->hasOne(BranchScheduleGroup::class, 'uuid', 'schedule_group');
	}

	public function appointment(): BelongsTo
	{
		return $this->belongsTo(Appointment::class);
	}
}
