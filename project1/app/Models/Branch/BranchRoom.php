<?php

namespace App\Models\Branch;

use App\Models\Appointment;
use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Branch\BranchRooms
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property int $branch_id
 * @property string|null $name
 * @property int|null $order
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchRoom onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchRoom withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchRoom withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $color
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom ordered()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereColor($value)
 * @property int $hidden
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[] $appointments
 * @property-read \App\Models\Branch\Branch $branch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchSchedule[] $schedules
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchWorkTime[] $workTimes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom whereHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchRoom withoutCacheCheck()
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read int|null $appointments_count
 * @property-read int|null $schedules_count
 * @property-read int|null $work_times_count
 */
class BranchRoom extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity;

	public $timestamps = false;

	protected $fillable = [
		'branch_id',
		'color',
		'name',
		'order',
		'hidden',
	];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function scopeOrdered($query)
	{
		return $query->orderBy('order', 'ASC');
	}

	public function schedules(): HasMany
	{
		return $this->hasMany(BranchSchedule::class, 'room_id');
	}

	public function appointments(): HasMany
	{
		return $this->hasMany(Appointment::class, 'room_id');
	}

	public function workTimes(): HasMany
	{
		return $this->hasMany(BranchWorkTime::class, 'branch_id', 'branch_id');
	}

	public function branch(): HasOne
	{
		return $this->hasOne(Branch::class, 'id', 'branch_id');
	}
}
