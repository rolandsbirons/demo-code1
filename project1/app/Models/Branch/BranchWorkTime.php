<?php

namespace App\Models\Branch;

use App\Enums\DateTypes;
use App\Models\DenticModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Branch\BranchWorkTime
 *
 * @property int         $id
 * @property int         $branch_id
 * @property int         $day
 * @property string      $time_from
 * @property string      $time_to
 * @property bool        $active
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchWorkTime onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime whereTimeTo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchWorkTime withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Branch\BranchWorkTime withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Branch\BranchWorkTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 */
class BranchWorkTime extends DenticModel
{

	use SoftDeletes;

	public $timestamps = false;

	protected $fillable = [
		'active',
		'branch_id',
		'day',
		'time_from',
		'time_to',
	];

	protected $casts = [
		'active' => 'boolean',
	];

	protected $hidden = ['company_id'];

	public function getTimeFromAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function getTimeToAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

}
