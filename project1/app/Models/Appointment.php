<?php

namespace App\Models;

use App\Enums\DateTypes;
use App\Models\Branch\BranchRoom;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethPlanVisit;
use App\Models\Service\ServiceCategory;
use App\Models\User\User;
use App\Traits\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Appointment
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property string|null $schedule_uuid
 * @property int $patient_id
 * @property int $room_id
 * @property int $user_id
 * @property int|null $provider_id
 * @property int|null $assistant_id
 * @property int|null $service_id
 * @property int $plan_id
 * @property int $status
 * @property \Illuminate\Support\Carbon $event_date
 * @property string $event_time_from
 * @property string $event_time_to
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $session_start
 * @property \Illuminate\Support\Carbon|null $session_end
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\User\User|null $assistant
 * @property-read \App\Models\Patient\Patient $patient
 * @property-read \App\Models\Branch\BranchRoom $room
 * @property-read \App\Models\Service\ServiceCategory|null $service
 * @property-read \App\Models\User\User|null $user
 * @property-read \App\Models\Patient\PatientToothPlanVisit $visit
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Appointment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereAssistantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereEventDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereEventTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereEventTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereRoomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereScheduleUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereSessionEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereSessionStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Appointment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Appointment withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment withoutCacheCheck()
 * @property float|null $price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment wherePrice($value)
 * @property bool $strict
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment whereStrict($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Appointment branchUuid($uuid)
 * @property-read int|null $activities_count
 */
class Appointment extends DenticModel
{
	use SoftDeletes, UuidTrait, LogsActivity;

	protected $fillable = [
		'assistant_id',
		'description',
		'event_date',
		'event_time_from',
		'event_time_to',
		'patient_id',
		'plan_id',
		'price',
		'provider_id',
		'room_id',
		'schedule_uuid',
		'service_id',
		'session_end',
		'session_start',
		'status',
		'strict',
		'user_id',
	];

	protected $dates = ['event_date'];

	protected $casts = [
		'event_date'    => 'datetime',
		'price'         => 'float',
		'session_end'   => 'datetime',
		'session_start' => 'datetime',
		'strict'        => 'boolean',
	];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function patient(): BelongsTo
	{
		return $this->belongsTo(Patient::class);
	}

	public function author(): BelongsTo
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function user(): BelongsTo //Refactor
	{
		return $this->belongsTo(User::class, 'provider_id');
	}

	public function assistant(): BelongsTo
	{
		return $this->belongsTo(User::class, 'assistant_id');
	}

	public function service(): BelongsTo
	{
		return $this->belongsTo(ServiceCategory::class, 'service_id');
	}

	public function room(): BelongsTo
	{
		return $this->belongsTo(BranchRoom::class);
	}


	public function scopeBranchUuid($query, $uuid): void
	{
		$query->whereHas('room.branch', static function ($query) use ($uuid) {
			$query->where('uuid', $uuid);
		});
	}

	public function visit(): HasOne
	{
		return $this->hasOne(PatientTeethPlanVisit::class, 'id', 'plan_id');
	}

	public function getEventTimeFromAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function getEventTimeToAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function setPriceAttribute($value): void
	{
		if ($value) {
			$value = str_replace(',', '.', $value);
		}

		$this->attributes['price'] = $value;
	}

	public function getPriceAttribute($value)
	{
		if ($value) {
			return number_format($value, 2, '.', '');
		}

		return $value;
	}
}
