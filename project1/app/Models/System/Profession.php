<?php

namespace App\Models\System;

use App\Models\DenticModel;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\System\Professions
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Profession whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Profession extends DenticModel
{
	use HasTranslations;

	protected $fillable = [
		'name',
	];

	public $translatable = ['name'];

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
