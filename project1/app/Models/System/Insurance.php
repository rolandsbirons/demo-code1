<?php

namespace App\Models\System;

use App\Models\DenticModel;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\System\Insurance
 *
 * @property int $id
 * @property array $name
 * @property-read mixed $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Insurance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Insurance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Insurance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Insurance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\System\Insurance whereName($value)
 * @mixin \Eloquent
 */
class Insurance extends DenticModel
{
	use HasTranslations;

	public $timestamps = false;

	protected $fillable = [
		'name',
	];

	public $translatable = ['name'];

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
