<?php

namespace App\Models\Services;

use App\Models\DenticModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Services\ServiceSpecificPermissions
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property int|null                                                                            $company_id
 * @property int                                                                                 $service_id
 * @property int                                                                                 $permission_id
 * @property string|null                                                                         $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Services\ServiceSpecificPermission onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission wherePermissionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Services\ServiceSpecificPermission withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Services\ServiceSpecificPermission withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Services\ServiceSpecificPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class ServiceSpecificPermission extends DenticModel
{
	use SoftDeletes, LogsActivity;

	public $incrementing = false;

	protected $fillable = [
		'service_id',
		'permission_id',
	];
}
