<?php

namespace App\Models\Service;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Service\ServiceCategory
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property int|null $group_id
 * @property string|null $sku
 * @property string|null $sku_backend
 * @property array|null $name
 * @property string|null $color
 * @property bool $deletable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property string|null $latin_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service\Service[] $services
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory isLocal()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereLatinName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereSkuBackend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceCategory whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceCategory withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read \App\Models\Service\ServiceGroup|null $group
 * @property-read int|null $activities_count
 * @property-read int|null $services_count
 */
class ServiceCategory extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity, HasTranslations;

	public $translatable = ['name'];

	protected $guarded = ['id'];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	protected $fillable = [
		'color',
		'name',
	];

	protected $casts = [
		'deletable' => 'boolean',
	];

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function services(): HasMany
	{
		return $this->hasMany(Service::class, 'service_category');
	}

	public function group(): BelongsTo
	{
		return $this->belongsTo(ServiceGroup::class, 'group_id', 'id');
	}

	public function scopeIsLocal($query)
	{
		return $query->where('group_id', null);
	}

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
