<?php

namespace App\Models\Service;

use App\Enums\DateTypes;
use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Service\Service
 *
 * @property int $id
 * @property string $uuid
 * @property int|null $company_id
 * @property string|null $sku
 * @property float $costs
 * @property int $surface
 * @property int $service_category
 * @property array|null $service_name
 * @property string|null $service_time
 * @property float $service_price
 * @property int|null $service_tax
 * @property string|null $service_unit
 * @property string|null $description
 * @property int $custom
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \App\Models\Service\ServiceCategory $category
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\Service onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereCosts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServiceCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServiceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServicePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServiceTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServiceTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServiceUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereSurface($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\Service withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\Service withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property float $service_price_from
 * @property float $service_price_to
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServicePriceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\Service whereServicePriceTo($value)
 * @property-read mixed $service_price_avg
 * @property-read int|null $activities_count
 */
class Service extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity, HasTranslations;

	public $translatable = ['service_name'];

	protected $appends = ['service_price_avg'];

	protected $fillable = [
		'costs',
		'custom',
		'description',
		'service_category',
		'service_name',
		'service_price_from',
		'service_price_to',
		'service_time',
		'sku',
		'surface',
	];

	protected $casts = [
		'custom' => 'boolean',
	];

	protected $hidden = ['company_id'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function getServiceTimeAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function getServicePriceFromAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}

	public function getServicePriceToAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}

	public function getCostsAttribute($value)
	{
		return number_format($value, 2, '.', '');
	}

	public function getServicePriceAvgAttribute()
	{
		$servicePriceTo = ($this->service_price_to > $this->service_price_from && $this->service_price_from) ?
			$this->service_price_to :
			$this->service_price_from;

		$avgPrice = collect([$this->service_price_from, $servicePriceTo])->avg();

		return number_format($avgPrice, 2, '.', '');
	}

	public function category(): HasOne
	{
		return $this->hasOne(ServiceCategory::class, 'id', 'service_category');
	}

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
