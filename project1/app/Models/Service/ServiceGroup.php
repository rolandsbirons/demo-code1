<?php

namespace App\Models\Service;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Service\ServiceGroup
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property array|null $name
 * @property string|null $color_on
 * @property string|null $color_off
 * @property bool $deletable
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $sku_backend
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Service\ServiceCategory[] $categories
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereColorOff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereColorOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereSkuBackend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceGroup withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Service\ServiceGroup withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property int $group_type
 * @property array|null $service_type
 * @property int|null $discount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup serviceType($serviceType)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereGroupType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Service\ServiceGroup whereServiceType($value)
 * @property-read int|null $activities_count
 * @property-read int|null $categories_count
 */
class ServiceGroup extends DenticModel
{
	use UuidTrait, SoftDeletes, LogsActivity, HasTranslations;

	public $translatable = ['name'];

	protected $fillable = [
		'color_off',
		'color_on',
		'deletable',
		'group_type',
		'service_type',
		'name',
	];

	protected $hidden = [
		'sku_backend',
	];

	protected $casts = [
		'deletable'    => 'boolean',
		'group_type'   => 'integer',
		'service_type' => 'array',
	];

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function categories(): HasMany
	{
		return $this->hasMany(ServiceCategory::class, 'group_id');
	}

	public function scopeServiceType($query, $serviceType)
	{
		return $query->where('service_type', 'like', '%"' . $serviceType . '"%');
	}

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}
