<?php

namespace App\Models\Department;

use App\Models\Branch\Branch;
use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Department\Department
 *
 * @property int                                                                       $id
 * @property string                                                                    $uuid
 * @property int                                                                       $company_id
 * @property array                                                                     $name
 * @property string|null                                                               $color
 * @property bool                                                                      $deletable
 * @property string|null                                                               $deleted_at
 * @property \Illuminate\Support\Carbon|null                                           $created_at
 * @property \Illuminate\Support\Carbon|null                                           $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\Branch[] $branches
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department\Department onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Department\Department whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department\Department withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department\Department withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read mixed $translations
 * @property-read int|null $branches_count
 */
class Department extends DenticModel
{
	use UuidTrait, SoftDeletes, HasTranslations;

	public $translatable = [
		'name',
	];

	protected $fillable = [
		'color',
		'deletable',
		'name',
	];

	protected $casts = [
		'deletable' => 'boolean',
	];

	protected $hidden = ['company_id'];

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function branches(): BelongsToMany
	{
		return $this->belongsToMany(Branch::class);
	}
}
