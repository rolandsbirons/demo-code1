<?php

namespace App\Models\User;

use App\Models\DenticModel;

/**
 * App\Models\User\UserSettings
 *
 * @property int    $user_id
 * @property string $setting_key
 * @property string $setting_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting whereSettingKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting whereSettingValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 */
class UserSetting extends DenticModel
{
	protected $primaryKey = null;

	public $incrementing = false;

	public $timestamps = false;

	public $fillable = [
		'setting_key',
		'setting_value',
		'user_id',
	];
}
