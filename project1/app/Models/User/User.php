<?php

namespace App\Models\User;

use App\Models\Appointment;
use App\Models\Branch\Branch;
use App\Models\Branch\BranchSchedule;
use App\Models\Company;
use App\Models\Messenger\MessengerMessage;
use App\Models\Patient\Patient;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\Models\User\User
 *
 * @property int $id
 * @property string $uuid
 * @property int $company_id
 * @property string $password
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $personal_code
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $certificate
 * @property string|null $certificate_date
 * @property string|null $sanitary_book
 * @property bool $user_status
 * @property string|null $color
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\Branch[] $branches
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Patient\Patient[] $clients
 * @property-read mixed $branches_text
 * @property-read mixed $full_name
 * @property-read mixed $full_name_short
 * @property-read mixed $positions_text
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereCertificate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereCertificateDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePersonalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereSanitaryBook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUserStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\User withoutTrashed()
 * @mixin \Eloquent
 * @property string $username
 * @property bool $deletable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User newQuery()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Position[] $positions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchSchedule[] $userSchedules
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\UserWorkTime[] $workTimes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\User query()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Branch\BranchSchedule[] $schedules
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\UserSetting[] $userSettings
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Messenger\MessengerMessage[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[] $appointments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[] $appointmentsAssistant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Appointment[] $appointmentsProvider
 * @property-read int|null $activities_count
 * @property-read int|null $appointments_count
 * @property-read int|null $appointments_assistant_count
 * @property-read int|null $appointments_provider_count
 * @property-read int|null $branches_count
 * @property-read int|null $clients_count
 * @property-read int|null $messages_count
 * @property-read int|null $positions_count
 * @property-read int|null $schedules_count
 * @property-read int|null $user_settings_count
 * @property-read int|null $work_times_count
 */
class User extends Authenticatable implements JWTSubject
{
	use SoftDeletes, UuidTrait, LogsActivity;

	protected $fillable = [
		'certificate',
		'certificate_date',
		'deletable',
		'email',
		'first_name',
		'last_name',
		'password',
		'personal_code',
		'phone',
		'sanitary_book',
		'user_status',
		'username',
	];

	protected $hidden = [
		'password',
	];

	protected $casts = [
		'deletable'   => 'boolean',
		'user_status' => 'boolean',
		'gender'      => 'integer',
	];

	protected $appends = [
		'full_name',
		'full_name_short',
	];

	protected static $ignoreChangedAttributes = ['password'];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function canAction($actionType)
	{
		if (is_array($actionType)) {
			$user = self::whereHas('positions', function ($q) {
				$q->where('user_id', $this->id);
			})->with([
				'positions' => function ($q) use ($actionType) {
					$q->where('user_id', $this->id);
					$q->with([
						'permissions' => static function ($q) use ($actionType) {
							$q->whereIn('permission_type', $actionType);
						},
					]);
				}])
				->first();

			if ($user->positions->isEmpty()) {
				return false;
			}

			return $user->positions->pluck('permissions')
					->flatten(1)
					->pluck('permission_type')
					->unique()
					->count() === count($actionType);
		}

		$user = self::whereHas('positions', function ($q) {
			$q->where('user_id', $this->id);
		})->with([
			'positions' => function ($q) use ($actionType) {
				$q->where('user_id', $this->id);
				$q->whereHas('permissions', static function ($q) use ($actionType) {
					$q->where('permission_type', $actionType);
				});
			}])->first();

		if ($user->positions->isEmpty()) {
			return false;
		}

		return $user->positions->pluck('permissions')->flatten(1)->isNotEmpty();
	}

	public function clients(): HasMany
	{
		return $this->hasMany(Patient::class);
	}

	public function appointments(): HasMany
	{
		return $this->hasMany(Appointment::class);
	}

	public function appointmentsProvider(): HasMany
	{
		return $this->hasMany(Appointment::class, 'provider_id');
	}

	public function appointmentsAssistant(): HasMany
	{
		return $this->hasMany(Appointment::class, 'assistant_id');
	}

	public function positions()
	{
		return $this->belongsToMany(Position::class, 'user_positions', 'user_id', 'position_id');
	}

	public function branches()
	{
		return $this->belongsToMany(Branch::class, 'branch_users', 'user_id', 'branch_id');
	}

	public function workTimes()
	{
		return $this->hasMany(UserWorkTime::class)->orderBy('day')->where('break', false);
	}

	public function schedules()
	{
		return $this->hasMany(BranchSchedule::class);
	}

	public function userSettings()
	{
		return $this->hasMany(UserSetting::class);
	}

	public function userSetting($key)
	{
		return $this->hasOne(UserSetting::class)->where('settings_key', $key);
	}

	public function company()
	{
		return $this->hasOne(Company::class, 'id', 'company_id');
	}

	public function messages()
	{
		return $this->hasMany(MessengerMessage::class, 'user_id');
	}

	public function getFullNameAttribute()
	{
		return $this->last_name . ' ' . $this->first_name;
	}

	public function getFullNameShortAttribute()
	{
		return $this->last_name . ' ' . strtoupper(mb_substr($this->first_name, 0, 1)) . '.';
	}

	public function getJWTIdentifier()
	{
		return $this->getKey();
	}

	public function getJWTCustomClaims(): array
	{
		return [];
	}
}