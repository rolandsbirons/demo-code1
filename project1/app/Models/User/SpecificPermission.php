<?php

namespace App\Models\User;

use App\Models\DenticModel;
use App\Traits\UuidOnlyTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\User\SpecificPermissions
 *
 * @property int                                                                                 $id
 * @property string                                                                              $uuid
 * @property string                                                                              $code
 * @property array|null                                                                          $name
 * @property string|null                                                                         $description
 * @property string|null                                                                         $deleted_at
 * @property \Illuminate\Support\Carbon|null                                                     $created_at
 * @property \Illuminate\Support\Carbon|null                                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\SpecificPermission onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\SpecificPermission withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User\SpecificPermission withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\SpecificPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read int|null $activities_count
 */
class SpecificPermission extends DenticModel
{
	use UuidOnlyTrait, SoftDeletes, HasTranslations, LogsActivity;

	protected $fillable = [
		'code',
		'name',
		'description',
	];

	public $translatable = [
		'name',
	];
}
