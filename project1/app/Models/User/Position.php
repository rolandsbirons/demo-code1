<?php

namespace App\Models\User;

use App\Models\DenticModel;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\User\Positions
 *
 * @property int                                                                                  $id
 * @property string                                                                               $uuid
 * @property array|null                                                                           $name
 * @property string|null                                                                          $color
 * @property int                                                                                  $company_id
 * @property int                                                                                  $type
 * @property bool                                                                                 $deletable
 * @property \Illuminate\Support\Carbon|null                                                      $created_at
 * @property \Illuminate\Support\Carbon|null                                                      $updated_at
 * @property string|null                                                                          $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[]  $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\PositionPermission[] $permissions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position whereUuid($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Position query()
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read mixed $translations
 * @property-read int|null $activities_count
 * @property-read int|null $permissions_count
 * @property-read int|null $users_count
 */
class Position extends DenticModel
{
	use UuidTrait, LogsActivity, HasTranslations;

	public $translatable = ['name'];

	protected $fillable = [
		'color',
		'name',
		'type',
	];

	protected $casts = [
		'deletable' => 'boolean',
	];

	protected static $logFillable = true;

	public function getRouteKeyName(): string
	{
		return 'uuid';
	}

	public function permissions(): HasMany
	{
		return $this->hasMany(PositionPermission::class);
	}

	public function users(): BelongsToMany
	{
		return $this->belongsToMany(User::class, 'user_positions', 'position_id', 'user_id');
	}

	/**
	 * Convert the model instance to an array.
	 *
	 * @return array
	 */

	public function toArray(): array
	{
		$attributes = parent::toArray();

		foreach ($this->getTranslatableAttributes() as $name) {
			$attributes[$name] = $this->getTranslation($name, app()->getLocale());
		}

		return $attributes;
	}
}