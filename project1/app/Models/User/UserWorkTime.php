<?php

namespace App\Models\User;

use App\Enums\DateTypes;
use App\Models\DenticModel;
use Carbon\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\User\UserWorkTime
 *
 * @property int                                                                                 $id
 * @property int                                                                                 $user_id
 * @property int                                                                                 $day
 * @property string                                                                              $time_from
 * @property string                                                                              $time_to
 * @property bool                                                                                $active
 * @property bool                                                                                $break
 * @property string|null                                                                         $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereBreak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereTimeTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\UserWorkTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 */
class UserWorkTime extends DenticModel
{
	use LogsActivity;

	public $timestamps = false;

	protected $fillable = [
		'active',
		'break',
		'day',
		'time_from',
		'time_to',
		'user_id',
	];

	protected $casts = [
		'active' => 'boolean',
		'break'  => 'boolean',
	];

	protected static $logFillable = true;

	public function getTimeFromAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function getTimeToAttribute($value): string
	{
		return Carbon::parse($value)->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME);
	}

	public function breaks()
	{
		return $this->where([
			'break'   => true,
			'day'     => $this->day,
			'user_id' => $this->user_id,
		])->orderBy('time_from', 'ASC')->get();
	}
}
