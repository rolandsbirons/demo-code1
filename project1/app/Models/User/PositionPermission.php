<?php

namespace App\Models\User;

use App\Models\DenticModel;

/**
 * App\Models\User\PositionPermissions
 *
 * @property int         $id
 * @property int         $positions_id
 * @property string      $permission_type
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission wherePermissionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission wherePositionsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel disableCache()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DenticModel withCacheCooldownSeconds($seconds)
 * @property int $position_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\PositionPermission wherePositionId($value)
 */
class PositionPermission extends DenticModel
{
	public $timestamps = false;

	public $incrementing = false;

	protected $fillable = [
		'permission_type',
		'position_id',
	];
}
