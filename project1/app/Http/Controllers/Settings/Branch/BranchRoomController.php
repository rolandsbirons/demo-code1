<?php

namespace App\Http\Controllers\Settings\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\Rooms\BranchRoomCreateRequest;
use App\Http\Requests\Branch\Rooms\BranchRoomDeleteRequest;
use App\Http\Requests\Branch\Rooms\BranchRoomUpdateOrderRequest;
use App\Http\Requests\Branch\Rooms\BranchRoomUpdateRequest;
use App\Http\Requests\Branch\Rooms\BranchRoomViewRequest;
use App\Http\Resources\Branch\BranchRoomsListResource;
use App\Http\Resources\Branch\BranchRoomsResource;
use App\Models\Branch\Branch;
use App\Models\Branch\BranchRoom;
use Exception;
use Illuminate\Http\Response;

class BranchRoomController extends Controller
{
	public function index(BranchRoomViewRequest $request, Branch $branch): BranchRoomsResource
	{
		try {
			return new BranchRoomsResource($branch);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function view(BranchRoomViewRequest $request, Branch $branch, $branchRoomUuid)
	{
		try {
			$branchRoom = $branch->rooms()
				->whereUuid($branchRoomUuid)
				->firstOrFail();

			return new BranchRoomsListResource($branchRoom);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(BranchRoomCreateRequest $request, Branch $branch)
	{
		try {
			$branchLastOrder = BranchRoom::select('order')
				->where('branch_id', $branch->id)
				->orderBy('order', 'DESC')
				->first();

			$room = new BranchRoom;
			$room->branch_id = $branch->id;
			$room->color = $request->get('color');
			$room->name = $request->get('name');
			$room->order = $branchLastOrder ? $branchLastOrder->order + 1 : 1;

			if ($room->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(BranchRoomUpdateRequest $request, Branch $branch, $branchRoomUuid)
	{
		try {
			$branchRoom = $branch->rooms()
				->whereUuid($branchRoomUuid)
				->firstOrFail();

			$branchRoom->color = $request->get('color');
			$branchRoom->hidden = $request->get('hidden');
			$branchRoom->name = $request->get('name');

			if ($branchRoom->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_GATEWAY);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function order(BranchRoomUpdateOrderRequest $request, Branch $branch)
	{
		try {
			$branchRooms = $request->get('rooms', []);
			$branchRoomCollection = collect($branchRooms);

			if ($branchRoomCollection->count() > 0) {
				$branchRoomUuids = $branchRoomCollection->pluck('uuid');

				foreach ($branchRoomUuids as $roomIndex => $roomUuid) {
					$branchRoom = $branch->rooms()
						->whereUuid($roomUuid)
						->firstOrFail();

					$branchRoom->order = ++$roomIndex;
					$branchRoom->save();
				}
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(BranchRoomDeleteRequest $request, Branch $branch, $roomUuid)
	{
		try {
			$branchRoom = $branch->rooms()
				->whereUuid($roomUuid)
				->firstOrFail();

			if ($branchRoom->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_GATEWAY);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
