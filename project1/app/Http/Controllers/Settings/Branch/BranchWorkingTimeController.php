<?php

namespace App\Http\Controllers\Settings\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\WorkingHours\BranchWorkingHoursUpdateRequest;
use App\Http\Requests\Branch\WorkingHours\BranchWorkingHoursViewRequest;
use App\Http\Resources\Branch\BranchWorkingHoursResource;
use App\Models\Branch\Branch;
use App\Traits\PermissionTrait;
use Exception;
use Illuminate\Http\Response;

class BranchWorkingTimeController extends Controller
{
	use PermissionTrait;

	public function __construct()
	{
		$this->canAccessFunction([
			'branch_view',
			'branch_working_hours_view',
		], 'abort');
	}

	public function index(BranchWorkingHoursViewRequest $request, Branch $branch)
	{
		try {
			return new BranchWorkingHoursResource($branch);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(BranchWorkingHoursUpdateRequest $request, Branch $branch)
	{
		try {
			$this->parseWorkTime($branch, $request->all());

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function parseWorkTime(Branch $branch, array $data = []): void
	{
		foreach ($data as $time) {
			$workingTime = $branch->workTimes()->whereId($time['id'])->firstOrFail();
			$workingTime->time_from = (strtotime($time['time_from']) === false) ? '00:00:00' : $time['time_from'];
			$workingTime->time_to = (strtotime($time['time_to']) === false) ? '00:00:00' : $time['time_to'];
			$workingTime->active = $time['active'];
			$workingTime->save();
		}
	}
}
