<?php

namespace App\Http\Controllers\Settings\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\Settings\BranchSettingsDeleteRequest;
use App\Http\Requests\Branch\Settings\BranchSettingsUpdateRequest;
use App\Http\Requests\Branch\Settings\BranchSettingsViewRequest;
use App\Http\Resources\Branch\BranchSettingResource;
use App\Models\Branch\Branch;
use Exception;
use Illuminate\Http\Response;

class BranchSettingsController extends Controller
{
	public function index(BranchSettingsViewRequest $request, Branch $branch)
	{
		try {
			return new BranchSettingResource($branch);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(BranchSettingsUpdateRequest $request, Branch $branch)
	{
		try {
			$branch->name = $request->get('name');
			$branch->color = $request->get('color');

			if ($branch->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(BranchSettingsDeleteRequest $request, Branch $branch)
	{
		try {
			if ($branch->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
