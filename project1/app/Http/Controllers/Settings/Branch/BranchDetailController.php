<?php

namespace App\Http\Controllers\Settings\Branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\BranchDetailsViewRequest;
use App\Http\Requests\Branch\BranchUpdateRequest;
use App\Http\Resources\Branch\BranchDetailsResource;
use App\Models\Branch\Branch;
use Exception;
use Illuminate\Http\Response;

class BranchDetailController extends Controller
{
	public function show(BranchDetailsViewRequest $request, Branch $branch)
	{
		try {
			return new BranchDetailsResource($branch);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(BranchUpdateRequest $request, Branch $branch)
	{
		try {
			$branch->address_actual = $request->get('address_actual');
			$branch->address_declared = $request->get('address_declared');
			$branch->bank_code = $request->get('bank_code');
			$branch->bank_name = $request->get('bank_name');
			$branch->bank_swift = $request->get('bank_swift');
			$branch->color = $request->get('color');
			$branch->company_name = $request->get('company_name');
			$branch->company_number = $request->get('company_number');
			$branch->email = $request->get('email');
			$branch->phone = $request->get('phone');

			if ($branch->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
