<?php

namespace App\Http\Controllers\Settings\Branch;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\BranchCreateRequest;
use App\Http\Requests\Branch\BranchViewRequest;
use App\Http\Resources\Branch\BranchListResource;
use App\Models\Branch\Branch;
use App\Models\Branch\BranchWorkTime;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class BranchController extends Controller
{
	use SearchTrait;

	public function index(BranchViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'name',
					'uuid',
					'color',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = Branch::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return BranchListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(BranchCreateRequest $request)
	{
		try {
			$branch = new Branch;
			$branch->name = $request->get('name');
			$branch->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;

			if ($branch->save()) {
				$branch->workTimes()->saveMany([
					new BranchWorkTime(['day' => 1]),
					new BranchWorkTime(['day' => 2]),
					new BranchWorkTime(['day' => 3]),
					new BranchWorkTime(['day' => 4]),
					new BranchWorkTime(['day' => 5]),
					new BranchWorkTime(['day' => 6]),
					new BranchWorkTime(['day' => 7]),
				]);

				return response()->json([
					'uuid' => $branch->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
