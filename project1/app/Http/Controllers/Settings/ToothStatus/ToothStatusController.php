<?php

namespace App\Http\Controllers\Settings\ToothStatus;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\ToothStatus\ToothStatusCreateRequest;
use App\Http\Requests\ToothStatus\ToothStatusViewRequest;
use App\Http\Resources\ToothStatus\ToothStatusListResource;
use App\Models\Teeth\ToothStatusCategory;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class ToothStatusController extends Controller
{
	use SearchTrait;

	public function index(ToothStatusViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'color',
					'name',
					'uuid',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = ToothStatusCategory::isLocal()->select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return ToothStatusListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ToothStatusCreateRequest $request)
	{
		try {
			$toothStatusCategory = new ToothStatusCategory;
			$toothStatusCategory->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;
			$toothStatusCategory->name = $request->get('name');

			if ($toothStatusCategory->save()) {
				return response()->json([
					'tooth_status_uuid' => $toothStatusCategory->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
