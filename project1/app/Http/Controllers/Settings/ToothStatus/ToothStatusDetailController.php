<?php

namespace App\Http\Controllers\Settings\ToothStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothStatus\Status\ToothStatusItemUpdateOrderRequest;
use App\Http\Requests\ToothStatus\ToothStatusDetailViewRequest;
use App\Http\Resources\ToothStatus\ToothStatusDetailResource;
use App\Models\Teeth\ToothStatusCategory;
use Exception;
use Illuminate\Http\Response;

class ToothStatusDetailController extends Controller
{
	public function show(ToothStatusDetailViewRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		try {
			$toothStatusCategory->load([
				'statuses' => static function ($q) {
					$q->orderBy('order', 'ASC');
					$q->select(
						'id',
						'name_cure',
						'name_diagnostic',
						'name_scheduled',
						'tooth_status_category',
						'display_type',
						'icon_text',
						'uuid'
					);
					$q->with(['display' => static function ($q) {
						$q->select('id', 'configuration');
					}]);
				},
			]);

			return new ToothStatusDetailResource($toothStatusCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function order(ToothStatusItemUpdateOrderRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		$toothStatuses = $request->get('statuses', []);
		$i = 1;

		try {
			foreach ($toothStatuses as $status) {
				$toothStatus = $toothStatusCategory->statuses()
					->whereUuid($status)
					->firstOrFail();
				$toothStatus->order = $i++;
				$toothStatus->save();
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
