<?php

namespace App\Http\Controllers\Settings\ToothStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothStatus\Status\ToothStatusItemCreateRequest;
use App\Http\Requests\ToothStatus\Status\ToothStatusItemDeleteRequest;
use App\Http\Requests\ToothStatus\Status\ToothStatusItemUpdateRequest;
use App\Http\Requests\ToothStatus\Status\ToothStatusItemViewRequest;
use App\Http\Resources\ToothStatus\ToothStatusItemDetailResource;
use App\Models\Teeth\ToothStatus;
use App\Models\Teeth\ToothStatusCategory;
use App\Models\Teeth\ToothStatusConfiguration;
use Exception;
use Illuminate\Http\Response;

class ToothStatusItemController extends Controller
{
	public function show(ToothStatusItemViewRequest $request, ToothStatusCategory $toothStatusCategory, $toothStatusCategoryUuid)
	{
		try {
			$toothStatusCategory = $toothStatusCategory->statuses()
				->whereUuid($toothStatusCategoryUuid)
				->firstOrFail();

			return new ToothStatusItemDetailResource($toothStatusCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ToothStatusItemUpdateRequest $request, ToothStatusCategory $toothStatusCategory, $toothStatusUuid)
	{
		try {
			$toothStatusCategory = $toothStatusCategory->statuses()
				->whereUuid($toothStatusUuid)
				->firstOrFail();

			if (!$toothStatusCategory->deletable) {
				$toothConfiguration = ToothStatusConfiguration::whereUuid($request->get('display_type'))->firstOrFail();
				$toothStatusCategory->display_type = $toothConfiguration->id;
				$toothStatusCategory->has_surface = $request->get('has_surface', false);
				$toothStatusCategory->icon_text = $request->get('icon_text', false);
				$toothStatusCategory->name_cure = $request->get('name_cure', null);
				$toothStatusCategory->name_diagnostic = $request->get('name_diagnostic', null);
				$toothStatusCategory->name_scheduled = $request->get('name_scheduled', null);

				$toothStatusCategory->save();

				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ToothStatusItemCreateRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		try {
			$toothConfiguration = ToothStatusConfiguration::whereUuid($request->get('display_type'))->firstOrFail();

			$toothStatus = new ToothStatus;
			$toothStatus->tooth_status_category = $toothStatusCategory->id;
			$toothStatus->display_type = $toothConfiguration->id;
			$toothStatus->has_surface = $request->get('has_surface', false);
			$toothStatus->icon_text = $request->get('icon_text', false);
			$toothStatus->name_cure = $request->get('name_cure');
			$toothStatus->name_diagnostic = $request->get('name_diagnostic');
			$toothStatus->name_scheduled = $request->get('name_scheduled');

			if ($toothStatus->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ToothStatusItemDeleteRequest $request, ToothStatusCategory $toothStatusCategory, $toothStatusUuid)
	{
		try {
			$toothStatusCategory = $toothStatusCategory->statuses()
				->whereUuid($toothStatusUuid)
				->firstOrFail();

			if ($toothStatusCategory->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
