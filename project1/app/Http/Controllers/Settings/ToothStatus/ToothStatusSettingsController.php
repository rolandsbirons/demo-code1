<?php

namespace App\Http\Controllers\Settings\ToothStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothStatus\Settings\ToothStatusSettingsDeleteRequest;
use App\Http\Requests\ToothStatus\Settings\ToothStatusSettingsUpdateRequest;
use App\Http\Requests\ToothStatus\Settings\ToothStatusSettingsViewRequest;
use App\Http\Resources\ToothStatus\ToothStatusSettingsResource;
use App\Models\Teeth\ToothStatusCategory;
use Exception;
use Illuminate\Http\Response;

class ToothStatusSettingsController extends Controller
{
	public function show(ToothStatusSettingsViewRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		try {
			return new ToothStatusSettingsResource($toothStatusCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ToothStatusSettingsUpdateRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		try {
			$toothStatusCategory->name = $request->get('name');
			$toothStatusCategory->color = $request->get('color');
			$toothStatusCategory->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ToothStatusSettingsDeleteRequest $request, ToothStatusCategory $toothStatusCategory)
	{
		try {
			$toothStatusCategory->statuses()->delete();

			if ($toothStatusCategory->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
