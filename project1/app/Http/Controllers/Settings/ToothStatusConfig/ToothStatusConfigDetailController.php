<?php

namespace App\Http\Controllers\Settings\ToothStatusConfig;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothConfig\ToothStatusConfigDetailsViewRequest;
use App\Http\Requests\ToothConfig\ToothStatusConfigUpdateRequest;
use App\Http\Resources\ToothStatusConfig\ToothStatusConfigDetailResource;
use App\Models\Teeth\ToothStatusConfiguration;
use Exception;
use Illuminate\Http\Response;

class ToothStatusConfigDetailController extends Controller
{
	public function show(ToothStatusConfigDetailsViewRequest $request, ToothStatusConfiguration $toothStatusConfiguration)
	{
		try {
			return new ToothStatusConfigDetailResource($toothStatusConfiguration);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ToothStatusConfigUpdateRequest $request, ToothStatusConfiguration $toothStatusConfiguration)
	{
		try {
			$toothStatusConfiguration->configuration = $request->get('configuration');

			if ($toothStatusConfiguration->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
