<?php

namespace App\Http\Controllers\Settings\ToothStatusConfig;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothConfig\ToothStatusConfigCreateRequest;
use App\Http\Requests\ToothConfig\ToothStatusConfigViewRequest;
use App\Http\Resources\ToothStatusConfig\ToothStatusConfigListResource;
use App\Models\Teeth\ToothStatusConfiguration;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class ToothStatusConfigController extends Controller
{
	use SearchTrait;

	public function index(ToothStatusConfigViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'name',
					'uuid',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = ToothStatusConfiguration::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return ToothStatusConfigListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ToothStatusConfigCreateRequest $request)
	{
		try {
			$toothStatusCategory = new ToothStatusConfiguration;
			$toothStatusCategory->name = $request->get('name');
			$toothStatusCategory->active = true;
			$toothStatusCategory->configuration = [];

			if ($toothStatusCategory->save()) {
				return response()->json([
					'tooth_status_uuid' => $toothStatusCategory->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
