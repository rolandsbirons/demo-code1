<?php

namespace App\Http\Controllers\Settings\ToothStatusConfig;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothConfig\Settings\ToothStatusConfigSettingsDeleteRequest;
use App\Http\Requests\ToothConfig\Settings\ToothStatusConfigSettingsUpdateRequest;
use App\Http\Requests\ToothConfig\Settings\ToothStatusConfigSettingsViewRequest;
use App\Http\Resources\ToothStatusConfig\ToothStatusConfigSettingsResource;
use App\Models\Teeth\ToothStatusConfiguration;
use Exception;
use Illuminate\Http\Response;

class ToothStatusConfigSettingsController extends Controller
{
	public function show(ToothStatusConfigSettingsViewRequest $request, ToothStatusConfiguration $toothStatusConfiguration)
	{
		try {
			return new ToothStatusConfigSettingsResource($toothStatusConfiguration);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ToothStatusConfigSettingsUpdateRequest $request, ToothStatusConfiguration $toothStatusConfiguration)
	{
		try {
			$toothStatusConfiguration->name = $request->get('name');
			$toothStatusConfiguration->active = true;

			if ($toothStatusConfiguration->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ToothStatusConfigSettingsDeleteRequest $request, ToothStatusConfiguration $toothStatusConfiguration)
	{
		try {
			if ($toothStatusConfiguration->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
