<?php

namespace App\Http\Controllers\Settings\Services;

use App\Enums\ServiceGroupTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Service\Settings\ServiceSettingsDeleteRequest;
use App\Http\Requests\Service\Settings\ServiceSettingsUpdateRequest;
use App\Http\Requests\Service\Settings\ServiceSettingsViewRequest;
use App\Http\Resources\Service\ServiceSettingsResource;
use App\Models\Service\ServiceGroup;
use Exception;
use Illuminate\Http\Response;

class ServiceSettingsController extends Controller
{
	public function show(ServiceSettingsViewRequest $request, $type, ServiceGroup $serviceGroup, $categoryUuid)
	{
		try {
			$serviceDetails = $serviceGroup->categories()
				->whereUuid($categoryUuid)
				->firstOrFail();

			return new ServiceSettingsResource($serviceDetails);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ServiceSettingsUpdateRequest $request, $type, ServiceGroup $serviceGroup, $categoryUuid)
	{
		try {
			$serviceCategory = $serviceGroup->categories()
				->whereUuid($categoryUuid)
				->firstOrFail();

			if ($type === ServiceGroupTypes::GROUP_LOCAL_STRING) {
				$serviceCategory->name = $request->get('name');
			}

			$serviceCategory->color = $request->get('color');

			if ($serviceCategory->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ServiceSettingsDeleteRequest $request, $type, ServiceGroup $serviceGroup, $categoryUuid)
	{
		try {
			if ($type === ServiceGroupTypes::GROUP_LOCAL_STRING) {
				$serviceCategory = $serviceGroup->categories()
					->whereUuid($categoryUuid)
					->firstOrFail();

				$serviceCategory->services()->delete();

				if ($serviceCategory->delete()) {
					return response(null, Response::HTTP_OK);
				}
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
