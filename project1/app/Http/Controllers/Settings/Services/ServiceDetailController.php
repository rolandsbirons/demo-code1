<?php

namespace App\Http\Controllers\Settings\Services;

use App\Http\Controllers\Controller;
use App\Http\Requests\Service\ServiceDetailViewRequest;
use App\Http\Resources\Service\ServiceDetailResource;
use App\Models\Service\ServiceGroup;
use Exception;
use Illuminate\Http\Response;

class ServiceDetailController extends Controller
{
	public function show(ServiceDetailViewRequest $request, $type, ServiceGroup $serviceGroup, $categoryUuid)
	{
		try {
			$serviceDetails = $serviceGroup->categories()
				->whereUuid($categoryUuid)
				->with(['services'])
				->firstOrFail();

			return new ServiceDetailResource($serviceDetails);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
