<?php

namespace App\Http\Controllers\Settings\Services;

use App\Enums\BrandStyleTypes;
use App\Enums\ServiceGroupTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Service\ServiceCreateRequest;
use App\Http\Requests\Service\ServiceGroupsViewRequest;
use App\Http\Requests\Service\ServiceViewRequest;
use App\Http\Resources\Service\ServiceCategoryResource;
use App\Http\Resources\Service\ServiceGroupResource;
use App\Models\Service\ServiceCategory;
use App\Models\Service\ServiceGroup;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class ServiceController extends Controller
{
	use SearchTrait;

	public function groups(ServiceGroupsViewRequest $request, $type = null)
	{
		try {
			$systemType = $type === 'local' ? ServiceGroupTypes::GROUP_LOCAL : ServiceGroupTypes::GROUP_CUSTOM;

			$groups = ServiceGroup::where('group_type', $systemType)->get(['name', 'uuid']);

			return ServiceGroupResource::collection($groups);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function index(ServiceViewRequest $request, $type, ServiceGroup $serviceGroup)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'color',
					'name',
					'uuid',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = ServiceCategory::where('group_id', $serviceGroup->id)
				->whereHas('group', static function ($q) use ($type) {
//					$systemType = $type === 'local' ? ServiceGroupTypes::GROUP_LOCAL : ServiceGroupTypes::GROUP_CUSTOM;

//					$q->where('group_type', $systemType);
				});

			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return ServiceCategoryResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ServiceCreateRequest $request, $type, ServiceGroup $serviceGroup)
	{
		try {
			if ($type === ServiceGroupTypes::GROUP_LOCAL_STRING) {
				$serviceCategory = new ServiceCategory;
				$serviceCategory->name = $request->get('name');
				$serviceCategory->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;
				$serviceCategory->group_id = $serviceGroup->id;

				if ($serviceCategory->save()) {
					return response()->json([
						'service_category_uuid' => $serviceCategory->uuid,
					]);
				}
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
