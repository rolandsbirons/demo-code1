<?php

namespace App\Http\Controllers\Settings\Services;

use App\Enums\ServiceGroupTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Service\Item\ServiceItemCreateRequest;
use App\Http\Requests\Service\Item\ServiceItemDeleteRequest;
use App\Http\Requests\Service\Item\ServiceItemUpdateRequest;
use App\Http\Requests\Service\Item\ServiceItemViewRequest;
use App\Http\Resources\Service\ServiceItemResource;
use App\Models\Service\Service;
use App\Models\Service\ServiceGroup;
use Exception;
use Illuminate\Http\Response;

class ServiceItemController extends Controller
{
	public function show(ServiceItemViewRequest $request, $serviceType, ServiceGroup $serviceGroup, $categoryUuid, $serviceUuid)
	{
		try {
			$service = $this->getService($serviceUuid, $serviceGroup, $categoryUuid, $serviceType);

			return new ServiceItemResource($service);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ServiceItemCreateRequest $request, $serviceType, ServiceGroup $serviceGroup, $categoryUuid)
	{
		try {
			$serviceCategory = $serviceGroup->categories()
				->select('id')
				->whereUuid($categoryUuid)
				->firstOrFail();

			$service = new Service;
			$service->costs = $request->get('costs');
			$service->service_category = $serviceCategory->id;
			$service->service_name = $request->get('service_name');
			$service->service_price_from = $request->get('service_price_from');

			if ($request->has('service_price_to') && $request->get('service_price_to') > 0) {
				$service->service_price_to = $request->get('service_price_to');
			} else {
				$service->service_price_to = 0;
			}

			$service->service_time = $request->get('service_time');
			$service->service_tax = $request->get('service_tax');
			$service->service_unit = $request->get('service_unit');
			$service->sku = $request->get('sku');

			if ($service->save()) {
				return response()->json([
					'service_uuid' => $service->uuid,
					'status'       => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ServiceItemUpdateRequest $request, $serviceType, ServiceGroup $serviceGroup, $categoryUuid, $serviceUuid)
	{
		try {
			$service = $this->getService($serviceUuid, $serviceGroup, $categoryUuid, $serviceType);

			if ($serviceType === ServiceGroupTypes::GROUP_LOCAL_STRING) {
				$service->costs = $request->get('costs');
				$service->service_name = $request->get('service_name');
				$service->service_price_from = $request->get('service_price_from');

				if ($request->has('service_price_to') && $request->get('service_price_to') > 0) {
					$service->service_price_to = $request->get('service_price_to');
				} else {
					$service->service_price_to = 0;
				}

				$service->service_time = $request->get('service_time');
				$service->service_tax = $request->get('service_tax');
				$service->service_unit = $request->get('service_unit');
				$service->sku = $request->get('sku');
			} else {
				$service->service_time = $request->get('service_time');
			}

			$service->save();

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ServiceItemDeleteRequest $request, $serviceType, ServiceGroup $serviceGroup, $categoryUuid, $serviceUuid)
	{
		try {
			if ($serviceType === ServiceGroupTypes::GROUP_LOCAL_STRING) {
				$service = $this->getService($serviceUuid, $serviceGroup, $categoryUuid, $serviceType);

				if ($service->delete()) {
					return response()->json([
						'status' => 'success',
					]);
				}
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function getService($serviceUuid, $serviceGroup, $categoryUuid, $serviceType)
	{
		return Service::whereUuid($serviceUuid)
			->whereHas('category', static function ($q) use ($serviceGroup, $categoryUuid, $serviceType) {
				$q->where('uuid', $categoryUuid);

				$q->whereHas('group', static function ($q) use ($serviceGroup, $serviceType) {
					$systemType = $serviceType === 'local' ? ServiceGroupTypes::GROUP_LOCAL : ServiceGroupTypes::GROUP_CUSTOM;

					$q->where('id', $serviceGroup->id);
					$q->where('group_type', $systemType);
				});
			})->firstOrFail();
	}
}
