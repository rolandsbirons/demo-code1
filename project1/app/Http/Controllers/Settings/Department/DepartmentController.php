<?php

namespace App\Http\Controllers\Settings\Department;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Department\DepartmentCreateRequest;
use App\Http\Requests\Department\DepartmentViewRequest;
use App\Http\Resources\Department\DepartmentListResource;
use App\Models\Department\Department;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
	use SearchTrait;

	public function index(DepartmentViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'color',
					'name',
					'uuid',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = Department::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return DepartmentListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(DepartmentCreateRequest $request)
	{
		try {
			$department = new Department;
			$department->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;
			$department->deletable = true;
			$department->name = $request->get('name');

			if ($department->save()) {
				return response()->json([
					'uuid' => $department->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
