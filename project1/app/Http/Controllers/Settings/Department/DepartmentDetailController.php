<?php

namespace App\Http\Controllers\Settings\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\Department\DepartmentDetailViewRequest;
use App\Http\Requests\Department\DepartmentUpdateRequest;
use App\Http\Resources\Department\DepartmentDetailsResource;
use App\Models\Branch\Branch;
use App\Models\Department\Department;
use Exception;
use Illuminate\Http\Response;

class DepartmentDetailController extends Controller
{
	public function show(DepartmentDetailViewRequest $request, Department $department)
	{
		try {
			$department->load([
				'branches' => static function ($q) {
					$q->select('id', 'uuid');
				},
			]);

			return new DepartmentDetailsResource($department);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(DepartmentUpdateRequest $request, Department $department)
	{
		try {
			$branchIds = Branch::select('id')
				->whereIn('uuid', $request->branches ?? [])
				->get()
				->pluck('id');

			$branchRooms = $department->branches()->sync($branchIds);

			if ($branchRooms) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
