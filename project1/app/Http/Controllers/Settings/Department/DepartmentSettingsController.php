<?php

namespace App\Http\Controllers\Settings\Department;

use App\Http\Controllers\Controller;
use App\Http\Requests\Department\Settings\DepartmentSettingsDeleteRequest;
use App\Http\Requests\Department\Settings\DepartmentSettingsUpdateRequest;
use App\Http\Requests\Department\Settings\DepartmentSettingsViewRequest;
use App\Http\Resources\Department\DepartmentSettingResource;
use App\Models\Department\Department;
use Exception;
use Illuminate\Http\Response;

class DepartmentSettingsController extends Controller
{
	public function index(DepartmentSettingsViewRequest $request, Department $department)
	{
		try {
			return new DepartmentSettingResource($department);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(DepartmentSettingsUpdateRequest $request, Department $department)
	{
		try {
			$department->name = $request->get('name');
			$department->color = $request->get('color');
			$department->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(DepartmentSettingsDeleteRequest $request, Department $department)
	{
		try {
			if ($department->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
