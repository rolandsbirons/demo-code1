<?php

namespace App\Http\Controllers\Settings\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UsersDetailViewRequest;
use App\Http\Requests\User\UsersUpdateRequest;
use App\Http\Resources\User\UserDetailsResource;
use App\Models\User\User;
use Exception;
use Illuminate\Http\Response;

class UserDetailController extends Controller
{
	public function show(UsersDetailViewRequest $request, User $user)
	{
		try {
			return new UserDetailsResource($user);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(UsersUpdateRequest $request, User $user)
	{
		try {
			$user->certificate = $request->get('certificate');
			$user->certificate_date = $request->get('certificate_date');
			$user->email = $request->get('email');
			$user->personal_code = $request->get('personal_code');
			$user->phone = $request->get('phone');
			$user->sanitary_book = $request->get('sanitary_book');

			if ($user->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
