<?php

namespace App\Http\Controllers\Settings\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Settings\UserSettingsDeleteRequest;
use App\Http\Requests\User\Settings\UserSettingsUpdateRequest;
use App\Http\Requests\User\Settings\UserSettingsViewRequest;
use App\Http\Resources\User\UserSettingResource;
use App\Models\Branch\Branch;
use App\Models\User\Position;
use App\Models\User\User;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class UserSettingsController extends Controller
{
	public function show(UserSettingsViewRequest $request, User $user)
	{
		try {
			return new UserSettingResource($user);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(UserSettingsUpdateRequest $request, User $user)
	{
		try {
			$user->color = $request->get('color');
			$user->first_name = $request->get('first_name');
			$user->last_name = $request->get('last_name');
			$user->user_status = $request->get('user_status');
			$user->username = strtolower(Str::slug($request->get('username'), ''));

			if ($request->has('password') && \strlen($request->get('password')) >= 3) {
				$user->password = password_hash($request->input('password'), PASSWORD_DEFAULT);
			}

			if ($request->has('positions')) {
				$positionIds = Position::select('id')
					->whereIn('uuid', $request->get('positions'))
					->get()
					->pluck('id');

				$user->positions()->sync($positionIds);
			}

			if ($request->has('branches')) {
				$branchIds = Branch::select('id')
					->whereIn('uuid', $request->get('branches'))
					->get()
					->pluck('id');

				$user->branches()->sync($branchIds);
			}

			if ($user->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(UserSettingsDeleteRequest $request, User $user)
	{
		try {
			if ($user->deletable && $user->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
