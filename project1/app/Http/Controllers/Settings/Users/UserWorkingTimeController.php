<?php

namespace App\Http\Controllers\Settings\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\WorkingHours\UserWorkingHoursUpdateRequest;
use App\Http\Requests\User\WorkingHours\UserWorkingHoursViewRequest;
use App\Http\Resources\User\UserWorkTimeCollection;
use App\Models\User\User;
use App\Models\User\UserWorkTime;
use Exception;
use Illuminate\Http\Response;

class UserWorkingTimeController extends Controller
{
	public function show(UserWorkingHoursViewRequest $request, User $user)
	{
		try {
			return new UserWorkTimeCollection($user);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(UserWorkingHoursUpdateRequest $request, User $user)
	{
		try {
			$update = $this->parseWorkTime($user, $request->all());

			if ($update) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Validate and update all user working hours
	 *
	 * @param User  $user
	 * @param array $data
	 *
	 * @return bool
	 */

	private function parseWorkTime(User $user, array $data = []): bool
	{
		$userId = $user->id;

		foreach ($data as $time) {

			try {
				$currentDay = (int)$time['day'];

				UserWorkTime::where(['user_id' => $userId, 'id' => $time['id']])->update([
					'time_from' => (strtotime($time['time_from']) === false) ? '00:00' : $time['time_from'],
					'time_to'   => (strtotime($time['time_to']) === false) ? '00:00' : $time['time_to'],
					'active'    => (bool)$time['active'],
				]);

				UserWorkTime::where(['user_id' => $userId, 'day' => $currentDay, 'break' => true])->delete();

				if (isset($time['breaks'])) {
					foreach ($time['breaks'] as $break) {
						if ($break['time_from'] !== '00:00' && $break['time_to'] !== '00:00') {
							$userWorkTime = new UserWorkTime;
							$userWorkTime->user_id = $userId;
							$userWorkTime->day = $currentDay;
							$userWorkTime->time_from = (strtotime($break['time_from']) === false) ? '00:00' : $break['time_from'];
							$userWorkTime->time_to = (strtotime($break['time_to']) === false) ? '00:00' : $break['time_to'];
							$userWorkTime->active = 1;
							$userWorkTime->break = true;
							$userWorkTime->save();
						}
					}
				}
			} catch (Exception $e) {
				return false;
			}
		}

		return true;
	}
}
