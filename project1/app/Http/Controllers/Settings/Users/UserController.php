<?php

namespace App\Http\Controllers\Settings\Users;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Branch\UsersCreateRequest;
use App\Http\Requests\User\UsersViewRequest;
use App\Http\Resources\Branch\UserListResource;
use App\Models\User\User;
use App\Models\User\UserWorkTime;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class UserController extends Controller
{
	use SearchTrait;

	public function index(UsersViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'first_name',
					'last_name',
					'uuid',
					'color',
				],
				'searchFields' => [
					'first_name' => 'first_name',
					'last_name'  => 'last_name',
				],
			];

			$searchEloquent = User::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return UserListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(UsersCreateRequest $request)
	{
		try {
			$user = new User;
			$user->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;
			$user->first_name = $request->get('first_name');
			$user->last_name = $request->get('last_name');
			$user->password = password_hash(Str::random(), PASSWORD_DEFAULT);
			$user->username = strtolower(Str::slug($request->get('username'), ''));

			if ($user->save()) {
				$this->createUserEmptyWorkingTimes($user);

				return response()->json([
					'uuid' => $user->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function createUserEmptyWorkingTimes(User $user)
	{
		try {
			$user->workTimes()->saveMany([
				new UserWorkTime(['day' => 1, 'active' => false]),
				new UserWorkTime(['day' => 2, 'active' => false]),
				new UserWorkTime(['day' => 3, 'active' => false]),
				new UserWorkTime(['day' => 4, 'active' => false]),
				new UserWorkTime(['day' => 5, 'active' => false]),
				new UserWorkTime(['day' => 6, 'active' => false]),
				new UserWorkTime(['day' => 7, 'active' => false]),
			]);

			return true;
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
