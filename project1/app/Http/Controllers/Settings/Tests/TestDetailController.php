<?php

namespace App\Http\Controllers\Settings\Tests;

use App\Http\Controllers\Controller;
use App\Http\Requests\Test\Questions\TestQuestionOrderUpdate;
use App\Http\Requests\Test\TestDetailViewRequest;
use App\Http\Resources\Test\TestDetailsResource;
use App\Models\Patient\Test\PatientTestCategory;
use Exception;
use Illuminate\Http\Response;

class TestDetailController extends Controller
{
	public function show(TestDetailViewRequest $request, PatientTestCategory $patientTestCategory)
	{
		try {
			$patientTestCategory->with([
				'questionsMain',
				'questionsMain.parent',
			]);

			return new TestDetailsResource($patientTestCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function order(TestQuestionOrderUpdate $request, PatientTestCategory $patientTestCategory)
	{
		$testQuestions = $request->get('questions', []);

		try {
			$orderIndex = 1;
			$parent = null;

			if ($request->has('parent_uuid')) {
				$parent = $patientTestCategory
					->questionsCategory()
					->whereUuid($request->get('parent_uuid'))
					->firstOrFail()
					->id;
			}

			foreach ($testQuestions as $questionUuid) {
				if (empty($parent)) {
					$question = $patientTestCategory
						->questionsCategory()
						->whereUuid($questionUuid)
						->firstOrFail();
				} else {
					$question = $patientTestCategory
						->questionsParent($parent)
						->whereUuid($questionUuid)
						->firstOrFail();
				}

				$question->order = $orderIndex++;
				$question->save();
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
