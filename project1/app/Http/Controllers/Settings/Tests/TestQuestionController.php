<?php

namespace App\Http\Controllers\Settings\Tests;

use App\Http\Controllers\Controller;
use App\Http\Requests\Test\Questions\TestQuestionCreateRequest;
use App\Http\Requests\Test\Questions\TestQuestionUpdateRequest;
use App\Http\Requests\Test\Questions\TestQuestionViewRequest;
use App\Http\Resources\Test\TestQuestionResource;
use App\Models\Patient\Test\PatientTestCategory;
use App\Models\Patient\Test\PatientTestQuestion;
use Exception;
use Illuminate\Http\Response;

class TestQuestionController extends Controller
{
	public function show(TestQuestionViewRequest $request, PatientTestCategory $patientTestCategory, $questionUuid)
	{
		try {
			$question = $patientTestCategory->questionsAll()
				->whereUuid($questionUuid)
				->firstOrFail();

			return new TestQuestionResource($question);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(TestQuestionCreateRequest $request, PatientTestCategory $patientTestCategory)
	{
		try {
			$patientQuestion = new PatientTestQuestion;
			$patientQuestion->question_category = $patientTestCategory->id;
			$patientQuestion->trigger = $request->get('trigger');
			$patientQuestion->comment = $request->get('comment');
			$patientQuestion->test_value = $request->get('test_value');
			$patientQuestion->test_type = $request->get('test_type');

			if ($request->has('parent_uuid')) {
				$parentUuid = $request->get('parent_uuid');

				$questionQuery = PatientTestQuestion::whereUuid($parentUuid)->firstOrFail();
				$lastParentTest = PatientTestQuestion::whereUuid($parentUuid)->orderBy('order', 'DESC')->first();

				$patientQuestion->parent_id = $questionQuery->id;

				if ($lastParentTest) {
					$order = $lastParentTest->order + 1;
				}
			} else {
				$lastTest = PatientTestQuestion::orderBy('order', 'DESC')->first();

				if ($lastTest) {
					$order = $lastTest->order + 1;
				}
			}

			$patientQuestion->order = $order ?? 0;

			if ($patientQuestion->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_GATEWAY);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(TestQuestionUpdateRequest $request, PatientTestCategory $patientTestCategory, $questionUuid)
	{
		try {
			$patientTestQuestion = $patientTestCategory
				->questionsAll()
				->whereUuid($questionUuid)
				->firstOrFail();

			$patientTestQuestion->comment = $request->get('comment', null);
			$patientTestQuestion->test_type = $request->get('test_type');
			$patientTestQuestion->test_value = $request->get('test_value');
			$patientTestQuestion->trigger = $request->get('trigger', null);

			if ($patientTestQuestion->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientTestCategory $patientTestCategory, $questionUuid)
	{
		try {
			$question = $patientTestCategory->questionsAll()
				->whereUuid($questionUuid)
				->firstOrFail();

			if ($question->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
