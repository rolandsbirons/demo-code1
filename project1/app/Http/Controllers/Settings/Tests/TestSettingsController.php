<?php

namespace App\Http\Controllers\Settings\Tests;

use App\Http\Controllers\Controller;
use App\Http\Requests\Test\Settings\TestSettingsDeleteRequest;
use App\Http\Requests\Test\Settings\TestSettingsUpdateRequest;
use App\Http\Requests\Test\Settings\TestSettingsViewRequest;
use App\Http\Resources\Test\TestSettingsResource;
use App\Models\Patient\Test\PatientTestCategory;
use Exception;
use Illuminate\Http\Response;

class TestSettingsController extends Controller
{
	public function show(TestSettingsViewRequest $request, PatientTestCategory $patientTestCategory)
	{
		try {
			return new TestSettingsResource($patientTestCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(TestSettingsUpdateRequest $request, PatientTestCategory $patientTestCategory)
	{
		try {
			$patientTestCategory->name = $request->get('name');
			$patientTestCategory->color = $request->get('color');

			if ($patientTestCategory->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_GATEWAY);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(TestSettingsDeleteRequest $request, PatientTestCategory $patientTestCategory)
	{
		try {
			$patientTestCategory->questionsAll()->delete();

			if ($patientTestCategory->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
