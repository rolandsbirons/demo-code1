<?php

namespace App\Http\Controllers\Settings\Tests;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Test\TestCreateRequest;
use App\Http\Requests\Test\TestViewRequest;
use App\Http\Resources\Patient\Tests\PatientTestCategoryResource;
use App\Models\Patient\Test\PatientTestCategory;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class TestController extends Controller
{
	use SearchTrait;

	public function index(TestViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'name',
					'uuid',
					'color',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = PatientTestCategory::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return PatientTestCategoryResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(TestCreateRequest $request)
	{
		try {
			$questionCategory = new PatientTestCategory;
			$questionCategory->name = $request->get('name');
			$questionCategory->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;

			if ($questionCategory->save()) {
				return response()->json([
					'test_uuid' => $questionCategory->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_GATEWAY);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
