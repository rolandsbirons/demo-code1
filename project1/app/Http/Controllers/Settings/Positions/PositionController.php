<?php

namespace App\Http\Controllers\Settings\Positions;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Position\PositionCreateRequest;
use App\Http\Requests\Position\PositionViewRequest;
use App\Http\Resources\Branch\PositionListResource;
use App\Models\User\Position;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class PositionController extends Controller
{
	use SearchTrait;

	public function index(PositionViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'name',
					'uuid',
					'color',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = Position::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return PositionListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(PositionCreateRequest $request)
	{
		try {
			$position = new Position;
			$position->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;
			$position->name = $request->input('name');
			$position->type = $request->input('type');

			if ($position->save()) {
				return response()->json([
					'role_uuid' => $position->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
