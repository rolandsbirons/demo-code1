<?php

namespace App\Http\Controllers\Settings\Positions;

use App\Helpers\PermissionHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Position\PositionDetailsViewRequest;
use App\Http\Requests\Position\PositionUpdateRequest;
use App\Http\Resources\Position\PositionDetailResource;
use App\Models\User\Position;
use Exception;
use Illuminate\Http\Response;

class PositionDetailsController extends Controller
{
	public function show(PositionDetailsViewRequest $request, Position $userPosition)
	{
		try {
			return new PositionDetailResource($userPosition);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PositionUpdateRequest $request, Position $userPosition)
	{
		try {
			$parsedPermissions = PermissionHelper::parseActivePermissions($request);

			$userPosition->permissions()->delete();
			$userPosition->permissions()->saveMany($parsedPermissions);

			if ($userPosition->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
