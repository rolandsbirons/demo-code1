<?php

namespace App\Http\Controllers\Settings\Positions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Position\Settings\PositionSettingsDeleteRequest;
use App\Http\Requests\Position\Settings\PositionSettingsUpdateRequest;
use App\Http\Requests\Position\Settings\PositionSettingsViewRequest;
use App\Http\Resources\Position\PositionSettingResource;
use App\Models\User\Position;
use Exception;
use Illuminate\Http\Response;

class PositionSettingsController extends Controller
{
	public function show(PositionSettingsViewRequest $request, Position $userPosition)
	{
		try {
			return new PositionSettingResource($userPosition);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PositionSettingsUpdateRequest $request, Position $userPosition)
	{
		try {
			$userPosition->color = $request->get('color');
			$userPosition->name = $request->get('name');
			$userPosition->type = $request->get('type');

			if ($userPosition->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PositionSettingsDeleteRequest $request, Position $userPosition)
	{
		try {
			if ($userPosition->deletable && $userPosition->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
