<?php

namespace App\Http\Controllers\Settings\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\Settings\ProductSettingsDeleteRequest;
use App\Http\Requests\Product\Settings\ProductSettingsUpdateRequest;
use App\Http\Requests\Product\Settings\ProductSettingsViewRequest;
use App\Http\Resources\Product\ProductSettingsResource;
use App\Models\Product\ProductCategory;
use Exception;
use Illuminate\Http\Response;

class ProductSettingsController extends Controller
{
	public function show(ProductSettingsViewRequest $request, ProductCategory $productCategory)
	{
		try {
			return new ProductSettingsResource($productCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ProductSettingsUpdateRequest $request, ProductCategory $productCategory)
	{
		try {
			$productCategory->name = $request->get('name');
			$productCategory->color = $request->get('color');

			if ($productCategory->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ProductSettingsDeleteRequest $request, ProductCategory $productCategory)
	{
		try {
			$productCategory->products()->delete();

			if ($productCategory->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
