<?php

namespace App\Http\Controllers\Settings\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\Item\ProductItemCreateRequest;
use App\Http\Requests\Product\Item\ProductItemDeleteRequest;
use App\Http\Requests\Product\Item\ProductItemUpdateRequest;
use App\Http\Requests\Product\Item\ProductItemViewRequest;
use App\Http\Resources\Product\ProductItemResource;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use Exception;
use Illuminate\Http\Response;

class ProductItemController extends Controller
{
	public function show(ProductItemViewRequest $request, ProductCategory $productCategory, $productUuid)
	{
		try {
			$product = $productCategory->products()
				->whereUuid($productUuid)
				->firstOrFail();

			return new ProductItemResource($product);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ProductItemCreateRequest $request, ProductCategory $productCategory)
	{
		try {
			$product = new Product;
			$product->costs = $request->get('costs');
			$product->product_category = $productCategory->id;
			$product->product_name = $request->get('product_name');
			$product->product_price = $request->get('product_price');
			$product->product_time = $request->get('product_time');
			$product->product_tax = $request->get('product_tax');
			$product->product_unit = $request->get('product_unit');
			$product->sku = $request->get('sku');

			if ($product->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ProductItemUpdateRequest $request, ProductCategory $productCategory, $productUuid)
	{
		try {
			$product = $productCategory->products()
				->whereUuid($productUuid)
				->firstOrFail();

			$product->costs = $request->get('costs');
			$product->product_name = $request->get('product_name');
			$product->product_price = $request->get('product_price');
			$product->product_time = $request->get('product_time');
			$product->product_tax = $request->get('product_tax');
			$product->product_unit = $request->get('product_unit');
			$product->sku = $request->get('sku');
			$product->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(ProductItemDeleteRequest $request, ProductCategory $productCategory, $productUuid)
	{
		try {
			$product = $productCategory->products()
				->whereUuid($productUuid)
				->firstOrFail();

			if ($product->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
