<?php

namespace App\Http\Controllers\Settings\Products;

use App\Enums\BrandStyleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Requests\Product\ProductViewRequest;
use App\Http\Resources\Product\ProductCategoryResource;
use App\Models\Product\ProductCategory;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class ProductController extends Controller
{
	use SearchTrait;

	public function index(ProductViewRequest $request)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'color',
					'name',
					'uuid',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = ProductCategory::select($searchOptions['fetchFields']);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return ProductCategoryResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(ProductCreateRequest $request)
	{
		try {
			$productCategory = new ProductCategory;
			$productCategory->name = $request->get('name');
			$productCategory->color = BrandStyleTypes::ELEMENT_DEFAULT_COLOR;

			if ($productCategory->save()) {
				return response()->json([
					'product_category_uuid' => $productCategory->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
