<?php

namespace App\Http\Controllers\Settings\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductDetailsViewRequest;
use App\Http\Resources\Product\ProductDetailResource;
use App\Models\Product\ProductCategory;
use Exception;
use Illuminate\Http\Response;

class ProductDetailController extends Controller
{
	public function show(ProductDetailsViewRequest $request, ProductCategory $productCategory)
	{
		try {
			return new ProductDetailResource($productCategory);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
