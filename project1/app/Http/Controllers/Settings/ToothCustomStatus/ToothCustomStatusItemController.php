<?php

namespace App\Http\Controllers\Settings\ToothCustomStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothCustomStatus\Status\ToothCustomStatusItemUpdateRequest;
use App\Http\Requests\ToothCustomStatus\Status\ToothCustomStatusItemViewSingleRequest;
use App\Http\Resources\ToothCustomStatus\ToothCustomStatusItemDetailResource;
use App\Models\Teeth\ToothStatusGroup;
use App\Models\Teeth\ToothStatusConfiguration;
use Exception;
use Illuminate\Http\Response;

class ToothCustomStatusItemController extends Controller
{
	public function show(ToothCustomStatusItemViewSingleRequest $request, ToothStatusGroup $toothCustomStatusGroup, $categoryUuid, $toothCustomStatusUuid)
	{
		try {
			$toothCustomStatusGroup = $toothCustomStatusGroup->load([
				'categories',
			])->categories()
				->whereUuid($categoryUuid)
				->firstOrFail()
				->statuses()
				->whereUuid($toothCustomStatusUuid)
				->firstOrFail();

			return new ToothCustomStatusItemDetailResource($toothCustomStatusGroup);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(ToothCustomStatusItemUpdateRequest $request,
	                       ToothStatusGroup $toothCustomStatusGroup,
	                       $categoryUuid,
	                       $toothCustomStatusUuid)
	{
		try {
			$toothCustomStatusGroup = $toothCustomStatusGroup->load([
				'categories',
			])->categories()
				->whereUuid($categoryUuid)
				->firstOrFail()
				->statuses()
				->whereUuid($toothCustomStatusUuid)
				->firstOrFail();

			if (!$toothCustomStatusGroup->deletable) {
				$toothConfiguration = ToothStatusConfiguration::whereUuid($request->get('display_type'))->firstOrFail();
				$toothCustomStatusGroup->display_type = $toothConfiguration->id;
				$toothCustomStatusGroup->has_surface = $request->get('has_surface', false);

				$toothCustomStatusGroup->save();

				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
