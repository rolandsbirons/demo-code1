<?php

namespace App\Http\Controllers\Settings\ToothCustomStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothCustomStatus\ToothCustomStatusGroupsViewRequest;
use App\Http\Requests\ToothCustomStatus\ToothCustomStatusViewRequest;
use App\Http\Resources\ToothCustomStatus\ToothCustomStatusCategoryResource;
use App\Http\Resources\ToothCustomStatus\ToothCustomStatusListResource;
use App\Models\Teeth\ToothStatusGroup;
use App\Models\Teeth\ToothStatusCategory;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class ToothCustomStatusController extends Controller
{
	use SearchTrait;

	public function groups(ToothCustomStatusGroupsViewRequest $request)
	{
		try {
			$toothCustomGroups = ToothStatusGroup::with('categories')->get();

			return ToothCustomStatusCategoryResource::collection($toothCustomGroups);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function index(ToothCustomStatusViewRequest $request, ToothStatusGroup $toothCustomStatusGroup)
	{
		try {
			$searchOptions = [
				'fetchFields'  => [
					'id',
					'name',
					'uuid',
					'color',
				],
				'searchFields' => [
					'name' => 'name',
				],
			];

			$searchEloquent = ToothStatusCategory::where('group_id', $toothCustomStatusGroup->id);
			$searchResults = $this->getUniversalSearchResults($searchEloquent, $searchOptions['searchFields']);

			return ToothCustomStatusListResource::collection($searchResults);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
