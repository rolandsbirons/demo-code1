<?php

namespace App\Http\Controllers\Settings\ToothCustomStatus;

use App\Http\Controllers\Controller;
use App\Http\Requests\ToothCustomStatus\ToothCustomStatusDetailsViewRequest;
use App\Http\Resources\ToothCustomStatus\ToothCustomStatusDetailResource;
use App\Models\Teeth\ToothStatusGroup;
use Exception;
use Illuminate\Http\Response;

class ToothCustomStatusDetailController extends Controller
{
	public function show(ToothCustomStatusDetailsViewRequest $request, ToothStatusGroup $toothCustomStatusGroup, $categoryUuid)
	{
		try {
			$toothCustomStatusGroup = $toothCustomStatusGroup->categories()
				->whereUuid($categoryUuid)
				->firstOrFail();

			return new ToothCustomStatusDetailResource($toothCustomStatusGroup);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
