<?php

namespace App\Http\Controllers\Patient\Tests;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Tests\PatientTestsCreateRequest;
use App\Http\Requests\Patient\Tests\PatientTestsSearchRequest;
use App\Http\Resources\Patient\Tests\PatientTestCategoryResource;
use App\Models\Patient\Patient;
use App\Models\Patient\Test\PatientTest;
use App\Models\Patient\Test\PatientTestAnswer;
use App\Models\Patient\Test\PatientTestCategory;
use Exception;
use Illuminate\Http\Response;

class PatientTestsController extends Controller
{
	public function show(PatientTestsSearchRequest $request, Patient $patient)
	{
		try {
			$searchString = request('search');

//			if ($searchString) {
//				$patient->whereHas('tests.question', function ($q) use ($searchString) {
//					$q->where('name', 'like', '%' . $searchString . '%');
//				});
//			}
			$patient->with([
				'tests' => static function ($q) {
					$q->select('id', 'uuid', 'name', 'color');
				},
			]);

			return response()->json([
				'data' => $patient->tests->transform(static function ($test) {
					return [
						'color' => $test->question->color,
						'name'  => $test->question->name,
						'uuid'  => $test->uuid,
					];
				}),
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(PatientTestsCreateRequest $request, Patient $patient)
	{
		try {
			$testQuery = PatientTestCategory::with(['questions'])->whereUuid($request->get('test_uuid'))->firstOrFail();

			$patientTest = new PatientTest;
			$patientTest->patient_id = $patient->id;
			$patientTest->category_id = $testQuery->id;

			if ($patientTest->save()) {

				$questions = $testQuery->questions->transform(static function ($question) use ($patientTest) {
					return [
						new PatientTestAnswer([
							'question_id'     => $question->id,
							'test_id'         => $patientTest->id,
							'question_answer' => ($question->test_type === 1) ? false : null,
						]),
					];
				})->flatten(1);

				$patientTest->answers()->saveMany($questions);

				return response()->json([
					'status'    => 'success',
					'test_uuid' => $patientTest->uuid,
				]);
			}

			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function availableTests()
	{
		try {
			return PatientTestCategoryResource::collection(PatientTestCategory::get());
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
