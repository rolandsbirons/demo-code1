<?php

namespace App\Http\Controllers\Patient\Tests;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Tests\PatientTestsApproveRequest;
use App\Http\Requests\Patient\Tests\PatientTestsDeleteRequest;
use App\Http\Requests\Patient\Tests\PatientTestsUpdateRequest;
use App\Http\Resources\Patient\Tests\PatientTestViewResource;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class PatientTestsDetailsController extends Controller
{
	public function index(Patient $patient, $questionUuid)
	{
		try {
			activity()
				->performedOn($patient)
				->withProperties([
					'page' => 'tests',
					'uuid' => $questionUuid,
				])
				->log('view');

			$testQuestion = $patient->tests()
				->with([
					'answers'          => static function ($q) {
						$q->select('id', 'uuid', 'test_id', 'question_id', 'question_answer');
					},
					'answers.question' => static function ($q) {
						$q->select('id', 'parent_id', 'test_value', 'test_type', 'comment', 'trigger', 'question_category', 'order')->ordered();
					},
					'question'         => static function ($q) {
						$q->select('id', 'name');
					},
				])->whereUuid($questionUuid)
				->firstOrFail();

			return new PatientTestViewResource($testQuestion);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientTestsUpdateRequest $request, Patient $patient, $questionUuid)
	{
		try {
			$questions = $request->get('questions', []);

			foreach ($questions as $question) {
				$patient->tests()->whereUuid($questionUuid)->firstOrFail()->answers()->where('uuid', $question['uuid'])
					->update([
						'question_answer' => ($question['question_type'] === 1) ? (bool)$question['answer'] : $question['answer'],
					]);
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function approve(PatientTestsApproveRequest $request, Patient $patient, $questionUuid)
	{
		try {
			$patientTest = $patient->tests()->whereUuid($questionUuid)->firstOrFail();
			$patientTest->approved_at = now()->toDateTimeString();

			if ($patientTest->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientTestsDeleteRequest $request, Patient $patient, $patientTestUuid)
	{
		try {
			$patientTest = $patient->tests()->whereUuid($patientTestUuid)->firstOrFail();

			if (empty($patientTest->approved_at)) {
				$patientTest->answers()->delete();

				if ($patientTest->delete()) {
					return response(null, Response::HTTP_OK);
				}

				return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
