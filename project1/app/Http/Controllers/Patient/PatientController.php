<?php

namespace App\Http\Controllers\Patient;

use App\Enums\TeethMapTypes;
use App\Helpers\DataValidationHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\PatientCreateRequest;
use App\Http\Requests\Patient\PatientTableSearchRequest;
use App\Http\Resources\Patient\PatientTableCollection;
use App\Http\Resources\Patient\PatientViewResource;
use App\Models\Branch\Branch;
use App\Models\Patient\Patient;
use App\Traits\PatientTrait;
use Exception;
use Illuminate\Http\Response;

class PatientController extends Controller
{
	use PatientTrait;

	public function index(PatientTableSearchRequest $request)
	{
		try {
			$patientFields = [
				'phone',
				'gender',
				'client_card',
				'first_name',
				'last_name',
				'personal_code',
				'email',
				'insurance',
			];

			$data = Patient::select('id',
				'client_card',
				'email',
				'first_name',
				'gender',
				'insurance',
				'last_name',
				'personal_code',
				'phone',
				'user_status',
				'uuid'
			)->with([
				'branches' => static function ($q) {
					$q->select('name');
				},
				'doctor'   => static function ($q) {
					$q->select('first_name', 'last_name');
				},
			]);

			if ($request->filled('globalSearch')) {
				$globalSearchValue = $request->get('globalSearch');
				unset($patientFields['first_name'], $patientFields['last_name']);

				$data->orWhereRaw("Concat(first_name,' ',last_name) LIKE '%{$globalSearchValue}%' ")
					->orWhereRaw("Concat(last_name,' ',first_name) LIKE '%{$globalSearchValue}%' ");

				foreach ($patientFields as $patientField) {
					$data->orWhere($patientField, 'like', '%' . $globalSearchValue . '%');
				}
			} else {
				foreach ($patientFields as $patientField) {
					if ($request->filled($patientField)) {
						$data->where($patientField, 'like', '%' . $request->get($patientField) . '%');
					}
				}
				if ($request->filled('phone')) {
					$data->where('phone', 'like', '%' . $request->get('phone') . '%');
					$data->orWhere('phone2', 'like', '%' . $request->get('phone') . '%');
				}

				if ($request->filled('user_status')) {
					$data->where('user_status', $request->get('user_status'));
				}

				if ($request->filled('branches')) {
					$data->whereHas('branches', static function ($q) use ($request) {
						$q->select('name');
						$q->whereUuid($request->get('branches'));
					});
				}

				if ($request->filled('doctor')) {
					$data->whereHas('doctor', static function ($q) use ($request) {
						$q->select('first_name', 'last_name');
						$q->whereUuid($request->get('doctor'));
					});
				}
			}

			$data->orderBy($request->get('orderField', 'uuid'), $request->get('orderBy', 'DESC'));

			$result = $data->simplePaginate($request->get('perPage', 20), ['*'], 'page', $request->get('page'));

			return (new PatientTableCollection($result))->additional([
				'meta' => [
					'has_more' => $result->hasMorePages(),
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function create()
	{
		try {
			return new PatientViewResource(collect(new Patient));
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(PatientCreateRequest $request)
	{
		try {
			$patient = new Patient;
			$patient->address = $request->get('address');
			$patient->client_card = $request->get('client_card');
			$patient->description = $request->get('comment');
			$patient->email = $request->get('email');
			$patient->first_name = $request->get('first_name');
			$patient->gender = $request->get('gender', 0);
			$patient->insurance = $request->get('insurance', 0);
			$patient->job = $request->get('job');
			$patient->last_name = $request->get('last_name');
			$patient->personal_code = $request->get('personal_code');
			$patient->phone = $request->get('phone');
			$patient->phone2 = $request->get('phone2');
			$patient->dob = $request->get('dob');
			$patient->user_status = 1;

			if ($request->has('personal_code') && !$request->filled('dob')) {
				$date = DataValidationHelper::personalCodeGetCarbonDate($request->get('personal_code'));

				if ($date) {
					$patient->dob = $date->toDateString();
				}
			}

			if ($patient->save()) {
				self::attachPatientUsers($patient, $request);

				$branchIds = Branch::select('id')
					->whereIn('uuid', $request->get('branches'))
					->get()
					->pluck('id');

				$patient->branches()->sync($branchIds);

				$newPlan = $patient->toothPlan()->create([
					'approved'      => true,
					'company_id'    => $patient->company_id,
					'is_default'    => true,
					'name'          => TeethMapTypes::DEFAULT_PLAN_NAME,
					'user_id'       => 0,
					'approved_date' => now()->toDateTimeString(),
				]);

				$newPlan->visits()->create([
					'company_id'   => $patient->company_id,
					'disabled'     => true,
					'visit_number' => 0,
				]);

				return response()->json([
					'status'       => 'success',
					'patient_uuid' => $patient->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
