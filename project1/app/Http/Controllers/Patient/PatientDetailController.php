<?php

namespace App\Http\Controllers\Patient;

use App\Helpers\DataValidationHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\PatientUpdateRequest;
use App\Http\Resources\Patient\PatientViewResource;
use App\Models\Branch\Branch;
use App\Models\Patient\Patient;
use App\Traits\PatientTrait;
use Exception;
use Illuminate\Http\Response;

class PatientDetailController extends Controller
{
	use PatientTrait;

	public function index(Patient $patient)
	{
		try {
			activity()
				->performedOn($patient)
				->withProperties(['page' => 'details'])
				->log('view');

			$patient->with([
				'doctor'     => static function ($q) {
					$q->select('uuid');
				},
				'branches'   => static function ($q) {
					$q->select('uuid');
				},
				'assistants' => static function ($q) {
					$q->select('uuid');
				},
			]);

			return new PatientViewResource($patient);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientUpdateRequest $request, Patient $patient)
	{
		try {
			$patient->address = $request->get('address');
			$patient->client_card = $request->get('client_card');
			$patient->description = $request->get('comment');
			$patient->email = $request->get('email');
			$patient->first_name = $request->get('first_name');
			$patient->gender = $request->get('gender', 0);
			$patient->insurance = $request->get('insurance', 0);
			$patient->job = $request->get('job');
			$patient->last_name = $request->get('last_name');
			$patient->personal_code = $request->get('personal_code');
			$patient->phone = $request->get('phone');
			$patient->phone2 = $request->get('phone2');
			$patient->dob = $request->get('dob');

			if ($request->has('personal_code') && !$request->has('dob')) {
				$date = DataValidationHelper::personalCodeGetCarbonDate($request->get('personal_code'));

				if ($date) {
					$patient->dob = $date->toDateString();
				}
			}

			$branchIds = Branch::select('id')
				->whereIn('uuid', request('branches'))
				->get()
				->pluck('id');

			if ($patient->save()) {
				self::attachPatientUsers($patient, $request);

				$patient->branches()->sync($branchIds);

				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
