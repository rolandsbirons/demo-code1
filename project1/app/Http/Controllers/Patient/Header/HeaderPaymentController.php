<?php

namespace App\Http\Controllers\Patient\Header;

use App\Enums\InvoiceTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Header\PatientHeaderInvoiceCreateRequest;
use App\Http\Resources\Patient\Header\PatientHeaderPaymentServiceCollection;
use App\Models\Patient\Invoice\PatientInvoice;
use App\Models\Patient\Invoice\PatientInvoiceItem;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethService;
use App\Traits\CompanyTrait;
use App\Traits\ToothProcedureTrait;
use Exception;
use Illuminate\Http\Response;

class HeaderPaymentController extends Controller
{
	use CompanyTrait, ToothProcedureTrait;

	public function index(Patient $patient, $appointmentUuid)
	{
		try {
			//Check if we can submit old services from another appointment
			$patient = $patient->whereHas('appointment', static function ($q) use ($appointmentUuid) {
				$q->whereUuid($appointmentUuid);
			})->with([
				'services' => static function ($q) {
					$q->select(
						'id',
						'patient_id',
						'price',
						'service_id',
						'tooth_id',
						'meta_history',
						'price',
						'time',
						'uuid')
						->where('done', true)
						->where('invoice_id', null)
						->with([
							'service'                => static function ($q) {
								$q->select(
									'id',
									'service_category',
									'service_name',
									'sku',
									'uuid'
								);
							},
							'service.category'       => static function ($q) {
								$q->select('id', 'group_id');
							},
							'service.category.group' => static function ($q) {
								$q->select('id', 'discount');
							},
							'tooth'                  => static function ($q) {
								$q->select(
									'id',
									'status_id',
									'surface1',
									'surface2',
									'surface3',
									'surface4',
									'surface5',
									'surface6',
									'surface7',
									'surface8',
									'tooth_number'
								);
							},
						]);
				},
			])->firstOrFail();

			return new PatientHeaderPaymentServiceCollection($patient->services);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(PatientHeaderInvoiceCreateRequest $request, Patient $patient)
	{
		try {
			$teethAffected = collect([]);
			$services = collect($request->get('services', []));

			if ($services->isNotEmpty()) {
				$patient->load([
					'services' => static function ($q) use ($services) {
						$q->whereIn('uuid', $services->pluck('uuid'))
							->where('done', true)
							->where('invoice_id', null)
							->with([
								'service'                => static function ($q) {
									$q->select(
										'id',
										'service_category',
										'service_name',
										'service_price_from',
										'service_price_to',
										'service_tax',
										);
								},
								'service.category'       => static function ($q) {
									$q->select('id', 'group_id');
								},
								'service.category.group' => static function ($q) {
									$q->select('id', 'discount');
								},
								'tooth'                  => static function ($q) {
									$q->select(
										'id',
										'status_id',
										'surface1',
										'surface2',
										'surface3',
										'surface4',
										'surface5',
										'surface6',
										'surface7',
										'surface8',
										'tooth_number',
										'uuid'
									);
									$q->with(['status' => static function ($q) {
										$q->select('id', 'name_diagnostic', 'latin_name');
									}]);
								},
							]);
					},
				]);

				$invoice = new PatientInvoice;
				$invoice->patient_id = $patient->id;
				$invoice->invoice_id = $this->generateCompanyInvoice();
				$invoice->invoice_due = now()->toDateString();

				$invoiceItems = [];

				if ($invoice->save()) {
					foreach ($patient->services as $teethService) {
						if (!$teethService->processed_at) {
							$teethService->processed_at = now()->toDateTimeString();
						}

						$teethService->invoice_id = $invoice->id;

						$requestObject = $services->where('uuid', $teethService->uuid)->first();

						$invoiceItems[] = $this->generateInvoiceItem($teethService, $invoice, $requestObject);

						if ($teethService->save()) {
							$teethAffected->push($teethService->tooth->uuid);
						}
					}

					$invoice->items()->saveMany($invoiceItems);

					//Trying to close all the tooth statuses
					$uniqueTeethAffected = $teethAffected->unique();

					if ($uniqueTeethAffected->isNotEmpty()) {
						$this->tryCloseToothStatus($uniqueTeethAffected);
					}

					return response()->json([
						'uuid' => $invoice->uuid,
					]);
				}
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function generateInvoiceItem(PatientTeethService $teethService, PatientInvoice $invoice, array $requestObject): PatientInvoiceItem
	{
		$descriptionSuffix = '';
		$descriptionPrefix = '';

		if ($teethService->tooth->status->latin_name) {
			$descriptionSuffix = " ($teethService->tooth->status->latin_name)";
		}

		if ($teethService->tooth->status->sku) {
			$descriptionPrefix = " ($teethService->tooth->status->sku)";
		}

		$discountStrict = $teethService->service->category->group->discount;
		$discount = $discountStrict ? 0 : $requestObject['discount'];

		return new PatientInvoiceItem([
			'description'      => $descriptionPrefix . $teethService->tooth->status->getTranslation('name_diagnostic', 'lv') . $descriptionSuffix,
			'discount'         => $discount,
			'discount_strict'  => $discountStrict,
			'invoice_id'       => $invoice->id,
			'name'             => optional($teethService->service)->service_name,
			'price'            => $teethService->price,
			'price_original'   => optional($teethService->service)->service_price_avg,
			'sku'              => optional($teethService->service)->sku,
			'sku_custom'       => $teethService->tooth->tooth_number,
			'sku_description'  => $requestObject['surfaces'],
			'tax'              => optional($teethService->service)->service_tax ?? 0,
			'service_id'       => optional($teethService->service)->id,
			'teeth_service_id' => $teethService->id,
			'type'             => InvoiceTypes::INVOICE_TYPE_SERVICE,
		]);
	}
}