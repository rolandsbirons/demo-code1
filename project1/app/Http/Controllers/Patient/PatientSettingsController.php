<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Settings\PatientSettingsDeleteRequest;
use App\Http\Requests\Patient\Settings\PatientSettingsUpdateRequest;
use App\Http\Resources\Patient\PatientViewSettingResource;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class PatientSettingsController extends Controller
{
	public function index(Patient $patient)
	{
		try {
			return new PatientViewSettingResource($patient);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientSettingsUpdateRequest $request, Patient $patient)
	{
		try {
			$patient->user_status = $request->get('user_status', 1);

			if ($patient->save()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientSettingsDeleteRequest $request, Patient $patient)
	{
		try {
			$patient->appointments()->delete();

			if ($patient->delete()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
