<?php

namespace App\Http\Controllers\Patient\Invoices;

use App\Enums\InvoiceTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Invoices\Method\PatientInvoiceMethodUpdateRequest;
use App\Http\Resources\Patient\Invoices\Item\InvoiceItemMethodResource;
use App\Models\Patient\Invoice\PatientInvoice;
use App\Models\Patient\Invoice\PatientInvoiceMethod;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class PatientInvoiceMethodController extends Controller
{
	public function show(Patient $patient, $invoiceUuid)
	{
		try {
			$patient->load(['invoice' => static function ($q) use ($invoiceUuid) {
				$q->whereUuid($invoiceUuid);
				$q->with([
					'methods' => static function ($q) {
						$q->select(
							'amount',
							'amount_paid',
							'id',
							'invoice_id',
							'invoice_sku',
							'status',
							'type',
							'uuid'
						);
					},
				]);
			}]);

			return InvoiceItemMethodResource::collection($patient->invoice->methods);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(Patient $patient, $invoiceUuid)
	{
		try {
			$invoice = PatientInvoice::whereUuid($invoiceUuid)->firstOrFail();
			$invoiceMethod = new PatientInvoiceMethod;
			$invoiceMethod->patient_id = $patient->id;
			$invoiceMethod->invoice_id = $invoice->id;
			$invoiceMethod->type = InvoiceTypes::INVOICE_TYPE_SERVICE;
			$invoiceMethod->invoice_payment = 1;
			$invoiceMethod->locked = false;
			$invoiceMethod->amount = 0;
			$invoiceMethod->amount_paid = 0;
			$invoiceMethod->status = 1;

			$invoiceMethod->save();

			return response()->json([
				'method_uuid' => $invoiceMethod->uuid,
				'status'      => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientInvoiceMethodUpdateRequest $request, Patient $patient, $invoiceUuid)
	{
		try {
			$patient->invoice()->select('id')->whereUuid($invoiceUuid)->firstOrFail();

			foreach ($request->get('methods') as $method) {
				$methodDb = PatientInvoiceMethod::whereUuid($method['uuid'])->firstOrFail();

				if (($method['type'] === InvoiceTypes::INVOICE_METHOD_BANK_TRANSFER) && !$methodDb['invoice_sku']) {
					$method['invoice_sku'] = $patient->id + 33 . '-' . Str::random(10);
				}

				//On basic request we need only to save the type
				if ($request->get('basic')) {
					$method = [
						'type' => $method['type'],
					];
				}

				$methodDb->fill($method);
				$methodDb->save();
			}

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
