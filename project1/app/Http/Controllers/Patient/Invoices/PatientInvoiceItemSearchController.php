<?php

namespace App\Http\Controllers\Patient\Invoices;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Invoices\Item\PatientInvoiceFastSearchRequest;
use App\Http\Resources\Component\ToothMap\ToothMapSearchProcedureResource;
use App\Http\Resources\Patient\Invoices\Item\InvoiceItemSearchServicesResource;
use App\Models\Service\Service;
use App\Models\Teeth\ToothStatus;
use Exception;
use Illuminate\Http\Response;

class PatientInvoiceItemSearchController extends Controller
{
	public function services(PatientInvoiceFastSearchRequest $request)
	{
		try {
			$services = Service::limit(30)
				->select('service_name', 'sku', 'uuid', 'price')
				->where(static function ($q) use ($request) {
					$q->orWhere('service_name', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('sku', 'like', '%' . $request->get('search_query') . '%');
				})->get();

			return InvoiceItemSearchServicesResource::collection($services);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function procedures(PatientInvoiceFastSearchRequest $request)
	{
		try {
			$services = ToothStatus::limit(30)
				->select(
					'name_diagnostic',
					'name_scheduled',
					'name_diagnostic',
					'sku',
					'uuid',
					'price'
				)->where(static function ($q) use ($request) {
					$q->orWhere('name_diagnostic', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('name_scheduled', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('name_diagnostic', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('name_cure', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('latin_name', 'like', '%' . $request->get('search_query') . '%')
						->orWhere('sku', 'like', '%' . $request->get('search_query') . '%');
				})
				->get();

			return ToothMapSearchProcedureResource::collection($services);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}