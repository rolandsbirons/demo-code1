<?php

namespace App\Http\Controllers\Patient\Invoices;

use App\Http\Controllers\Controller;
use App\Models\Patient\Invoice\PatientInvoice;
use App\Models\Patient\Patient;
use App\Traits\CompanyTrait;
use App\Traits\SearchTrait;
use Exception;
use Illuminate\Http\Response;

class PatientInvoiceController extends Controller
{
	use SearchTrait, CompanyTrait;

	public function index(Patient $patient)
	{
		try {
			$patient->with([
				'invoices' => static function ($q) {
					$q->select('id', 'uuid', 'invoice_id');
				},
			]);

			return response()->json([
				'data' => $patient->invoices->transform(static function ($test) {
					return [
						'color' => null,
						'name'  => $test->invoice_id,
						'uuid'  => $test->uuid,
					];
				}),
				'meta' => [
					'title' => $patient->full_name,
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}


	public function store(Patient $patient)
	{
		try {
			$invoice = new PatientInvoice;
			$invoice->patient_id = $patient->id;
			$invoice->invoice_id = $this->generateCompanyInvoice();
			$invoice->invoice_due = now()->addDays(7)->toDateString();

			if ($invoice->save()) {
				return response()->json([
					'status'       => 'success',
					'invoice_uuid' => $invoice->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
