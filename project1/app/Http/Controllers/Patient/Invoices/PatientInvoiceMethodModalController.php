<?php

namespace App\Http\Controllers\Patient\Invoices;

use App\Enums\InvoiceTypes;
use App\Http\Controllers\Controller;
use App\Http\Resources\Patient\Invoices\Item\InvoiceItemServiceModalResource;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use js\tools\numbers2words\Speller;

class PatientInvoiceMethodModalController extends Controller
{
	public function show(Patient $patient, $invoiceUuid, $methodUuid)
	{
		try {
			$patient->load([
				'invoice' => static function ($q) use ($invoiceUuid, $methodUuid) {
					$q->whereUuid($invoiceUuid)
						->select(
							'id',
							'locked',
							'patient_id',
							'uuid'
						)->with([
							'items' => static function ($q) use ($methodUuid) {
								$q->where(static function ($q) use ($methodUuid) {
									$q->where('method_id', null)
										->orWhereHas('method', static function ($q) use ($methodUuid) {
											$q->where('uuid', $methodUuid);
										});
								});
								$q->select(
									'count',
									'description',
									'discount',
									'discount_strict',
									'id',
									'invoice_id',
									'method_id',
									'name',
									'price',
									'price_insurance',
									'service_id',
									'sku',
									'sku_custom',
									'sku_description',
									'tax',
									'type',
									'uuid'
								);
							},
						]);
				},
			]);

			return new InvoiceItemServiceModalResource($patient->invoice);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(Patient $patient, $invoiceUuid, $methodUuid)
	{
		try {
			$patient->load([
				'invoice' => static function ($q) use ($invoiceUuid, $methodUuid) {
					$q->whereUuid($invoiceUuid);
					$q->with([
						'items'  => static function ($q) {
							$q->select(
								'count',
								'discount',
								'id',
								'invoice_id',
								'method_id',
								'price',
								'price_insurance',
								'type',
								'uuid'
							);
						},
						'method' => static function ($q) use ($methodUuid) {
							$q->where('uuid', $methodUuid);
							$q->select(
								'id',
								'invoice_id',
								'invoice_sku',
								'type'
							);
						},
					]);
				},
			]);

			$invoice = $patient->invoice;
			$total = 0;
			foreach ($invoice->items->whereIn('uuid', request()->all()) as $item) {
				if ($item->type === InvoiceTypes::INVOICE_TYPE_SERVICE) {
					if ($item->discount > 0) {
						$total += ($item->price / 100 * $item->discount) - $item->price_insurance;
					} else {
						$total += $item->price - $item->price_insurance;
					}
				} else {
					$total += $item->price * $item->count;
				}

				$item->method_id = $invoice->method->id;
				$item->save();
			}

			$unsetItems = $invoice->items
				->whereNotIn('uuid', request()->all())
				->where('method_id', $invoice->method->id);

			foreach ($unsetItems as $item) {
				$item->method_id = null;
				$item->save();
			}

			if ($invoice->method->type === 2 && !$invoice->method->invoice_sku) {
				$invoice->method->invoice_sku = $patient->id + 33 . '-' . Str::random(10);
			}

			$invoice->method->amount = $total;
			$invoice->method->save();

			return response()->json([
				'status' => 'success',
			]);

		} catch (Exception $e) {
			dd($e);
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(Patient $patient, $invoiceUuid, $methodUuid)
	{
		try {
			$patient->load([
				'invoice' => static function ($q) use ($invoiceUuid, $methodUuid) {
					$q->whereUuid($invoiceUuid);
					$q->with([
						'method' => static function ($q) use ($methodUuid) {
							$q->whereUuid($methodUuid);
							$q->with(['items']);
						},
					]);
				},
			]);

			$method = $patient->invoice->method;

			foreach ($method->items as $item) {
				$item->method_id = null;
				$item->save();
			}

			if ($method->delete()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function print(Patient $patient, $invoiceUuid, $methodUuid)
	{
		try {
			$patient->load([
				'invoice' => static function ($q) use ($invoiceUuid, $methodUuid) {
					$q->whereUuid($invoiceUuid);
					$q->with([
						'items'  => static function ($q) {
							$q->select(
								'count',
								'discount',
								'name',
								'description',
								'id',
								'invoice_id',
								'method_id',
								'price',
								'price_insurance',
								'type',
								'uuid'
							);
						},
						'method' => static function ($q) use ($methodUuid) {
							$q->where('uuid', $methodUuid);
							$q->select(
								'id',
								'invoice_id',
								'invoice_sku',
								'type',
								'amount'
							);
						},
					]);
				},
			]);

			$invoice = $patient->invoice;

			$totalPrice = [
				'total_vat'     => 0,
				'total_raw'     => $invoice->method->amount,
				'total_raw_tax' => $invoice->method->amount,
				'price_words'   => Speller::spellNumber($invoice->method->amount, Speller::LANGUAGE_LATVIAN),
			];

//			$pdf = PDF::loadView('pdf.invoices.default', compact('invoice', 'totalPrice'));

//			return $pdf->download('invoice.pdf');

//			return $pdf->stream('download.pdf');
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
