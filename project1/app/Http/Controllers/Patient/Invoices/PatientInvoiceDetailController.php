<?php

namespace App\Http\Controllers\Patient\Invoices;

use App\Enums\InvoiceTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Settings\PatientInvoiceUpdateRequest;
use App\Http\Resources\Patient\Invoices\PatientInvoiceDetailResource;
use App\Models\Patient\Invoice\PatientInvoiceItem;
use App\Models\Patient\Patient;
use App\Models\Product\Product;
use App\Models\Service\Service;
use Exception;
use Illuminate\Http\Response;

class PatientInvoiceDetailController extends Controller
{
	public function show(Patient $patient, $invoiceUuid)
	{
		try {
			$patient->load([
				'invoice' => static function ($q) use ($invoiceUuid) {
					$q->whereUuid($invoiceUuid)
						->select(
							'comment',
							'created_at',
							'id',
							'invoice_due',
							'invoice_id',
							'locked',
							'patient_id',
							'uuid'
						)->with([
							'items' => static function ($q) {
								$q->select(
									'count',
									'description',
									'discount',
									'discount_strict',
									'uuid',
									'id',
									'invoice_id',
									'name',
									'price',
									'price_insurance',
									'price_original',
									'service_id',
									'teeth_service_id',
									'sku',
									'sku_custom',
									'sku_description',
									'tax',
									'type'
								)->with([
									'teethService' => static function ($q) {

									},
									'service'      => static function ($q) {
										$q->select('id', 'service_category')
											->with([
												'category' => static function ($q) {
													$q->select('id', 'group_id')
														->with(['group' => static function ($q) {
															$q->select('id', 'uuid');
														}]);
												},
											]);
									},
								]);
							},
						]);
				},
			]);

			return new PatientInvoiceDetailResource($patient->invoice);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientInvoiceUpdateRequest $request, Patient $patient, $invoiceUuid)
	{
		try {
			$invoice = $patient->invoices()->whereUuid($invoiceUuid)->firstOrFail();
			$serviceItems = collect($request->get('services'));
			$productItems = collect($request->get('products'));
			$serviceItemsDelete = $request->get('delete_services');
			$productItemsDelete = $request->get('delete_products');
			$services = collect([]);
			$products = collect([]);

			if (count($serviceItemsDelete) > 0) {
				PatientInvoiceItem::whereIn('uuid', $serviceItemsDelete)->delete();
			}

			if (count($productItemsDelete) > 0) {
				PatientInvoiceItem::whereIn('uuid', $productItemsDelete)->delete();
			}

			foreach ($serviceItems as $serviceItem) {
				if ($serviceItem['saved'] === true) {
					$serviceRecord = PatientInvoiceItem::whereUuid($serviceItem['uuid'])->firstOrFail();
				} else {
					if (empty($serviceItem['name']) || $serviceItem['price'] === 0) {
						continue;
					}

					$serviceRecord = new PatientInvoiceItem;
					$serviceRecord->price_original = $serviceItem['price'];
					$serviceRecord->price = $serviceItem['price'];
					$serviceRecord->type = InvoiceTypes::INVOICE_TYPE_SERVICE;
					$serviceRecord->invoice_id = $invoice->id;

					if (isset($serviceItem['service_uuid'])) {
						$service = Service::select('id', 'service_tax')
							->whereUuid($serviceItem['service_uuid'])
							->firstOrFail();
						$serviceRecord->service_id = $service->id;
					}
				}

				$serviceRecord->name = $serviceItem['name'];
				$serviceRecord->description = $serviceItem['description'];
				$serviceRecord->sku = $serviceItem['sku'];
				$serviceRecord->sku_custom = $serviceItem['sku_custom'];
				$serviceRecord->sku_description = $serviceItem['sku_description'];
				$serviceRecord->tax = $serviceItem['service_tax'] ?? null;
				$serviceRecord->discount_strict = $serviceItem['discount_strict'] ?? null;

				if (!$invoice->locked) {
					$serviceRecord->price_insurance = $serviceItem['price_insurance'];
				}

				if ($serviceRecord->save()) {
					$services->push($serviceRecord->uuid);
				}
			}

			foreach ($productItems as $productItem) {
				if ($productItem['saved'] === true) {
					$productRecord = PatientInvoiceItem::whereUuid($productItem['uuid'])->firstOrFail();
				} else {
					if (empty($productItem['name']) || $productItem['price'] === 0) {
						continue;
					}

					$productRecord = new PatientInvoiceItem;
					$productRecord->price_original = $productItem['price'];
					$productRecord->price = $productItem['price'];
					$productRecord->type = InvoiceTypes::INVOICE_TYPE_PRODUCT;
					$productRecord->invoice_id = $invoice->id;

					if (isset($productItem['product_uuid'])) {
						$product = Product::select('id')
							->whereUuid($productItem['product_uuid'])
							->firstOrFail();
						$productRecord->product_id = $product->id;
					}
				}

				$productRecord->name = $productItem['name'];
				$productRecord->sku = $productItem['sku'];
				$productRecord->count = abs($productItem['count']);
				$productRecord->tax = $productItem['tax'] ?? null;

				if ($productRecord->save()) {
					$products->push($productRecord->uuid);
				}
			}

			$invoice->locked = true;

			if ($invoice->save()) {
				return response()->json([
					'products' => $products,
					'services' => $services,
					'status'   => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
