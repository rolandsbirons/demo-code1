<?php

namespace App\Http\Controllers\Patient\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\Dashboard\PatientDashboardAppointmentsRequest;
use App\Http\Resources\Patient\Dashboard\PatientDashboardAppointmentCollection;
use App\Models\Appointment;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class PatientDashboardAppointmentController extends Controller
{
	public function index(PatientDashboardAppointmentsRequest $request, Patient $patient)
	{
		try {
			$appointments = Appointment::where('patient_id', $patient->id)
				->with([
					'user'       => function ($q) {
						$q->select(
							'color',
							'first_name',
							'id',
							'last_name',
							'uuid'
						);
					},
					'room'       => function ($q) {
						$q->select(
							'color',
							'id',
							'name'
						);
					},
					'visit'      => function ($q) {
						$q->select('id', 'visit_number', 'plan_id');
					},
					'visit.plan' => function ($q) {
						$q->select('id', 'name');
					},
					'assistant'  => function ($q) {
						$q->select(
							'color',
							'first_name',
							'id',
							'last_name',
							'uuid'
						);
					},
				])->select(
					'assistant_id',
					'description',
					'event_date',
					'event_time_from',
					'event_time_to',
					'id',
					'patient_id',
					'plan_id',
					'price',
					'provider_id',
					'room_id',
					'status',
					'user_id',
					'uuid'
				)->orderBy('event_date', 'DESC')
				->orderBy('event_time_from', 'ASC');

			return new PatientDashboardAppointmentCollection($appointments->get());
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
