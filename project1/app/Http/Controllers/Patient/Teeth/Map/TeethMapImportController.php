<?php

namespace App\Http\Controllers\Patient\Teeth\Map;

use App\Enums\TeethMapTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Map\PatientToothMapImportRequest;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethStatus;
use Exception;
use Illuminate\Http\Response;

class TeethMapImportController extends Controller
{
	public function index(PatientToothMapImportRequest $request, Patient $patient)
	{
		try {
			$importType = $request->get('import_type');

			$patient->teethStatuses()->delete();

			if ($importType === 'children') {
				foreach (TeethMapTypes::TEETH_CHILDREN as $tooth) {
					$toothStatus = new PatientTeethStatus;
					$toothStatus->patient_id = $patient->id;
					$toothStatus->tooth_id = $tooth;
					$toothStatus->tooth_status = TeethMapTypes::TEETH_DEFAULT_STATUS_CHILDREN;
					$toothStatus->save();
				}

				foreach ([16, 17, 18, 26, 27, 28, 46, 47, 48, 36, 37, 38] as $tooth) {
					$toothStatus = new PatientTeethStatus;
					$toothStatus->patient_id = $patient->id;
					$toothStatus->tooth_id = $tooth;
					$toothStatus->tooth_status = 3;
					$toothStatus->save();
				}
			}

			if ($importType === 'adult') {
				foreach (TeethMapTypes::TEETH_ADULT as $tooth) {
					$toothStatus = new PatientTeethStatus;
					$toothStatus->patient_id = $patient->id;
					$toothStatus->tooth_id = $tooth;
					$toothStatus->tooth_status = TeethMapTypes::TEETH_DEFAULT_STATUS_ADULT;
					$toothStatus->save();
				}
			}

			$patient->tooth_map_done = true;
			$patient->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}