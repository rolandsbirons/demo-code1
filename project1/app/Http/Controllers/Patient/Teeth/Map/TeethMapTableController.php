<?php

namespace App\Http\Controllers\Patient\Teeth\Map;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Table\PatientToothTableUpdateRequest;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class TeethMapTableController extends Controller
{
	public function update(PatientToothTableUpdateRequest $request, Patient $patient)
	{
		try {
			$patientServices = collect($request->get('services', []))
				->groupBy('visit')
				->values();

			foreach ($patientServices as $visitIndex => $patientGroupedServices) {
				foreach ($patientGroupedServices as $groupedService) {
					$serviceTooth = $patient->services()
						->select('id', 'visit')
						->whereUuid($groupedService['uuid'])
						->whereNull('invoice_id')
						->first();

					//TODO: filter data in frontend
					if ($serviceTooth) {
						$serviceTooth->visit = $visitIndex + 1;
						$serviceTooth->save();
					}
				}
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}