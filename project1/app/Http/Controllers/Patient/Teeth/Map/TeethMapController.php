<?php

namespace App\Http\Controllers\Patient\Teeth\Map;

use App\Enums\TeethMapTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Map\PatientToothMapViewRequest;
use App\Http\Resources\Patient\TeethMap\PatientTeethMapViewResource;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class TeethMapController extends Controller
{
	public function index(PatientToothMapViewRequest $request, Patient $patient, $planUuid = null, $procedureType = null)
	{
		try {
			$planSelected = $patient->toothPlan()->whereUuid($planUuid)->firstOrFail();
			$showAllPlans = ($procedureType === 'treatment' && $planSelected->is_default);

			$patient->load([
				'teeth'         => static function ($q) use ($planSelected, $showAllPlans, $planUuid) {
					if (!$showAllPlans) {
						$q->where('plans', 'like', '%' . $planUuid . '%')
							->orWhere('plan_id', $planSelected->id);
					}

					$q->orderBy('id', 'DESC')
						->distinct('tooth_number')
						->select(
							'id',
							'patient_id',
							'approved_date',
							'done_after',
							'done_before',
							'finished',
							'id',
							'plans',
							'surface1',
							'surface2',
							'surface3',
							'surface4',
							'surface5',
							'surface6',
							'surface7',
							'surface8',
							'plan_id',
							'tooth_number',
							'uuid',
							'status_id'
						);

					$q->with([
						'status'                         => static function ($q) {
							$q->select(
								'display_type',
								'has_surface',
								'icon_text',
								'id',
								'name_cure',
								'name_diagnostic',
								'name_diagnostic',
								'name_scheduled',
								'tooth_status_category'
							)->with([
								'display' => static function ($q) {
									$q->where('active', true);
									$q->select(
										'active',
										'configuration',
										'id',
										'is_default',
										'name'
									);
								},
							])->withTrashed();
						},
						'toothServices'                  => static function ($q) {
							$q->select(
								'done',
								'id',
								'invoice_id',
								'service_id',
								'tooth_id',
								'uuid',
								'price',
								'time',
								'meta_history',
								'visit'
							);
						},
						'toothServices.service'          => static function ($q) {
							$q->select(
								'id',
								'service_name',
								'sku'
							)->withTrashed();
						},
						'toothServices.service.category' => static function ($q) {
							$q->select(
								'color',
								'id'
							)->withTrashed();
						},
					]);
				},
				'teethStatuses' => static function ($q) {
					$q->orderBy('id', 'DESC')
						->distinct('place_id');
//						->limit(TeethMapTypes::TEETH_MAP_TOOTH_PLACES);

					$q->select(
						'id',
						'patient_id',
						'tooth_id',
						'tooth_status'
					);
				},
			]);

			return new PatientTeethMapViewResource($patient);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
