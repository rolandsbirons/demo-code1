<?php

namespace App\Http\Controllers\Patient\Teeth\Plans;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanDeleteRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanUpdateRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanViewRequest;
use App\Http\Resources\Patient\Plans\PatientToothPlanResource;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class TeethPlanItemController extends Controller
{
	public function index(PatientToothPlanViewRequest $request, Patient $patient, $planUuid)
	{
		try {
			$result = $patient
				->toothPlan()
				->whereUuid($planUuid)
				->whereCanDelete(true)
				->firstOrFail();

			return new PatientToothPlanResource($result);
		} catch (Exception $e) {
			return abort(Response::HTTP_NOT_FOUND);
		}
	}

	public function update(PatientToothPlanUpdateRequest $request, Patient $patient, $planUuid)
	{
		try {
			$patient->toothPlan()
				->whereUuid($planUuid)
				->whereCanDelete(true)
				->update([
					'name' => request('name'),
				]);

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientToothPlanDeleteRequest $request, Patient $patient, $planUuid)
	{
		try {
			$delete = $patient
				->toothPlan()
				->whereUuid($planUuid)
				->whereCanDelete(true)
				->whereApproved(false)
				->delete();

			if ($delete) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}