<?php

namespace App\Http\Controllers\Patient\Teeth\Plans;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanCreateRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanOrderRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanToothRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanViewListRequest;
use App\Http\Resources\Patient\Plans\PatientToothPlanCollection;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethPlan;
use Exception;
use Illuminate\Http\Response;

class TeethPlanController extends Controller
{
	public function index(PatientToothPlanViewListRequest $request, Patient $patient)
	{
		try {
			$result = $patient->toothPlan()
				->ordered()
				->get();

			return new PatientToothPlanCollection($result);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store(PatientToothPlanCreateRequest $request, Patient $patient)
	{
		try {
			//There is always __DEFAULT__ plan, so no need to check
			$lastPlan = $patient->toothPlan()->ordered('DESC')->firstOrFail();

			$patientToothPlan = new PatientTeethPlan;
			$patientToothPlan->patient_id = $patient->id;
			$patientToothPlan->name = request('name');
			$patientToothPlan->order = $lastPlan->order + 1;

			if ($patientToothPlan->save()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function order(PatientToothPlanOrderRequest $request, Patient $patient)
	{
		try {
			$orderStart = 1;
			$planArray = request('plans', []);

			foreach ($planArray as $plan) {
				$patient->toothPlan()
					->whereUuid($plan)
					->whereCanDelete(true)
					->update([
						'order' => $orderStart++,
					]);
			}

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientToothPlanToothRequest $request, Patient $patient)
	{
		try {
			$procedure = $patient->teeth()
				->whereUuid($request->get('tooth_uuid'))
				->firstOrFail();

			$updatePlans = null;
			$requestPlans = collect($request->get('plans', []));

			if ($requestPlans->isNotEmpty()) {
				$updatePlans = $requestPlans;
			}

			$procedure->plans = $updatePlans;

			if ($procedure->save()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}