<?php

namespace App\Http\Controllers\Patient\Teeth\Plans;

use App\Enums\DateTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanApproveRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanCopyRequest;
use App\Http\Requests\Patient\ToothMap\Plans\PatientToothPlanSubmitRequest;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethPlan;
use App\Models\Patient\Teeth\PatientTeethPlanVisit;
use Exception;
use Illuminate\Http\Response;


class TeethPlanItemActionsController extends Controller
{
	public function submit(PatientToothPlanSubmitRequest $request, Patient $patient, $planUuid)
	{
		try {
			$planObject = PatientTeethPlan::whereUuid($planUuid)->firstOrFail();
			$patientToothList = $patient
				->teeth()
				->where('plan_id', $planObject->id)
				->get();

			foreach ($patientToothList as $patientTooth) {
				$unfinishedToothServices = $patientTooth->unFinishedServices()->get();

				foreach ($unfinishedToothServices as $unfinishedService) {
					if ($unfinishedService->done) {
//						$unfinishedService->saved = true;
						$unfinishedService->processed_at = now()->toDateTimeString();
						$unfinishedService->save();
					}
				}
			}

			return response()->json([
				'data' => [
					'status' => 'success',
				],
			]);

		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function approve(PatientToothPlanApproveRequest $request, Patient $patient, $planUuid)
	{
		try {
			//We can check update because approved is false.
			$toothPlan = $patient->toothPlan()
				->whereUuid($planUuid)
				->firstOrFail();

			//Why do we need to approve already approved tooth?
			if ($toothPlan->approved) {
				return abort(Response::HTTP_BAD_REQUEST);
			}

			$toothPlan->approved = true;
			$toothPlan->save();

			$statusesObject = $patient->teeth()->with(['services'])->where('plans', 'like', '%' . $planUuid . '%')->get();

			$servicesList = [];
			foreach ($statusesObject as $status) {
				foreach ($status->services as $inStatus) {
					$servicesList[] = $inStatus;
				}
			}

			$servicesList = collect($servicesList);
			$servicesVisitCount = $servicesList->sortBy('visit')->unique('visit')->count();

			for ($i = 1; $i <= $servicesVisitCount; $i++) {
				PatientTeethPlanVisit::create([
					'plan_id'      => $toothPlan->id,
					'visit_number' => $i++,
				]);
			}

			$patient->teeth()->where('plans', 'like', '%' . $planUuid . '%')->update([
				'plans'         => null,
				'plan_id'       => $toothPlan->id,
				'approved_date' => now(),
			]);

			return response()->json(['status' => 'success']);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function copy(PatientToothPlanCopyRequest $request, Patient $patient, $planUUid)
	{
		try {
			$patientPlan = $patient->toothPlan()->whereUuid($planUUid)->firstOrFail();

			if ($patientPlan->is_default) {
				return abort(Response::HTTP_FORBIDDEN);
			}

			$patientPlanCount = $patient->toothPlan()->count();

			$patientPlanCopy = $patientPlan->replicate();
			$patientPlanCopy->name .= ')' . now()->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME) . '(';
			$patientPlanCopy->order = $patientPlanCount + 1;
			$patientPlanCopy->approved = 0;

			if ($patientPlanCopy->save()) {
				return response()->json([
					'status' => 'success',
					'data'   => [
						'uuid' => $patientPlanCopy->uuid,
					],
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

}