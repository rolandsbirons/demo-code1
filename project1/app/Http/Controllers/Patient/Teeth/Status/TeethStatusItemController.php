<?php

namespace App\Http\Controllers\Patient\Teeth\Status;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Status\PatientToothStatusItemDeleteRequest;
use App\Http\Requests\Patient\ToothMap\Status\PatientToothStatusItemUpdateFixRequest;
use App\Http\Requests\Patient\ToothMap\Status\PatientToothStatusItemUpdateRequest;
use App\Models\Patient\Patient;
use Exception;
use Illuminate\Http\Response;

class TeethStatusItemController extends Controller
{
	public function update(PatientToothStatusItemUpdateRequest $request, Patient $patient, $statusUuid)
	{
		try {
			$surfaceElement = 'surface' . $request->get('surface_number');

			$patientTooth = $patient->teeth()
				->whereUuid($statusUuid)
				->firstOrFail();

			$patientTooth->{$surfaceElement} = !$patientTooth->{$surfaceElement};
			$patientTooth->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function fix(PatientToothStatusItemUpdateFixRequest $request, Patient $patient, $toothUuid, $appointmentUuid)
	{
		try {
			$appointment = $patient->appointment()->whereUuid($appointmentUuid)->firstOrFail();

			$serviceTooth = $patient->services()
				->whereNull('invoice_id')
				->whereUuid($toothUuid)
				->firstOrFail();

			$serviceTooth->done = !$request->get('status');

			if ($serviceTooth->done) {
				$serviceTooth->appointment_id = $appointment->id;
			}

			if ($serviceTooth->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientToothStatusItemDeleteRequest $request, Patient $patient, $statusUuid)
	{
		try {
			$patientTooth = $patient->teeth()
				->whereUuid($statusUuid)
				->where('finished', false)
				->firstOrFail();

			$toothServices = $patientTooth->services();

			if ($toothServices->count() > 0) {
				$toothServices->delete();
			}

			if ($patientTooth->delete()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}