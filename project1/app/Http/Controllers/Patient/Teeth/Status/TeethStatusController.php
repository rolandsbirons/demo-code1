<?php

namespace App\Http\Controllers\Patient\Teeth\Status;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Status\PatientToothStatusStoreRequest;
use App\Http\Requests\Patient\ToothMap\Status\PatientToothStatusUpdateRequest;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTooth;
use App\Models\Patient\Teeth\PatientTeethPlan;
use App\Models\Patient\Teeth\PatientTeethStatus;
use App\Models\Teeth\ToothStatus;
use Exception;
use Illuminate\Http\Response;

class TeethStatusController extends Controller
{
	public function store(PatientToothStatusStoreRequest $request, Patient $patient)
	{
		try {
			$planUuid = $request->get('plan_uuid');
			$toothStatusQuery = ToothStatus::whereUuid($request->get('status_uuid'))->firstOrFail();
			$plan = PatientTeethPlan::whereUuid($planUuid)->firstOrFail();

			$toothStatus = new PatientTooth;
			$toothStatus->patient_id = $patient->id;
			$toothStatus->status_id = $toothStatusQuery->id;
			$toothStatus->tooth_number = $request->get('tooth_id');
			$toothStatus->done_before = $request->get('done_before', false);
			$toothStatus->done_after = $request->get('done_after', false);
			$toothStatus->plans = [$planUuid];
			$toothStatus->plan_id = $plan->id;

			if ($toothStatus->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientToothStatusUpdateRequest $request, Patient $patient)
	{
		try {
			$patientToothStatus = new PatientTeethStatus;
			$patientToothStatus->patient_id = $patient->id;
			$patientToothStatus->tooth_id = $request->get('tooth_id');
			$patientToothStatus->tooth_status = $request->get('status');

			//Add security check if frontend validation fails

			if ($patientToothStatus->save()) {
				return response(null, Response::HTTP_OK);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
