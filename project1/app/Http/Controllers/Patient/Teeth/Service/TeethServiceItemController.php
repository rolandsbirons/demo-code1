<?php

namespace App\Http\Controllers\Patient\Teeth\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Service\PatientToothServiceDeleteRequest;
use App\Http\Requests\Patient\ToothMap\Service\PatientToothServiceStoreRequest;
use App\Http\Requests\Patient\ToothMap\Service\PatientToothServiceUpdateRequest;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTooth;
use App\Models\Patient\Teeth\PatientTeethService;
use App\Models\Service\Service;
use Exception;
use Illuminate\Http\Response;


class TeethServiceItemController extends Controller
{
	public function store(PatientToothServiceStoreRequest $request, Patient $patient, PatientTooth $patientTooth)
	{
		try {
			$toothService = Service::whereUuid($request->get('service_uuid'))->firstOrFail();

			if ($patientTooth->done_before && $patientTooth->approved_date) {
				return abort(Response::HTTP_NOT_ACCEPTABLE);
			}

			$toothProblem = new PatientTeethService;
			$toothProblem->tooth_id = $patientTooth->id;
			$toothProblem->patient_id = $patient->id;
			$toothProblem->service_id = $toothService->id;
			$toothProblem->price = $toothService->service_price_avg;
			$toothProblem->time = $toothService->service_time;
			$toothProblem->meta_history = [
				'price'      => $toothService->service_price_avg ?? null,
				'price_from' => $toothService->service_price_from ?? null,
				'price_to'   => $toothService->service_price_to ?? null,
				'sku'        => $toothService->sku ?? null,
				'time'       => $toothService->service_time ?? null,
			];

			if ($toothProblem->save()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PatientToothServiceUpdateRequest $request, Patient $patient, $serviceUuid)
	{
		try {
			$service = $patient->services()
				->whereUuid($serviceUuid)
				->whereNull('invoice_id')
				->firstOrFail();

			$service->price = $request->get('price');
			$service->time = date('H:i:00', mktime(0, $request->get('time')));

			if ($service->save()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(PatientToothServiceDeleteRequest $request, Patient $patient, $serviceUuid)
	{
		try {
			$service = $patient->services()
				->whereUuid($serviceUuid)
				->whereNull('invoice_id')
				->firstOrFail();

			if ($service->delete()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}