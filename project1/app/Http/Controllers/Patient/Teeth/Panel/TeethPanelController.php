<?php

namespace App\Http\Controllers\Patient\Teeth\Panel;

use App\Enums\TeethMapTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\ToothMap\Panel\PatientToothPanelUpdateRequest;
use App\Traits\TeethMap\TeethCacheTrait;
use App\Traits\UserSettingsTrait;
use Exception;
use Illuminate\Http\Response;

class TeethPanelController extends Controller
{
	use UserSettingsTrait, TeethCacheTrait;

	public function update(PatientToothPanelUpdateRequest $request)
	{
		try {
			$updateKey = null;
			$updateType = $request->get('modal_segment');
			$updateData = $request->get('modal_data');
			$updatePrefix = $request->get('modal_prefix');

			if ($updateType === 'services_shortcut' ||
				$updateType === 'procedure_shortcut') {
				$updateData = collect($updateData);
			}

			if ($updateType === 'services_shortcut') {
				$updateKey = TeethMapTypes::USER_TEETH_MAP_PANEL_SERVICES_SHORTCUTS;
			}

			if ($updateType === 'procedure_shortcut') {
				$updateKey = TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES_SHORTCUT;
			}

			if ($updateType === 'procedure_blocks') {
				$updateKey = TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES;
				$updateData = collect($updateData)->values()->toArray();
			}

			if (empty($updateKey)) {
				return abort(Response::HTTP_BAD_REQUEST);
			}

			$globalPrefix = $updateKey . $this->getGlobalCachePrefix($updatePrefix);

			$this->updateUserSettings($globalPrefix, [
				'list' => $updateData,
			]);

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}