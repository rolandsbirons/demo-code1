<?php

namespace App\Http\Controllers\Patient\Teeth\Panel;

use App\Enums\TeethMapTypes;
use App\Helpers\TeethMapHelper;
use App\Http\Controllers\Controller;
use App\Http\Resources\Patient\TeethMap\PatientTeethMapViewResource;
use App\Models\Teeth\ToothStatusGroup;
use App\Models\Teeth\ToothStatus;
use App\Models\Teeth\ToothStatusCategory;
use App\Traits\TeethMap\TeethCacheTrait;
use App\Traits\TeethMap\TeethStatusTrait;
use App\Traits\UserSettingsTrait;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;


class TeethPanelServiceController extends Controller
{
	use UserSettingsTrait, TeethStatusTrait, TeethCacheTrait;

	public function getProcedureGroupGrid(PatientTeethMapViewResource $request, $groupUuid = null)
	{
		try {
			$reload = false;

			$globalPrefix = $this->getGlobalCachePrefix($groupUuid);

			$procedureGridKey = TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES . $globalPrefix;
			$procedureShortcutKey = TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES_SHORTCUT . $globalPrefix;

			$userProcedures = collect($this->getUserSettings($procedureGridKey));
			$userShortcuts = collect($this->getUserSettings($procedureShortcutKey));

			if (!empty($groupUuid)) {
				if ($userProcedures->isEmpty()) {
					$this->importGlobalDefaultVisualSettings($groupUuid);

					$reload = true;
				}
			} else if ($userProcedures->isEmpty()) {
				$this->importLocalDefaultVisualSettings();

				$reload = true;
			}

			if ($reload) {
				$userProcedures = collect($this->getUserSettings($procedureGridKey));
				$userShortcuts = collect($this->getUserSettings($procedureShortcutKey));
			}

			return response()->json([
				'data' => [
					'status'    => [
						'list' => $this->getToothStatusGroupsCached($groupUuid),
					],
					'procedure' => [
						'list'      => $this->getToothDoctorProceduresCached($userProcedures, $groupUuid),
						'shortcuts' => $this->getShortcutProceduresCached($userShortcuts),
					],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function importGlobalDefaultVisualSettings($groupUuid)
	{
		try {
			$globalPrefix = $this->getGlobalCachePrefix($groupUuid);

			$globalToothObject = $this->getToothStatusGlobalGroupObject($groupUuid);
			$shortcutProcedures = $globalToothObject['categories']->pluck('uuid')->slice(0, TeethMapTypes::PANEL_PROCEDURE_GRID_COUNT);
			$categoriesCount = $shortcutProcedures->count();

			if ($categoriesCount !== 8) {
				$categoriesToAdd = TeethMapTypes::PANEL_PROCEDURE_GRID_COUNT - $categoriesCount;
				$categoriesNulled = array_fill(1, $categoriesToAdd, null);

				$shortcutProcedures = array_merge($shortcutProcedures->toArray(), $categoriesNulled);
			}

			$cacheGridKey = TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES . $globalPrefix;

			$update = $this->updateUserSettings($cacheGridKey, [
				'list' => $shortcutProcedures,
			]);

			if ($update) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function getToothStatusGroupsCached($groupUuid): array
	{
		if (empty($groupUuid)) {
			$toothStatusGroup = ToothStatusCategory::isLocal()->select('id', 'uuid', 'name')->get()->transform(static function ($group) {
				return [
					'id'   => Str::random(10),
					'uuid' => $group->uuid,
					'name' => $group->name,
				];
			})->toArray();
		} else {
			$toothStatusGlobalGroups = $this->getToothStatusGlobalGroupObject($groupUuid);
			$toothStatusGroup = $toothStatusGlobalGroups['categories']->transform(static function ($category) {
				return [
					'id'   => Str::random(10),
					'uuid' => $category['uuid'],
					'name' => $category['name'],
				];
			})->toArray();
		}

		return array_merge([['id' => 0, 'name' => '__EMPTY__', 'uuid' => null]], $toothStatusGroup);
	}

	private function importLocalDefaultVisualSettings()
	{
		try {
			$localPrefix = '_LOCAL';
			$visualToothStatusesGroups = ToothStatusCategory::isLocal()
				->select('uuid')
				->limit(TeethMapTypes::PANEL_PROCEDURE_GRID_COUNT)
				->get()
				->pluck('uuid')
				->toArray();

			$this->updateUserSettings(TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES . $localPrefix, [
				'list' => $visualToothStatusesGroups,
			]);

			$visualToothStatuses = ToothStatus::isLocal()
				->select('uuid')
				->where('display_type', '>', 0)
				->limit(TeethMapTypes::PANEL_PROCEDURE_SHORTCUT_COUNT)
				->get()
				->pluck('uuid')
				->toArray();

			$this->updateUserSettings(TeethMapTypes::USER_TEETH_MAP_PANEL_GRID_PROCEDURES_SHORTCUT . $localPrefix, [
				'list' => $visualToothStatuses,
			]);

			return true;
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function getToothDoctorProceduresCached($doctorProcedures, $groupUuid = null): array
	{
		$doctorProceduresRaw = $this->getToothStatusGroupCached($doctorProcedures, $groupUuid);

		return $this->formatDoctorProceduresConstructGrid($doctorProcedures, $doctorProceduresRaw);
	}

	private function getToothStatusGroupCached($doctorProcedures, $groupUuid = false)
	{
		if (!empty($groupUuid)) {
			return ToothStatusGroup::whereUuid($groupUuid)
				->with([
					'categories',
					'categories.statuses',
					'categories.statuses.display',
				])
				->firstOrFail()
				->categories->transform(function ($group) {
					return $this->formatToothStatusGroup($group);
				})->keyBy('uuid');
		}

		return ToothStatusCategory::with('statuses', 'statuses.display')->whereIn('uuid', $doctorProcedures)->get()->transform(function ($group) {
			return $this->formatToothStatusGroup($group);
		})->keyBy('uuid');
	}

	private function formatDoctorProceduresConstructGrid($doctorProcedures, $doctorProceduresRaw): array
	{
		$doctorProceduresResult = [];
		$doctorProceduresOrder = 0;

		foreach ($doctorProcedures as $service) {
			$doctorProceduresResult[] = empty($service) ? [
				'name'     => null,
				'order'    => $doctorProceduresOrder++,
				'statuses' => null,
				'uuid'     => null,
			] : [
				'name'     => $doctorProceduresRaw[$service]['name'],
				'order'    => $doctorProceduresOrder++,
				'statuses' => $doctorProceduresRaw[$service]['statuses'],
				'uuid'     => $doctorProceduresRaw[$service]['uuid'],
			];
		}

		return $doctorProceduresResult;
	}

	private function formatToothStatusGroup($group): array
	{
		return [
			'name'     => $group->name,
			'statuses' => $group->statuses->transform(static function ($status) {
				return [
					'uuid'        => $status->uuid,
					'icon_text'   => $status->icon_text,
					'name_object' => [
						'treatment' => $status->name_diagnostic,
						'planned'   => $status->name_scheduled,
						'cured'     => $status->name_cure,
					],
					'style'       => [
						'uuid'          => $status->display->uuid, //check if needed
						'is_default'    => $status->display->is_default, //check if needed
						'configuration' => TeethMapHelper::filterInActiveConfiguration(optional(optional($status)->display)->configuration),
					],

				];
			})->keyBy('uuid')->toArray(),
			'uuid'     => $group->uuid,
		];
	}

	private function getShortcutProceduresCached($doctorProceduresShortcut)
	{
		return ToothStatus::with('display')
			->whereIn('uuid', $doctorProceduresShortcut)->get()->transform(static function ($procedure) {
				return [
					'icon_text' => $procedure->icon_text,
					'name_cure' => $procedure->name_diagnostic,
					'uuid'      => $procedure->uuid,
					'style'     => TeethMapHelper::filterInActiveConfiguration(optional($procedure->display)->configuration),
				];
			});
	}
}