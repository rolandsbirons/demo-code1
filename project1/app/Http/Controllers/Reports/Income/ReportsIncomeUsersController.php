<?php

namespace App\Http\Controllers\Reports\Income;

use App\Enums\AppointmentTypes;
use App\Enums\UserPermissionRoleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Report\Income\ReportIncomeUserRequest;
use App\Models\User\User;
use App\Traits\ReportsTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

class ReportsIncomeUsersController extends Controller
{
	use ReportsTrait;

	private static $dataFilters = [
		'empty_records_ignored',
	];

	private static $dataIgnore = [
		'name',
	];

	private static $currencyFields = [
		'appointments_avg_income',
		'appointments_hours_worked',
		'income_per_hour',
		'income_total',
	];

	private static $roundFields = [
		'appointments_finished',
	];

	private $totalCalculations = ['avg', 'sum'];

	public function index(ReportIncomeUserRequest $request)
	{
		try {
			$branch = $request->get('branch_uuid');
			$userType = (int)$request->get('user_type');

			$filters = [
				'date'   => self::getDateByType($request->get('view_type'), $request->get('date')),
				'branch' => $branch,
			];

			$calculations = $this->generateUserWorkloadReport($filters, $userType);

			return response()->json([
				'data' => $calculations['results'],
				'meta' => [
					'calculations' => $calculations['calculations'],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function generateUserWorkloadReport($filters, $userType): array
	{
		$query = $this->getUserWorkloadData($filters, $userType);
		$parseQueryData = $this->parseData($query);
		$results = $parseQueryData;

		foreach ($this->totalCalculations as $totalCalculation) {
			$calculation = self::generateReportsTotalFromData($parseQueryData, $totalCalculation, self::$dataIgnore, self::$dataFilters);

			$results = $results->merge($calculation);
		}

		$prepareResults = self::prepareData($results, [
			'currency' => self::$currencyFields,
			'round'    => self::$roundFields,
		]);

		return [
			'results'      => $prepareResults,
			'calculations' => $this->totalCalculations,
		];
	}

	private function getUserWorkloadData($filters, $userType)
	{
		$user = User::select('id', 'first_name', 'last_name');

		if ($userType === UserPermissionRoleTypes::ROLE_DOCTOR) {
			$user->with([
				'appointmentsProvider' => static function ($q) use ($filters) {
					$q->select(
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'provider_id',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);

					$q->whereHas('room', static function ($q) use ($filters) {
						$q->whereHas('branch', static function ($q) use ($filters) {
							$q->where('uuid', $filters['branch']);
						});
					});
				},
			]);
		}

		if ($userType === UserPermissionRoleTypes::ROLE_ASSISTANT) {
			$user->with([
				'appointmentsAssistant' => static function ($q) use ($filters) {
					$q->select(
						'assistant_id',
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);

					$q->whereHas('room', static function ($q) use ($filters) {
						$q->whereHas('branch', static function ($q) use ($filters) {
							$q->where('uuid', $filters['branch']);
						});
					});
				},
			]);
		}

		return $user->whereHas('branches', static function ($q) use ($filters) {
			$q->where('uuid', $filters['branch']);
		})->get()->transform(static function ($user) use ($userType) {
			$appointmentCollection = collect([]);

			if (($userType === UserPermissionRoleTypes::ROLE_DOCTOR) && $user->appointmentsProvider->isNotEmpty()) {
				$appointmentCollection = $appointmentCollection->merge($user->appointmentsProvider);
			}

			if (($userType === UserPermissionRoleTypes::ROLE_ASSISTANT) && $user->appointmentsAssistant->isNotEmpty()) {
				$appointmentCollection = $appointmentCollection->merge($user->appointmentsAssistant);
			}

			return collect([
				'name'         => $user->full_name,
				'appointments' => $appointmentCollection,
			]);
		});
	}

	private function parseData(Collection $data)
	{
		return $data->map(static function ($user) {
			$finishedAppointments = $user['appointments']->where('status', '>=', AppointmentTypes::APPOINTMENT_CLOSED_STATUS)
				->map(static function ($appointment) {
					$dateTimeFrom = Carbon::parse($appointment->event_time_from);
					$dateTimeTo = Carbon::parse($appointment->event_time_to);

					return [
						'appointment_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
						'price'            => $appointment->price,
					];
				});

			$appointmentsTotal = $finishedAppointments->count();

			if ($appointmentsTotal === 0) {
				return [
					'appointments_avg_income'   => '',
					'appointments_finished'     => '',
					'appointments_hours_worked' => '',
					'income_per_hour'           => 0.00,
					'income_total'              => 0.00,
					'name'                      => $user['name'],
				];
			}

			$appointmentsTotalPrice = $finishedAppointments->sum('price');
			$appointmentsTotalMinutes = $finishedAppointments->sum('appointment_time');
			$appointmentsTotalHours = abs($appointmentsTotalMinutes / 60);

			return [
				'appointments_avg_income'   => $appointmentsTotalPrice / $appointmentsTotalHours,
				'appointments_finished'     => $appointmentsTotal,
				'appointments_hours_worked' => $appointmentsTotalHours,
				'income_per_hour'           => $appointmentsTotalPrice / $appointmentsTotalHours,
				'income_total'              => $appointmentsTotalPrice,
				'name'                      => $user['name'],
			];
		});
	}
}
