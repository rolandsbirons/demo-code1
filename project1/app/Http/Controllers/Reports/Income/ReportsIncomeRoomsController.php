<?php

namespace App\Http\Controllers\Reports\Income;

use App\Enums\AppointmentTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Report\Income\ReportIncomeRoomRequest;
use App\Models\Branch\BranchRoom;
use App\Traits\ReportsTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

class ReportsIncomeRoomsController extends Controller
{
	use ReportsTrait;

	private static $dataFilters = [
		'empty_records_ignored',
	];

	private static $dataIgnore = [
		'name',
	];

	private static $currencyFields = [
		'appointments_avg_income',
		'appointments_hours_worked',
		'income_per_hour',
		'income_total',
	];

	private static $roundFields = [
		'appointments_finished',
	];

	private $totalCalculations = ['avg', 'sum'];

	public function index(ReportIncomeRoomRequest $request)
	{
		try {
			$branch = $request->get('branch_uuid');

			$filters = [
				'date'   => self::getDateByType($request->get('view_type'), $request->get('date')),
				'branch' => $branch,
			];

			$calculations = $this->generateBranchIncomeReport($filters);

			return response()->json([
				'data' => $calculations['results'],
				'meta' => [
					'calculations' => $calculations['calculations'],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function generateBranchIncomeReport($filters): array
	{
		$query = $this->getBranchIncomeData($filters);
		$parseQueryData = $this->parseBranchData($query);
		$results = $parseQueryData;

		foreach ($this->totalCalculations as $totalCalculation) {
			$calculation = self::generateReportsTotalFromData($parseQueryData, $totalCalculation, self::$dataIgnore, self::$dataFilters);

			$results = $results->merge($calculation);
		}

		$prepareResults = self::prepareData($results, [
			'currency' => self::$currencyFields,
			'round'    => self::$roundFields,
		]);

		return [
			'results'      => $prepareResults,
			'calculations' => $this->totalCalculations,
		];
	}

	private function getBranchIncomeData($filters)
	{
		return BranchRoom::select('id', 'name', 'branch_id')
			->where('hidden', 0)
			->whereHas('branch', static function ($q) use ($filters) {
				$q->where('uuid', $filters['branch']);
			})
			->with([
				'appointments' => static function ($q) use ($filters) {
					$q->select(
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'room_id',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);
				},
			])->get();
	}

	private function parseBranchData(Collection $data)
	{
		return $data->map(static function ($room) {
			$finishedAppointments = $room->appointments->where('status', '>=', AppointmentTypes::APPOINTMENT_CLOSED_STATUS)
				->map(static function ($appointment) {
					$dateTimeFrom = Carbon::parse($appointment->event_time_from);
					$dateTimeTo = Carbon::parse($appointment->event_time_to);

					return [
						'appointment_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
						'price'            => $appointment->price,
					];
				});

			$appointmentsTotal = $finishedAppointments->count();

			if ($appointmentsTotal === 0) {
				return [
					'appointments_avg_income'   => '',
					'appointments_finished'     => '',
					'appointments_hours_worked' => '',
					'income_per_hour'           => 0.00,
					'income_total'              => 0.00,
					'name'                      => $room['name'],
				];
			}

			$appointmentsTotalPrice = $finishedAppointments->sum('price');
			$appointmentsTotalMinutes = $finishedAppointments->sum('appointment_time');
			$appointmentsTotalHours = abs($appointmentsTotalMinutes / 60);

			return [
				'appointments_avg_income'   => $appointmentsTotalPrice / $appointmentsTotalHours,
				'appointments_finished'     => $appointmentsTotal,
				'appointments_hours_worked' => $appointmentsTotalHours,
				'income_per_hour'           => $appointmentsTotalPrice / $appointmentsTotalHours,
				'income_total'              => $appointmentsTotalPrice,
				'name'                      => $room['name'],
			];
		});
	}
}
