<?php

namespace App\Http\Controllers\Reports\Income;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Response;

class ReportsIncomeSidebarController extends Controller
{
	public function index()
	{
		try {
			return response()->json([
				'data' => [
					[
						'name' => '__USERS_DOCTOR__',
						'uuid' => 'users-doctor',
						'path' => 'reports-income-users-doctor',
					],
					[
						'name' => '__USERS_ASSISTANT__',
						'uuid' => 'users-assistant',
						'path' => 'reports-income-users-assistant',
					],
					[
						'name' => '__BRANCHES_ROOMS__',
						'uuid' => 'rooms',
						'path' => 'reports-income-rooms',
					],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
