<?php

namespace App\Http\Controllers\Reports\Workload;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Response;

class ReportsWorkloadSidebarController extends Controller
{
	public function index()
	{
		try {
			return response()->json([
				'data' => [
					[
						'name' => '__USERS_DOCTOR__',
						'uuid' => 'users-doctor',
						'path' => 'reports-workload-users-doctor',
					],
					[
						'name' => '__USERS_ASSISTANT__',
						'uuid' => 'users-assistant',
						'path' => 'reports-workload-users-assistant',
					],
					[
						'name' => '__BRANCHES_ROOMS__',
						'uuid' => 'rooms',
						'path' => 'reports-workload-rooms',
					],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
