<?php

namespace App\Http\Controllers\Reports\Workload;

use App\Enums\AppointmentTypes;
use App\Enums\UserPermissionRoleTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Report\Workload\ReportWorkloadUserRequest;
use App\Models\User\User;
use App\Traits\ReportsTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

class ReportsWorkloadUsersController extends Controller
{
	use ReportsTrait;

	private static $dataIgnore = [
		'name',
	];

	private static $currencyFields = [
	];

	private static $roundFields = [
		'appointments_completed',
		'appointments_completed_coefficient',
		'appointments_planned',
		'appointments_worked_hours',
		'schedule_hours_planned',
		'workload_coefficient',
	];

	private static $ratioFields = [
		'hours_on_appointment',
	];

	private $totalCalculations = ['avg', 'sum'];

	public function index(ReportWorkloadUserRequest $request)
	{
		try {
			$branch = $request->get('branch_uuid');
			$userType = (int)$request->get('user_type');

			$filters = [
				'date'   => self::getDateByType($request->get('view_type'), $request->get('date')),
				'branch' => $branch,
			];

			$calculations = $this->generateUserIncomeReport($filters, $userType);

			return response()->json([
				'data' => $calculations['results'],
				'meta' => [
					'calculations' => $calculations['calculations'],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function generateUserIncomeReport($filters, $userType): array
	{
		$query = $this->getUserIncomeData($filters, $userType);
		$parseQueryData = $this->parseData($query);
		$results = $parseQueryData;

		foreach ($this->totalCalculations as $totalCalculation) {
			$calculation = self::generateReportsTotalFromData($parseQueryData, $totalCalculation, self::$dataIgnore);

			$results = $results->merge($calculation);
		}

		$prepareResults = self::prepareData($results, [
			'currency' => self::$currencyFields,
			'round'    => self::$roundFields,
			'ratio'    => self::$ratioFields,
		]);

		return [
			'results'      => $prepareResults,
			'calculations' => $this->totalCalculations,
		];
	}

	private function getUserIncomeData($filters, $userType)
	{
		$user = User::select('id', 'first_name', 'last_name');

		if ($userType === UserPermissionRoleTypes::ROLE_DOCTOR) {
			$user->with([
				'appointmentsProvider' => static function ($q) use ($filters) {
					$q->select(
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'provider_id',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);

					$q->whereHas('room', static function ($q) use ($filters) {
						$q->whereHas('branch', static function ($q) use ($filters) {
							$q->where('uuid', $filters['branch']);
						});
					});
				},
			]);
		}

		if ($userType === UserPermissionRoleTypes::ROLE_ASSISTANT) {
			$user->with([
				'appointmentsAssistant' => static function ($q) use ($filters) {
					$q->select(
						'assistant_id',
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);

					$q->whereHas('room', static function ($q) use ($filters) {
						$q->whereHas('branch', static function ($q) use ($filters) {
							$q->where('uuid', $filters['branch']);
						});
					});
				},
			]);
		}

		return $user->with([
			'schedules' => static function ($q) use ($filters) {
				$q->where('protected', 0);
				$q->whereBetween('schedule_date', [$filters['date']['from'], $filters['date']['to']]);
			},
		])->whereHas('branches', static function ($q) use ($filters) {
			$q->where('uuid', $filters['branch']);
		})->get()->transform(static function ($user) use ($userType) {
			$appointmentCollection = collect([]);

			if (($userType === UserPermissionRoleTypes::ROLE_DOCTOR) && $user->appointmentsProvider->isNotEmpty()) {
				$appointmentCollection = $appointmentCollection->merge($user->appointmentsProvider);
			}

			if (($userType === UserPermissionRoleTypes::ROLE_ASSISTANT) && $user->appointmentsAssistant->isNotEmpty()) {
				$appointmentCollection = $appointmentCollection->merge($user->appointmentsAssistant);
			}

			return collect([
				'appointments' => $appointmentCollection,
				'name'         => $user->full_name,
				'schedules'    => $user->schedules,
			]);
		});
	}

	private function parseData(Collection $data)
	{
		return $data->map(static function ($user) {
			$workloadSchedule = $user['schedules']->map(static function ($appointment) {
				$dateTimeFrom = Carbon::parse($appointment->time_from);
				$dateTimeTo = Carbon::parse($appointment->time_to);

				return [
					'schedule_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
				];
			});

			$completedAppointments = $user['appointments']->where('status', '>=', AppointmentTypes::APPOINTMENT_CLOSED_STATUS)
				->map(static function ($appointment) {
					$dateTimeFrom = Carbon::parse($appointment->event_time_from);
					$dateTimeTo = Carbon::parse($appointment->event_time_to);

					return [
						'appointment_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
					];
				});

			$appointmentsPlannedTotal = $user['appointments']->count();
			$completedAppointmentsTotal = $completedAppointments->count();
			$scheduleWorkloadMinutes = $workloadSchedule->sum('schedule_time');
			$scheduleWorkloadTotalHours = abs($scheduleWorkloadMinutes / 60);
			$completedAppointmentsMinutes = $completedAppointments->sum('appointment_time');
			$completedAppointmentsHours = abs($completedAppointmentsMinutes / 60);

			return [
				'appointments_completed'             => $completedAppointmentsTotal,
				'appointments_completed_coefficient' => ($completedAppointmentsTotal > 0 && $appointmentsPlannedTotal > 0) ? $completedAppointmentsTotal / $appointmentsPlannedTotal * 100 : 0,
				'appointments_planned'               => $appointmentsPlannedTotal,
				'appointments_worked_hours'          => $completedAppointmentsHours,
				'hours_on_appointment'               => ($completedAppointmentsHours > 0) ? $completedAppointmentsHours / $completedAppointmentsTotal : 0,
				'name'                               => $user['name'],
				'schedule_hours_planned'             => $scheduleWorkloadTotalHours,
				'workload_coefficient'               => ($completedAppointmentsHours > 0 && $scheduleWorkloadTotalHours) ? $completedAppointmentsHours / $scheduleWorkloadTotalHours * 100 : 0,
			];
		});
	}
}
