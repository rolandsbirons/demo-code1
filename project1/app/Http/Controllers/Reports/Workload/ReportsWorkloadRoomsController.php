<?php

namespace App\Http\Controllers\Reports\Workload;

use App\Enums\AppointmentTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Report\Workload\ReportWorkloadRoomRequest;
use App\Models\Branch\BranchRoom;
use App\Traits\ReportsTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

class ReportsWorkloadRoomsController extends Controller
{
	use ReportsTrait;

	private static $dataIgnore = [
		'name',
	];

	private static $currencyFields = [
	];

	private static $roundFields = [
		'appointments_completed',
		'appointments_completed_coefficient',
		'appointments_planned',
		'appointments_worked_hours',
		'schedule_hours_planned',
		'workload_coefficient',
	];

	private static $ratioFields = [
		'hours_on_appointment',
	];

	private $totalCalculations = ['avg', 'sum'];

	public function index(ReportWorkloadRoomRequest $request)
	{
		try {
			$branch = $request->get('branch_uuid');

			$filters = [
				'date'   => self::getDateByType($request->get('view_type'), $request->get('date')),
				'branch' => $branch,
			];

			$calculations = $this->generateUserWorkloadReport($filters);

			return response()->json([
				'data' => $calculations['results'],
				'meta' => [
					'calculations' => $calculations['calculations'],
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function generateUserWorkloadReport($filters): array
	{
		$query = $this->getBranchWorkloadData($filters);
		$parseQueryData = $this->parseData($query);
		$results = $parseQueryData;

		foreach ($this->totalCalculations as $totalCalculation) {
			$calculation = self::generateReportsTotalFromData($parseQueryData, $totalCalculation, self::$dataIgnore);

			$results = $results->merge($calculation);
		}

		$prepareResults = self::prepareData($results, [
			'currency' => self::$currencyFields,
			'round'    => self::$roundFields,
			'ratio'    => self::$ratioFields,
		]);

		return [
			'results'      => $prepareResults,
			'calculations' => $this->totalCalculations,
		];
	}

	private function getBranchWorkloadData($filters)
	{
		return BranchRoom::select('id', 'name', 'branch_id')
			->where('hidden', 0)
			->whereHas('branch', static function ($q) use ($filters) {
				$q->where('uuid', $filters['branch']);
			})
			->with([
				'appointments' => static function ($q) use ($filters) {
					$q->select(
						'event_date',
						'event_time_from',
						'event_time_to',
						'price',
						'room_id',
						'status',
						'user_id',
						);
					$q->whereBetween('event_date', [$filters['date']['from'], $filters['date']['to']]);
				},
				'schedules'    => static function ($q) use ($filters) {
					$q->whereBetween('schedule_date', [$filters['date']['from'], $filters['date']['to']]);
				},
			])->get();
	}

	private function parseData(Collection $data)
	{
		return $data->map(static function ($room) {
			$workloadSchedule = $room['schedules']->map(static function ($appointment) {
				$dateTimeFrom = Carbon::parse($appointment->time_from);
				$dateTimeTo = Carbon::parse($appointment->time_to);

				return [
					'schedule_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
				];
			});

			$completedAppointments = $room['appointments']->where('status', '>=', AppointmentTypes::APPOINTMENT_CLOSED_STATUS)
				->map(static function ($appointment) {
					$dateTimeFrom = Carbon::parse($appointment->event_time_from);
					$dateTimeTo = Carbon::parse($appointment->event_time_to);

					return [
						'appointment_time' => $dateTimeTo->diffInMinutes($dateTimeFrom),
					];
				});

			$appointmentsPlannedTotal = $room['appointments']->count();
			$completedAppointmentsTotal = $completedAppointments->count();
			$scheduleWorkloadMinutes = $workloadSchedule->sum('schedule_time');
			$scheduleWorkloadTotalHours = abs($scheduleWorkloadMinutes / 60);
			$completedAppointmentsMinutes = $completedAppointments->sum('appointment_time');
			$completedAppointmentsHours = abs($completedAppointmentsMinutes / 60);

			return [
				'appointments_completed'             => $completedAppointmentsTotal,
				'appointments_completed_coefficient' => ($completedAppointmentsTotal > 0 && $appointmentsPlannedTotal > 0) ? $completedAppointmentsTotal / $appointmentsPlannedTotal * 100 : 0,
				'appointments_planned'               => $appointmentsPlannedTotal,
				'appointments_worked_hours'          => $completedAppointmentsHours,
				'hours_on_appointment'               => ($completedAppointmentsHours > 0) ? $completedAppointmentsHours / $completedAppointmentsTotal : 0,
				'name'                               => $room['name'],
				'schedule_hours_planned'             => $scheduleWorkloadTotalHours,
				'workload_coefficient'               => ($completedAppointmentsHours > 0 && $scheduleWorkloadTotalHours) ? $completedAppointmentsHours / $scheduleWorkloadTotalHours * 100 : 0,
			];
		});
	}
}
