<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use App\Http\Requests\Planner\PlannerViewUsersEventsRequest;
use App\Http\Resources\Planner\PlannerUserResource;
use App\Models\Branch\Branch;
use App\Traits\MetaFieldsTrait;
use Exception;
use Illuminate\Http\Response;

class PlannerUserController extends Controller
{
	use MetaFieldsTrait;

	public function index(PlannerViewUsersEventsRequest $request)
	{
		try {
			$users = collect([]);

			$branch = Branch::whereUuid($request->get('branch_uuid'))->firstOrFail();

			$userTypes = $request->get('user_type') === 3 ? [0, 3] : [$request->get('user_type')];

			$userIds = $this->getUsersInBranch($branch->id, $userTypes);

			if ($userIds->count() > 0) {
				$usersFound = $userIds->pluck('uuid');

				$branch = Branch::whereUuid($request->get('branch_uuid'))
					->whereHas('users', static function ($query) use ($usersFound) {
						$query->select('id', 'uuid', 'first_name', 'last_name', 'color')->whereIn('uuid', $usersFound);
					})
					->with([
						'users.workTimes' => static function ($query) {
							$query->select('user_id', 'day', 'time_from', 'time_to', 'active', 'break');
						},
						'users.schedules' => static function ($query) use ($request) {
							$query->select('id', 'user_id')->where('schedule_date', $request->get('date'));
						},
					])->firstOrFail();

				$users = $branch->users->whereIn('uuid', $userIds->pluck('uuid'));
			}

			return PlannerUserResource::collection($users);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
