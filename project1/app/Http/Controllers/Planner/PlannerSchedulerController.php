<?php

namespace App\Http\Controllers\Planner;

use App\Helpers\DateFormatHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Planner\PlannerSchedulerDeleteEventRequest;
use App\Http\Requests\Planner\PlannerSchedulerUpdateEventRequest;
use App\Http\Requests\Planner\PlannerViewEventRequest;
use App\Http\Resources\Planner\PlannerScheduleResource;
use App\Models\Branch\BranchSchedule;
use App\Models\Branch\BranchScheduleGroup;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class PlannerSchedulerController extends Controller
{
	public function index(PlannerViewEventRequest $request, BranchSchedule $schedule)
	{
		try {
			$schedule->with([
				'group',
				'user',
				'appointment' => static function ($q) {
					$q->select('id', 'uuid');
				},
			]);

			return new PlannerScheduleResource($schedule);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(PlannerSchedulerUpdateEventRequest $request, BranchSchedule $schedule)
	{
		$scheduleDate = Carbon::parse($request->get('schedule_date'));
		$scheduleToDate = Carbon::parse($request->get('schedule_to_date'));

		$eventHasBeenChanged = $request->get('event_has_changed', false);
		$eventTimeOnlyChanged = $request->get('event_time_changed', false);
		$eventUpdateType = $request->get('update_type');
		$scheduleRecurring = $request->get('recurring', false);
		$scheduleRepeatAfter = $request->get('repeat_after', 1);
		$scheduleType = $request->get('schedule_type');

		try {
			$scheduleRange = DateFormatHelper::carbonDateFromTo($scheduleDate, $scheduleToDate, true, true);

			$eventDateToInsert = [];
			$repeatValues = [];

			/*
			 * Check if  we create event group, then we allow to change time, if event group does not exist,
			 * then we does not allow to change event time
			 */

			$checkChanges = $eventHasBeenChanged && !$eventTimeOnlyChanged;

			if ($schedule->schedule_group === null) {
				$checkChanges = true;
			}

			if ($scheduleRecurring && $checkChanges) {
				if ($scheduleType === 'day') {
					$eventDateToInsert = DateFormatHelper::parseDayScheduler($scheduleRange, $scheduleRepeatAfter);
				} else if ($scheduleType === 'week') {
					$repeatValues = $request->get('week_repeat_days', []);
					$eventDateToInsert = DateFormatHelper::parseWeekScheduler($scheduleRange, $scheduleRepeatAfter, $repeatValues);
				}

				if ($eventDateToInsert->count() > 0) {

					$newGroupUuid = (string)Str::uuid();

					if ($schedule->schedule_group !== null) {
						BranchSchedule::where('id', '!=', $schedule->id)
							->where('schedule_group', $schedule->schedule_group)
							->delete();

						BranchScheduleGroup::where([
							'uuid' => $schedule->schedule_group,
						])->delete();
					}

					foreach ($eventDateToInsert as $event) {
						if ($event->toDateString() !== $schedule->schedule_date->toDateString()) {
							$newBranchSchedule = $schedule->replicate();
							$newBranchSchedule->schedule_date = $event->toDateString();
							$newBranchSchedule->schedule_group = $newGroupUuid;
							$newBranchSchedule->time_from = request('time_from');
							$newBranchSchedule->time_to = request('time_to');
							$newBranchSchedule->save();
						}
					}

					$schedule->schedule_group = $newGroupUuid;
					$schedule->save();

					$scheduleBranchGroup = new BranchScheduleGroup;
					$scheduleBranchGroup->uuid = $newGroupUuid;
					$scheduleBranchGroup->date_from = $scheduleDate->toDateString();
					$scheduleBranchGroup->date_to = $scheduleToDate->toDateString();
					$scheduleBranchGroup->repeat_after = $scheduleRepeatAfter;
					$scheduleBranchGroup->repeat_values = $repeatValues;
					$scheduleBranchGroup->type = $scheduleType;
					$scheduleBranchGroup->save();

					return response()->json([
						'status'          => 'success',
						'events_inserted' => \count($eventDateToInsert),
					]);

				}

				return response()->json([
					'error_code'      => 'ZERO_EVENTS_INSERTED',
					'events_inserted' => 0,
					'status'          => 'success',
				]);
			}

			$updateData = [
				'time_from' => request('time_from'),
				'time_to'   => request('time_to'),
			];

			if ($eventUpdateType === 'single') {
				$eventObject = $schedule;
				$updateData['schedule_group'] = null;
			} else {
				$eventsInGroup = [];

				if ($schedule->schedule_group !== null) {
					$eventsInGroup = BranchSchedule::where('schedule_group', $schedule->schedule_group)->get()->pluck('id');
				}

				$eventObject = BranchSchedule::whereIn('id', $eventsInGroup);
			}

			$eventObject->update($updateData);

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function delete(PlannerSchedulerDeleteEventRequest $request, BranchSchedule $schedule)
	{
		try {
			if ($schedule->delete()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function deleteGroup(PlannerSchedulerDeleteEventRequest $request, BranchSchedule $schedule)
	{
		try {
			$branchUuid = $schedule->schedule_group;

			BranchScheduleGroup::whereUuid($branchUuid)->delete();
			BranchSchedule::where('schedule_group', $branchUuid)
				->where('schedule_date', '>', Carbon::now()->toDateString())
				->delete();

			return response()->json([
				'status' => 'success',
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
