<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use App\Traits\UserSettingsTrait;
use Exception;
use Illuminate\Http\Response;

class PlannerSettingsController extends Controller
{
	use UserSettingsTrait;

	public function index()
	{
		try {
			$settings = $this->getUserSettings([
				'PLANNER_ROOM_COUNT',
				'PLANNER_APPOINTMENTS_SHOW',
				'PLANNER_SLOT_DURATION',
//				'PLANNER_APPOINTMENTS_CANCELED',
			]);

			return response()->json([
				'data' => $settings,
			]);

		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update()
	{
		try {
			$settings = request()->all();

			$this->updateUserMultipleSettings($settings);

			return [
				'status' => 'success',
			];
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
