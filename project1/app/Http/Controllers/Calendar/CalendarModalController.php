<?php

namespace App\Http\Controllers\Calendar;

use App\Enums\TeethMapTypes;
use App\Helpers\DataValidationHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Calendar\CalendarModalCreatePatientRequest;
use App\Http\Requests\Calendar\CalendarViewAvailability;
use App\Http\Requests\Calendar\CalendarViewWeekAvailability;
use App\Http\Requests\Calendar\Modal\CalendarModalUpdateStatusRequest;
use App\Http\Resources\Calendar\CalendarAppointmentPatientsResource;
use App\Http\Resources\Calendar\CalendarAppointmentPatientViewResource;
use App\Models\Appointment;
use App\Models\Branch\Branch;
use App\Models\Branch\BranchRoom;
use App\Models\Branch\BranchSchedule;
use App\Models\Patient\Patient;
use App\Traits\MetaFieldsTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;

class CalendarModalController extends Controller
{
	use MetaFieldsTrait;

	public function index()
	{
		try {
			$orderBy = 'id';
			$orderType = 'ASC';

			$globalSearchValue = request('patient_input');

			$patientPerPage = 50;
			$page = request('page', 1);
			$offset = ($page - 1) * $patientPerPage;

			$patients = Patient::orderBy($orderBy, $orderType)
				->orWhereRaw("Concat(first_name,' ',last_name) LIKE '%{$globalSearchValue}%' ")
				->orWhereRaw("Concat(last_name,' ',first_name) LIKE '%{$globalSearchValue}%' ")
				->orWhere('client_card', 'like', '%' . $globalSearchValue . '%')
				->orWhere('first_name', 'like', '%' . $globalSearchValue . '%')
				->orWhere('last_name', 'like', '%' . $globalSearchValue . '%')
				->orWhere('phone', 'like', '%' . $globalSearchValue . '%')
				->orWhere('personal_code', 'like', '%' . $globalSearchValue . '%')
				->limit($patientPerPage)
				->offset($offset)
				->get();

			return CalendarAppointmentPatientsResource::collection($patients);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function view(Patient $patient): CalendarAppointmentPatientViewResource
	{
		return new CalendarAppointmentPatientViewResource($patient);
	}

	public function store(CalendarModalCreatePatientRequest $request)
	{
		try {
			$patient = new Patient;
			$patient->first_name = $request->get('first_name');
			$patient->last_name = $request->get('last_name');
			$patient->personal_code = $request->get('personal_code');
			$patient->phone = $request->get('phone');
			$patient->dob = $request->get('dob');
			$patient->user_status = 1;

			if ($request->has('personal_code') && !$request->has('dob')) {
				$date = DataValidationHelper::personalCodeGetCarbonDate($request->get('personal_code'));

				if ($date) {
					$patient->dob = $date->toDateString();
				}
			}

			if ($patient->save()) {

				$branchId = Branch::select('id')
					->select('id')
					->where('uuid', $request->get('branch'))
					->firstOrFail();

				$patient->branches()->sync([$branchId->id]);

				$newPlan = $patient->toothPlan()->create([
					'approved'      => true,
					'approved_date' => now()->toDateTimeString(),
					'can_delete'    => false,
					'is_default'    => true,
					'name'          => TeethMapTypes::DEFAULT_PLAN_NAME,
					'user_id'       => 0,
				]);

				if ($newPlan) {
					$newPlan->visits()->create([
						'disabled'     => true,
						'visit_number' => 0,
					]);
				}

				return response()->json([
					'status'       => 'success',
					'patient_uuid' => $patient->uuid,
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function availability(CalendarViewAvailability $request, BranchRoom $branchRooms)
	{
		try {
			$data = $this->getAppointmentProvider(
				$branchRooms->id,
				$branchRooms->uuid,
				$branchRooms->branch_id,
				$request->get('event_date')
			);

			return response()->json($data);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function availabilityWeek(CalendarViewWeekAvailability $request)
	{
		try {
			$date = Carbon::parse($request->get('schedule_date'));

			$schedule = BranchSchedule::select('id', 'room_id', 'user_id')->where([
				'schedule_date' => $date->toDateString(),
				'protected'     => false,
			])->whereHas('user', static function ($q) use ($request) {
				$q->where('uuid', $request->get('user_uuid'));
			})->with([
				'room' => static function ($q) {
					$q->select('id', 'uuid', 'branch_id');
				},
			])->limit(1)
				->first();

			$data = $this->getAppointmentProvider(
				optional(optional($schedule)->room)->id,
				optional(optional($schedule)->room)->uuid,
				optional(optional($schedule)->room)->branch_id,
				$date->toDateTimeString()
			);

			return response()->json($data);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function updateStatus(CalendarModalUpdateStatusRequest $request, Appointment $appointment)
	{
		try {
			$appointment->status = $request->get('status');
			$appointment->save();

			return response(null, Response::HTTP_OK);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	private function searchForScheduledWorkingUser($roomId, $type, $dateTime)
	{
		$date = Carbon::parse($dateTime)->toDateString();
		$time = Carbon::parse($dateTime)->addMinute()->toTimeString();

		$schedule = BranchSchedule::where('room_id', $roomId)
			->where('protected', false)
			->where('type', $type)
			->where('schedule_date', $date)
			->where('time_to', '>=', $time)
			->where('time_from', '<=', $time)
			->with([
				'user' => static function ($q) {
					$q->select('id', 'uuid', 'first_name', 'last_name');
				},
			])
			->limit(1)
			->first();

		if (!empty($schedule)) {
			return [
				'provider_uuid' => optional($schedule->user)->uuid,
				'provider_name' => optional($schedule->user)->full_name,
			];
		}

		return $schedule;
	}

	private function getAppointmentProvider($roomId, $roomUuid, $branchId, $dateTime)
	{
		try {
			if (!$roomId) {
				return [
					'status' => 'user_not_working',
				];
			}

			$doctor = $this->searchForScheduledWorkingUser($roomId, 1, $dateTime);
			$assistant = $this->searchForScheduledWorkingUser($roomId, 2, $dateTime);

			if (empty($assistant) && empty($doctor)) {
				$schedule = [
					'status' => 'schedule_not_found',
				];
			} else {
				$schedule = [
					'assistant' => $assistant,
					'doctor'    => $doctor,
					'status'    => 'success',
				];
			}

			return [
				'data' => $schedule,
				'meta' => [
					'room_uuid'      => $roomUuid,
					'assistant_list' => $this->getUsersInBranch($branchId, [2]),
					'doctor_list'    => $this->getUsersInBranch($branchId, [1]),
				],
			];
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}

