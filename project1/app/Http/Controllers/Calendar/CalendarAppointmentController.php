<?php

namespace App\Http\Controllers\Calendar;

use App\Enums\AppointmentTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Calendar\CalendarAppointmentUpdateRequest;
use App\Http\Requests\Calendar\CalendarAppointmentViewRequest;
use App\Http\Requests\Calendar\CalendarDeleteAppointmentRequest;
use App\Http\Resources\Calendar\CalendarAppointmentResource;
use App\Models\Appointment;
use App\Models\Branch\BranchSchedule;
use App\Models\Patient\Patient;
use App\Models\Patient\Teeth\PatientTeethPlanVisit;
use App\Models\User\User;
use App\Traits\MetaFieldsTrait;
use App\Traits\PatientTrait;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;

class CalendarAppointmentController extends Controller
{
	use MetaFieldsTrait, PatientTrait;

	public function index(CalendarAppointmentViewRequest $request, Appointment $appointment)
	{
		try {
			return new CalendarAppointmentResource($appointment);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(CalendarAppointmentUpdateRequest $request, Appointment $appointment)
	{
		try {
			$assistant = User::whereUuid($request->get('assistant_uuid'))->first();
			$doctor = User::whereUuid($request->get('provider_uuid'))->firstOrFail();
			$patient = Patient::whereUuid($request->get('patient_uuid'))->firstOrFail();
			$visit = PatientTeethPlanVisit::whereUuid($request->get('visit_uuid'))->firstOrFail();
			$lastAssistant = optional($appointment)->assistant_id;

			$eventTimeFrom = Carbon::parse($request->get('event_time_from'))->toTimeString();
			$eventTimeTo = Carbon::parse($request->get('event_time_to'))->toTimeString();

			$appointment->assistant_id = $assistant->id ?? null;
			$appointment->description = $request->get('description');
			$appointment->price = $request->get('price');
			$appointment->event_time_from = $eventTimeFrom;
			$appointment->event_time_to = $eventTimeTo;
			$appointment->patient_id = $patient->id;
			$appointment->provider_id = $doctor->id;
			$appointment->plan_id = $visit->id;

			if ($eventTimeFrom > $eventTimeTo) {
				return [
					'data' => [
						'status' => AppointmentTypes::APPOINTMENT_INVALID_TIME_RANGE,
					],
				];
			}

			/*For security reasons we don't update status because administrator can overwrite if editing appointment in the same time as doctor. */
//			if ($appointment->status <= config('dentic.constants.TOOTH_MAP.APPOINTMENT_AVAILABLE_STATUS') &&
//				$appointment->status >= config('dentic.constants.TOOTH_MAP.APPOINTMENT_CLOSED_STATUS')
//			) {
//				$appointment->status = $request->get('status');
//			}
			$appointment->status = $request->get('status');

			if ($appointment->save()) {
				$branchSchedule = BranchSchedule::where([
					'appointment_id' => $appointment->id,
					'protected'      => true,
					'user_id'        => $doctor->id,
				])->firstOrFail();

				$branchSchedule->time_from = $eventTimeFrom;
				$branchSchedule->time_to = $eventTimeTo;

				if ($branchSchedule->save()) {

					if (!empty($lastAssistant) || empty($assistant)) {
						BranchSchedule::where('user_id', '!=', $doctor->id)
							->where([
								'appointment_id' => $appointment->id,
								'protected'      => true,
								'user_id'        => $lastAssistant,
							])->delete();
					}

					if (!empty($assistant)) {
						BranchSchedule::updateOrCreate([
							'appointment_id' => $appointment->id,
							'protected'      => true,
							'branch_id'      => $branchSchedule->branch_id,
							'room_id'        => $branchSchedule->room_id,
							'tool_id'        => $branchSchedule->tool_id,
							'schedule_date'  => $branchSchedule->schedule_date,
							'type'           => 2,
							'user_id'        => $assistant->id,
						], [
							'time_from' => $eventTimeFrom,
							'time_to'   => $eventTimeTo,
						]);

						self::addHelperIfNotExists($patient, $assistant->id);
					}

					self::addHelperIfNotExists($patient, $doctor->id);

					return response()->json([
						'status' => 'success',
					]);
				}
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function destroy(CalendarDeleteAppointmentRequest $request, Appointment $appointment)
	{
		try {
			$branchDelete = BranchSchedule::where([
				'appointment_id' => $appointment->id,
				'protected'      => true,
			]);

			if ($branchDelete->delete() && $appointment->delete()) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}

