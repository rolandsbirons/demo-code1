<?php

namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Controller;
use App\Traits\UserSettingsTrait;
use Exception;
use Illuminate\Http\Response;

class CalendarSettingsController extends Controller
{
	use UserSettingsTrait;

	public function index()
	{
		try {
			$settings = $this->getUserSettings([
				'CALENDAR_APPOINTMENTS_CANCELED',
				'CALENDAR_DOCTOR_SCHEDULE_ONLY',
				'CALENDAR_ROOM_COUNT',
				'CALENDAR_SLOT_DURATION',
				'CALENDAR_TIME_SPACER',
				'CALENDAR_CANCELED_APPOINTMENTS_CALENDAR',
			]);

			return response()->json([
				'data' => $settings,
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update()
	{
		try {
			$settings = request()->all();

			$this->updateUserMultipleSettings($settings);

			return [
				'status' => 'success',
			];
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
