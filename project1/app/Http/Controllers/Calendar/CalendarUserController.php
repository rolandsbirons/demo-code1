<?php

namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Controller;
use App\Http\Requests\Calendar\CalendarModalViewPatientRequest;
use App\Models\Branch\Branch;
use App\Traits\MetaFieldsTrait;
use Exception;
use Illuminate\Http\Response;

class CalendarUserController extends Controller
{
	use MetaFieldsTrait;

	public function index(CalendarModalViewPatientRequest $request, Branch $branch)
	{
		try {
			$branch->load([
				'users',
				'users.positions',
			]);

			$users = $this->getMetaUsersSystemGroup($branch);

			return response()->json([
				'data' => $users,
			]);

		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}

