<?php

namespace App\Http\Controllers\Component\Invoice;

use App\Http\Controllers\Controller;
use App\Http\Requests\Component\Invoice\InvoiceCalculationRequest;
use App\Traits\InvoiceTrait;
use Exception;
use Illuminate\Http\Response;

class InvoiceCalculationComponent extends Controller
{
	use InvoiceTrait;

	public function index(InvoiceCalculationRequest $request)
	{
		try {
			$services = $request->get('services');
			$products = $request->get('products');

			return response()->json([
				'data' => $this->generateCalculations($services, $products),
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}