<?php

namespace App\Http\Controllers\Component\Products;

use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use Exception;
use Illuminate\Http\Response;

class ProductComponentController extends Controller
{
	public function index()
	{
		try {
			$products = [];
			$categoryUuid = request('category_uuid');
			$categories = ProductCategory::select('id', 'uuid', 'name')->get();

			if ($categories->isNotEmpty()) {
				if (!$categoryUuid) {
					$categoryUuid = $categories->first()->uuid;
				}

				$products = Product::whereHas('category', static function ($q) use ($categoryUuid) {
					$q->where('uuid', $categoryUuid);
				})->where('product_name', 'like', '%' . request('search') . '%')
					->get()->transform(static function ($product) {
						return [
							'name'  => $product->product_name,
							'price' => number_format($product->product_price, 2),
							'tax'   => $product->product_tax,
							'unit'  => $product->product_unit,
							'sku'   => $product->sku,
							'uuid'  => $product->uuid,
						];
					});
			}

			return response()->json([
				'data' => [
					'categories' => $categories,
					'products'   => $products,
				],
				'meta' => [
					'category_selected' => $categoryUuid,
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}