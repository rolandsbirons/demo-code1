<?php

namespace App\Http\Controllers\Component\DenticTable;

use App\Http\Controllers\Controller;
use App\Http\Requests\Component\DenticTable\DenticTableGetRequest;
use App\Http\Requests\Component\DenticTable\DenticTableUpdateRequest;
use App\Traits\UserSettingsTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class DenticTableController extends Controller
{
	use UserSettingsTrait;

	public function index(DenticTableGetRequest $request)
	{
		try {
			if ($request->filled('field')) {
				$tableKey = strtoupper($request->get('field'));

				return response()->json([
					'key_value' => $this->getUserSettings($tableKey),
					'status'    => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function update(DenticTableUpdateRequest $request)
	{
		try {
			if ($request->filled('table_keys')) {
				$updateTable = true;
				foreach ($request->get('table_keys') as $tableKey) {
					$fieldName = strtoupper($tableKey);
					$updateField = $this->updateUserSettings($fieldName, $request->get('cols'));

					if (!$updateField) {
						$updateTable = false;
					}
				}
			} else {
				$tableKey = strtoupper($request->get('table_key'));

				$updateTable = $this->updateUserSettings($tableKey, $request->get('value'));
			}

			if ($updateTable) {
				return response()->json([
					'status' => 'success',
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}