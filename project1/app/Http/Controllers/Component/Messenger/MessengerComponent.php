<?php

namespace App\Http\Controllers\Component\Messenger;

use App\Enums\DateTypes;
use App\Events\Messenger\MessagePosted;
use App\Http\Controllers\Controller;
use App\Models\Branch\Branch;
use App\Models\Messenger\MessengerMessage;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;

class MessengerComponent extends Controller
{
	public function index()
	{
		try {
			$branch = Branch::whereUuid(request()->get('branch_uuid'))->firstOrFail();

			$messages = MessengerMessage::where('branch_id', $branch->id)
				->with('user')
				->limit(30)
				->get()
				->transform(static function (MessengerMessage $message) use ($branch) {
					$date = Carbon::now()->toDateString() === $message->created_at->toDateString() ?
						$message->created_at->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME) :
						$message->created_at->format(DateTypes::MESSENGER_DATE_FORMAT);

					return [
						'message' => [
							'uuid'    => $message->uuid,
							'time'    => $date,
							'content' => $message->content,
						],
						'user'    => [
							'color' => $message->user->color,
							'id'    => $message->user->id,
							'name'  => $message->user->full_name,
						],
						'branch'  => [
							'uuid' => $branch->uuid,
						],
					];
				});

			return response()->json([
				'data' => $messages,
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function store()
	{
		try {
			$branch = Branch::whereUuid(request()->get('branch_uuid'))->firstOrFail();

			$message = new MessengerMessage;
			$message->content = request('message');
			$message->branch_id = $branch->id;

			if ($message->save()) {
				$date = Carbon::now()->toDateString() === $message->created_at->toDateString() ?
					$message->created_at->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME) :
					$message->created_at->format(DateTypes::MESSENGER_DATE_FORMAT);

				$responseMessage = [
					'message' => [
						'content' => $message->content,
						'time'    => $date,
						'uuid'    => $message->uuid,
					],
					'user'    => [
						'color' => $message->user->color,
						'id'    => $message->user->id,
						'name'  => $message->user->full_name,
					],
					'branch'  => [
						'uuid' => $branch->uuid,
					],
				];

				broadcast(new MessagePosted($responseMessage, $branch->uuid))->toOthers();

				return response()->json([
					'data' => [
						'message' => $responseMessage,
					],
				]);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}