<?php

namespace App\Http\Controllers\Auth;

use App\Enums\AdminEnums;
use App\Helpers\PermissionHelper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User\PositionPermission;
use App\Models\User\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Validator;

/**
 * Main Auth controller
 *
 * Class AuthenticationController
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
	/**
	 * @var JWTAuth
	 */

	protected $auth;

	/**
	 * AuthController constructor.
	 *
	 * @param JWTAuth $auth
	 */

	public function __construct(JWTAuth $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Authenticate user with credentials.
	 */

	public function login()
	{
		if (request()->has('token')) {
			$token = request()->get('token');
			$this->auth->setToken($token);
			$user = $this->auth->parseToken()->authenticate();
		} else {
			$credentials = request()->only('username', 'password', 'company_uuid');

			$validator = Validator::make($credentials, [
				'username' => 'required',
				'password' => 'required',
			]);

			if ($validator->fails()) {
				return response()->json('invalid_validation', Response::HTTP_FORBIDDEN);
			}

			$credentials['company_id'] = $this->getCompanyId();
			$credentials['username'] = strtolower($credentials['username']);

			if (!$token = $this->auth->attempt($credentials)) {
				return response()->json('invalid_credentials', Response::HTTP_BAD_REQUEST);
			}

			$user = $this->auth->setToken($token)->toUser();
		}

		$user = $this->returnUserObject($user);

		return response()->json([
			'data'   => [
				'token' => $token,
				'user'  => $user,
			],
			'status' => 'success',
		]);
	}


	/**
	 * Verify token
	 *
	 * @return JsonResponse
	 */

	public function verify(): ?JsonResponse
	{
		try {
			if (!$user = $this->auth->parseToken()->authenticate()) {
				return abort(Response::HTTP_NOT_FOUND);
			}

			$roles = $this->getUserPermissions($user);

			$user = $this->returnUserObject($user);

			return response()->json([
				'data' => compact('user', 'roles'),
			]);
		} catch (JWTException $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Return user object with only needed data for UI, Vuex or Sentry.
	 *
	 * @param $user
	 *
	 * @return array
	 */
	private function returnUserObject(User $user): array
	{
		return [
			'name' => $user->full_name,
			'uuid' => $user->uuid,
		];
	}

	/**
	 * Refresh users JWT token if authentication is valid
	 */

	public function refresh()
	{
		try {
			$this->auth->parseToken()->authenticate();
			$token = $this->auth->parseToken()->refresh();

			return response()->json(compact('token'));
		} catch (JWTException $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Destroy users session if authentication if valid
	 */

	public function destroy()
	{
		try {
			$this->auth->parseToken()->authenticate();
			$this->auth->parseToken()->invalidate();

			return response()->json([
				'status' => 'success',
			]);
		} catch (JWTException $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Return company ID
	 *
	 * @return int
	 */
	private function getCompanyId(): int
	{
		$slug = Str::before(Request::getHttpHost(), '.');
		$companyQuery = Company::whereSlug($slug)->first();

		return $companyQuery ? $companyQuery->id : 0;
	}

	/**
	 * Get and compare user permissions from systems available
	 *
	 * @param $user
	 *
	 * @return Collection
	 */

	private function getUserPermissions($user): Collection
	{
		$systemRoles = PermissionHelper::getSystemPermissionsList();

		$userPositions = $user->positions->pluck('id');

		$positions = PositionPermission::whereIn('position_id', $userPositions)
			->whereIn('permission_type', $systemRoles)
			->get(['permission_type'])
			->pluck('permission_type');

		if (in_array($user->id, AdminEnums::ADMIN_ID_LIST, true)) {
			$positions->push('admin');
		}

		return $positions;
	}

}
