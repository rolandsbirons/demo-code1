<?php

namespace App\Http\Controllers;

use App\Traits\MetaFieldsTrait;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class MetaController extends Controller
{
	use MetaFieldsTrait;

	public function getBranchList()
	{
		try {
			return response()->json([
				'data' => $this->getMetaBranches(),
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}