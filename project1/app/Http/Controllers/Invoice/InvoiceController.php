<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Http\Requests\Patient\PatientTableSearchRequest;
use App\Http\Resources\Invoice\InvoiceTableCollection;
use App\Models\Patient\Invoice\PatientInvoice;
use Exception;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{
	public function index(PatientTableSearchRequest $request)
	{
		try {
			$patientFields = [
				'uuid',
				'invoice_id',
			];

			$data = PatientInvoice::with([
				'patient',
				'method',
			]);

			if ($request->filled('globalSearch')) {
				$globalSearchValue = $request->get('globalSearch');
				unset($patientFields['first_name'], $patientFields['last_name']);

				$data->orWhereRaw("Concat(first_name,' ',last_name) LIKE '%{$globalSearchValue}%' ")
					->orWhereRaw("Concat(last_name,' ',first_name) LIKE '%{$globalSearchValue}%' ");

				foreach ($patientFields as $patientField) {
					$data->orWhere($patientField, 'like', '%' . $globalSearchValue . '%');
				}
			} else {
				foreach ($patientFields as $patientField) {
					if ($request->filled($patientField)) {
						$data->where($patientField, 'like', '%' . $request->get($patientField) . '%');
					}
				}
			}

			$data->orderBy($request->get('orderField', 'uuid'), $request->get('orderBy', 'DESC'));

			$result = $data->simplePaginate($request->get('perPage', 20), ['*'], 'page', $request->get('page'));

			return (new InvoiceTableCollection($result))->additional([
				'meta' => [
					'has_more' => $result->hasMorePages(),
				],
			]);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
}
