<?php

namespace App\Http;

use App\Http\Middleware\AdminDebugMiddleware;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\Localization;
use App\Http\Middleware\SentryContext;
use App\Http\Middleware\TrimStrings;
use App\Http\Middleware\TrustProxies;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;

class Kernel extends HttpKernel
{
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * These middleware are run during every request to your application.
	 *
	 * @var array
	 */
	protected $middleware = [
		ConvertEmptyStringsToNull::class,
		TrimStrings::class,
		TrustProxies::class,
		ValidatePostSize::class,
	];

	/**
	 * The application's route middleware groups.
	 *
	 * @var array
	 */
	protected $middlewareGroups = [
		'web' => [],

		'api' => [
//			'throttle:300,1',
			'api.localization',
			'bindings',
			'sentry.user',
		],
	];

	/**
	 * The application's route middleware.
	 *
	 * These middleware may be assigned to groups or used individually.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'admin'            => AdminMiddleware::class,
		'admin.debug'      => AdminDebugMiddleware::class,
		'api.localization' => Localization::class,
		'bindings'         => SubstituteBindings::class,
		'cache.headers'    => SetCacheHeaders::class,
		'sentry.user'      => SentryContext::class,
//		'throttle'         => ThrottleRequests::class,
	];
}
