<?php

namespace App\Http\Requests\Product\Item;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductItemDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'product_item_delete',
			'product_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}