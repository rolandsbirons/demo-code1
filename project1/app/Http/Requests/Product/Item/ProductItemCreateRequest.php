<?php

namespace App\Http\Requests\Product\Item;

use App\Enums\ProductUnitTypes;
use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductItemCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'product_item_create',
			'product_view',
		]);
	}

	public function rules(): array
	{
		return [
			'costs'         => 'required|regex:/^\d*(\.\d{1,2})?$/',
			'product_name'  => 'required|string',
			'product_price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
			'product_tax'   => 'nullable|digits_between:0,3|integer|min:0|max:100',
			'product_time'  => 'required|string|date_format:H:i',
			'product_unit'  => 'required|in:' . implode(',', ProductUnitTypes::PRODUCT_UNITS_SHORT),
			'sku'           => '',
		];
	}
}