<?php

namespace App\Http\Requests\Product\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'product_settings_view',
			'product_view',
			'service_settings_update',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required',
			'name'  => 'required|string',
		];
	}
}
