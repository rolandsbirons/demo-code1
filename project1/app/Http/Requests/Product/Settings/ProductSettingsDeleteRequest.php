<?php

namespace App\Http\Requests\Product\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductSettingsDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'product_delete',
			'product_settings_view',
			'product_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
