<?php

namespace App\Http\Requests\Product;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ProductViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'product_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
