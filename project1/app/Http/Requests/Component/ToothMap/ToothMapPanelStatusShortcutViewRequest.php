<?php

namespace App\Http\Requests\Component\ToothMap;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ToothMapPanelStatusShortcutViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_tooth_map_view');
	}

	public function rules(): array
	{
		return [];
	}
}
