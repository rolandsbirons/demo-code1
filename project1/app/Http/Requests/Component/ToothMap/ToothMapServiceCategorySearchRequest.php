<?php

namespace App\Http\Requests\Component\ToothMap;

use Illuminate\Foundation\Http\FormRequest;

class ToothMapServiceCategorySearchRequest extends FormRequest
{
	public function authorize(): bool
	{
		//Because we use this component also in invoices
		return true;
	}

	public function rules(): array
	{
		return [
			'group'          => 'nullable|uuid',
			'service_limit'  => 'integer|min:1|max:500',
			'service_search' => '',
		];
	}
}
