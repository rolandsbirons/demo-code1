<?php

namespace App\Http\Requests\Component\Invoice;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class InvoiceCalculationRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;
//		return $this->canAccessFunction('patients_tooth_map_view');
	}

	public function rules(): array
	{
		return [
			'services' => '',
			'products' => '',
		];
	}
}
