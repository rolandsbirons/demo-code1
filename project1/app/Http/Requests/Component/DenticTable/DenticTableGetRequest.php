<?php

namespace App\Http\Requests\Component\DenticTable;

use Illuminate\Foundation\Http\FormRequest;

class DenticTableGetRequest extends FormRequest
{
	public function authorize(): bool
	{
		return true;
	}

	public function rules(): array
	{
		return [
			'field' => 'required',
		];
	}
}
