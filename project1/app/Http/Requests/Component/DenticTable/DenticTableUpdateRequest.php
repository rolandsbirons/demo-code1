<?php

namespace App\Http\Requests\Component\DenticTable;

use App\Enums\UserSettingsTypes;
use Illuminate\Foundation\Http\FormRequest;

class DenticTableUpdateRequest extends FormRequest
{
	public function authorize(): bool
	{
		return true;
	}

	public function rules(): array
	{
		return [
			'cols'          => 'required',
			'table_key'     => 'in:' . strtolower(implode(',', array_keys(UserSettingsTypes::toArray()))),
			'table_keys.*.' => 'in:' . strtolower(implode(',', array_keys(UserSettingsTypes::toArray()))),
		];
	}
}
