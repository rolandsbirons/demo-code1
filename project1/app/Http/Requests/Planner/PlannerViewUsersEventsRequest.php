<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerViewUsersEventsRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('planner_view');
	}

	public function rules(): array
	{
		return [
			'branch_uuid' => 'required|uuid',
			'date'        => 'required|date_format:Y-m-d',
			'user_type'   => 'required|in:1,2,3',
		];
	}
}
