<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerStoreEventRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'planner_view',
			'planner_create',
		]);
	}

	public function rules(): array
	{
		return [
			'date'      => 'required|date_format:Y-m-d',
			'room_uuid' => 'required|uuid',
			'type'      => 'required|integer|in:1,2,3',
			'user_uuid' => 'required|uuid',
		];
	}
}
