<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerViewEventRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'planner_view',
			'planner_update',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
