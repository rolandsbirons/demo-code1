<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerViewEventsResourcesRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('planner_view');
	}

	public function rules(): array
	{
		return [
			'branch' => 'required|uuid',
			'date'   => 'required|date_format:Y-m-d',
			'page'   => 'required|integer',
		];
	}
}
