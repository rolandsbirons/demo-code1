<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerSchedulerUpdateEventRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'planner_view',
			'planner_update',
		]);
	}

	public function rules(): array
	{
		return [
			'schedule_date'      => 'required|date_format:Y-m-d',
			'schedule_to_date'   => 'required|date_format:Y-m-d',
			'event_has_changed'  => '',
			'event_time_changed' => '',
			'update_type'        => 'required|in:single,group',
			'recurring'          => 'required|boolean',
			'repeat_after'       => 'required|integer',
			'schedule_type'      => 'in:day,week,month',
		];
	}
}
