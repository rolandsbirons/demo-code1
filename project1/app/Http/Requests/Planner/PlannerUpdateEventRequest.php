<?php

namespace App\Http\Requests\Planner;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PlannerUpdateEventRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'planner_view',
			'planner_update',
		]);
	}

	public function rules(): array
	{
		return [
			'room_uuid' => 'nullable|uuid',
			'time_from' => 'required|string',
			'time_to'   => 'required|string',
			'uuid'      => 'required|uuid',
		];
	}
}
