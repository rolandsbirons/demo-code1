<?php

namespace App\Http\Requests\User\WorkingHours;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class UserWorkingHoursViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'user_view',
			'user_working_hours_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
