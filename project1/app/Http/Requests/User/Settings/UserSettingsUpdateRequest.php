<?php

namespace App\Http\Requests\User\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class UserSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'user_settings_update',
			'user_settings_view',
			'user_view',
		]);
	}

	public function rules(): array
	{
		return [
			'first_name' => 'required',
			'last_name'  => 'required',
			'username'   => 'required',
		];
	}
}
