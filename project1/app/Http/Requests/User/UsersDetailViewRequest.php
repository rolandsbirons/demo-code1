<?php

namespace App\Http\Requests\User;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class UsersDetailViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'user_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
