<?php

namespace App\Http\Requests\User;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class UsersUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'user_view',
			'user_update',
		]);
	}

	public function rules(): array
	{
		return [
			'certificate'      => '',
			'certificate_date' => '',
			'email'            => '',
			'personal_code'    => 'required',
			'phone'            => '',
			'sanitary_book'    => '',
		];
	}
}
