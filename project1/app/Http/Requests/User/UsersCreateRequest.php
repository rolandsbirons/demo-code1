<?php

namespace App\Http\Requests\Branch;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class UsersCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'user_create',
			'user_view',
		]);
	}

	public function rules(): array
	{
		return [
			'first_name' => 'required',
			'last_name'  => 'required',
			'username'   => 'required',
		];
	}
}
