<?php

namespace App\Http\Requests\Test\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestSettingsViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_settings_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
