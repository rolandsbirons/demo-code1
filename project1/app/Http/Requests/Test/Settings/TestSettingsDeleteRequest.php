<?php

namespace App\Http\Requests\Test\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestSettingsDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_delete',
			'test_settings_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
