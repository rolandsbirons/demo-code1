<?php

namespace App\Http\Requests\Test\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_settings_update',
			'test_settings_view',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required|string',
			'name'  => 'required|string',
		];
	}
}
