<?php

namespace App\Http\Requests\Test\Questions;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestQuestionViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
