<?php

namespace App\Http\Requests\Test\Questions;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestQuestionOrderUpdate extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_question_order',
			'test_view',
		]);
	}

	public function rules(): array
	{
		return [
			'parent_uuid' => 'nullable|uuid',
			'questions'   => 'required',
			'questions.*' => 'required|uuid',
		];
	}
}
