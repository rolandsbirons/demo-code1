<?php

namespace App\Http\Requests\Test\Questions;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestQuestionUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_question_update',
			'test_view',
		]);
	}

	public function rules(): array
	{
		return [
			'comment'     => '',
			'parent_uuid' => 'uuid|nullable',
			'test_type'   => 'required|integer|min:0|max:1',
			'test_value'  => 'required',
			'trigger'     => '',
		];
	}
}
