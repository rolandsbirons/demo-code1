<?php

namespace App\Http\Requests\Test;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
