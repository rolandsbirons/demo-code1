<?php

namespace App\Http\Requests\Test;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class TestCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'test_view',
			'test_create',
		]);
	}

	public function rules(): array
	{
		return [
			'name' => 'required|string',
		];
	}
}
