<?php

namespace App\Http\Requests\Department;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'department_create',
			'department_view',
		]);
	}

	public function rules(): array
	{
		return [
			'name' => 'required',
		];
	}
}
