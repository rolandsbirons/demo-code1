<?php

namespace App\Http\Requests\Department\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'department_view',
			'department_settings_view',
			'department_settings_update',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required',
			'name'  => 'required',
		];
	}
}
