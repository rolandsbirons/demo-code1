<?php

namespace App\Http\Requests\Department\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentSettingsViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'department_view',
			'department_settings_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
