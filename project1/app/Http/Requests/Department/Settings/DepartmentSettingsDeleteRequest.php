<?php

namespace App\Http\Requests\Department\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentSettingsDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'department_delete',
			'department_settings_view',
			'department_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
