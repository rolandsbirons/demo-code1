<?php

namespace App\Http\Requests\Report\Income;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ReportIncomeRoomRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'report_view',
		]);
	}

	public function rules(): array
	{
		return [
			'branch_uuid' => 'required|uuid',
			'date'        => 'required|date_format:Y-m-d',
			'view_type'   => 'in:day,week,month',
		];
	}
}
