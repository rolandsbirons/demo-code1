<?php

namespace App\Http\Requests\Report\Workload;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ReportWorkloadUserRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'report_view',
		]);
	}

	public function rules(): array
	{
		return [
			'branch_uuid' => 'required|uuid',
			'date'        => 'required|date_format:Y-m-d',
			'user_type'   => 'required|in:2,3',
			'view_type'   => 'in:day,week,month',
		];
	}
}
