<?php

namespace App\Http\Requests\Position\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PositionSettingsViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'role_settings_view',
			'role_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
