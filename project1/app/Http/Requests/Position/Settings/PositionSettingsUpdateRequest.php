<?php

namespace App\Http\Requests\Position\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PositionSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'role_settings_update',
			'role_settings_view',
			'role_view',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required',
			'name'  => 'required',
			'type'  => 'required|integer|min:1|max:3',
		];
	}
}
