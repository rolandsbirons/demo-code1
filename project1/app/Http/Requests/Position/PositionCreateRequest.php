<?php

namespace App\Http\Requests\Position;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PositionCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'role_create',
			'role_view',
		]);
	}

	public function rules(): array
	{
		return [
			'name' => 'required',
			'type' => 'required|integer|min:1|max:3',
		];
	}
}
