<?php

namespace App\Http\Requests\Position;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PositionViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('role_view');
	}

	public function rules(): array
	{
		return [];
	}
}
