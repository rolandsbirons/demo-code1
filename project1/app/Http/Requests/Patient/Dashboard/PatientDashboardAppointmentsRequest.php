<?php

namespace App\Http\Requests\Patient\Dashboard;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientDashboardAppointmentsRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;
//		return $this->canAccessFunction([
//			'dashboard_view',
//		]);
	}

	public function rules(): array
	{
		return [];
	}
}
