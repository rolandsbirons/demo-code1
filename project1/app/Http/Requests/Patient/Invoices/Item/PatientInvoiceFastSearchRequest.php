<?php

namespace App\Http\Requests\Patient\Invoices\Item;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientInvoiceFastSearchRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_view');
	}

	public function rules(): array
	{
		return [
			'search_query' => 'string|nullable',
		];
	}
}
