<?php

namespace App\Http\Requests\Patient\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientInvoiceUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'delete_services.*' => 'required|uuid',
			'delete_products.*' => 'required|uuid'
//			'comment'      => 'max:65535',
//			'discount'     => 'required|integer|between:-100,100',
////			'payment_type' => 'required|integer|between:0,4',
//			'invoice_due'  => 'required|date_format:Y-m-d',
		];
	}
}
