<?php

namespace App\Http\Requests\Patient\Invoices\Method;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientInvoiceMethodUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_view');
	}

	public function rules(): array
	{
		return [
			'basic'                 => 'boolean',
			'methods.*.amount'      => 'required',
			'methods.*.amount_paid' => 'required',
			'methods.*.invoice_sku' => 'string|nullable',
			'methods.*.status'      => 'required|min:1|max:3',
			'methods.*.type'        => 'required|min:1|max:5',
			'methods.*.uuid'        => 'required|uuid',
		];
	}
}
