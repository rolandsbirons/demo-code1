<?php

namespace App\Http\Requests\Patient\Tests;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientTestsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'questions.*.uuid'          => 'required|uuid',
			'questions.*.question_type' => 'required|in:0,1',
			'questions.*.answer'        => '',
		];
	}
}
