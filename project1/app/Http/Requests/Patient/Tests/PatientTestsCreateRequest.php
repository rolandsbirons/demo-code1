<?php

namespace App\Http\Requests\Patient\Tests;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientTestsCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'test_uuid' => 'required|uuid',
		];
	}
}
