<?php

namespace App\Http\Requests\Patient;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'address'       => '',
			'assistants'    => '',
			'branches.*'    => 'required|uuid',
			'client_card'   => '',
			'description'   => '',
			'dob'           => 'nullable|date_format:Y-m-d',
			'doctor'        => 'required|uuid',
			'email'         => '',
			'first_name'    => 'required',
			'gender'        => '',
			'insurance'     => '',
			'job'           => '',
			'last_name'     => 'required',
			'personal_code' => 'required',
			'phone'         => '',
			'phone2'        => '',
		];
	}
}
