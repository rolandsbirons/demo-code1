<?php

namespace App\Http\Requests\Patient;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientTableSearchRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'orderBy'    => 'in:ASC,DESC',
			'orderField' => '',
			'page'       => 'integer',
			'perPage'    => 'integer',

			'branches' => 'nullable|uuid',
			'doctor'   => 'nullable|uuid',
			'status'   => 'integer',

			'address'       => '',
			'client_card'   => '',
			'email'         => '',
			'first_name'    => '',
			'gender'        => '',
			'insurance'     => '',
			'last_name'     => '',
			'personal_code' => '',
			'phone'         => '',
			'user_status'   => '',
		];
	}
}
