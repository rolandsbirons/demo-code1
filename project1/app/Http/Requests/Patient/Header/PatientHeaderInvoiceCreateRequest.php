<?php

namespace App\Http\Requests\Patient\Header;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientHeaderInvoiceCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_view');
	}

	public function rules(): array
	{
		return [
			'services.*.uuid'     => 'required|uuid',
			'services.*.discount' => 'required|integer',
		];
	}
}
