<?php

namespace App\Http\Requests\Patient\ToothMap\Status;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientToothStatusStoreRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'patients_tooth_map_view',
			'patients_tooth_map_edit',
		]);
	}

	public function rules(): array
	{
		return [
			'done_after'  => 'required|boolean',
			'done_before' => 'required|boolean',
			'plan_uuid'   => 'required|uuid',
			'status_uuid' => 'required|uuid',
			'tooth_id'    => 'required|integer',
		];
	}
}
