<?php

namespace App\Http\Requests\Patient\ToothMap\Status;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientToothStatusItemUpdateFixRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'patients_tooth_map_view',
			'patients_tooth_map_edit',
		]);
	}

	public function rules(): array
	{
		return [
			'status' => 'required|boolean',
		];
	}
}
