<?php

namespace App\Http\Requests\Patient\ToothMap\Panel;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientToothPanelUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_tooth_map_view');
	}

	public function rules(): array
	{
		return [
			'modal_data'    => 'required',
			'modal_prefix'  => '',
			'modal_segment' => 'required|in:services_shortcut,procedure_shortcut,procedure_blocks',
		];
	}
}
