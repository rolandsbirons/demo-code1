<?php

namespace App\Http\Requests\Patient\ToothMap\Table;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientToothTableUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('patients_tooth_map_view');
	}

	public function rules(): array
	{
		return [
			'services.*.uuid'  => 'required|uuid',
			'services.*.visit' => 'required|integer',
		];
	}
}
