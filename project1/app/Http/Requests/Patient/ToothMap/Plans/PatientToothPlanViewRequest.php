<?php

namespace App\Http\Requests\Patient\ToothMap\Plans;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientToothPlanViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [];
	}
}
