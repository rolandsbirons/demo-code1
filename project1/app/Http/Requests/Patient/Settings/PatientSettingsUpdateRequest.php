<?php

namespace App\Http\Requests\Patient\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class PatientSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return true;

//		return $this->canAccessFunction('branch_create');
	}

	public function rules(): array
	{
		return [
			'user_status' => 'required|in:0,1',
		];
	}
}
