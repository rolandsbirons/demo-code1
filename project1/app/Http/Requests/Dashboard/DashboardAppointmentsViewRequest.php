<?php

namespace App\Http\Requests\Dashboard;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class DashboardAppointmentsViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
//			'dashboard_view',
		]);
	}

	public function rules(): array
	{
		return [
			'appointment_date'     => 'required|date_format:Y-m-d',
			'appointment_status.*' => 'required|integer|min:0|max:10',
			'view_type'            => 'required|in:day,week,month',
		];
	}
}
