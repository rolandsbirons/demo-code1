<?php

namespace App\Http\Requests\Branch\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchSettingsDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_delete',
			'branch_settings_view',
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
