<?php

namespace App\Http\Requests\Branch\WorkingHours;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchWorkingHoursViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_view',
			'branch_working_hours_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}