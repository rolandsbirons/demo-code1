<?php

namespace App\Http\Requests\Branch;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchDetailsViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
