<?php

namespace App\Http\Requests\Branch;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_update',
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [
			'address_actual'   => '',
			'address_declared' => '',
			'bank_code'        => '',
			'bank_name'        => '',
			'bank_swift'       => '',
			'color'            => '',
			'company_name'     => '',
			'company_number'   => '',
			'email'            => '',
			'phone'            => '',
		];
	}
}
