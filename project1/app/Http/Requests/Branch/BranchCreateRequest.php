<?php

namespace App\Http\Requests\Branch;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_view',
			'branch_create',
		]);
	}

	public function rules(): array
	{
		return [
			'name' => 'required',
		];
	}
}
