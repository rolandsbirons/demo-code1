<?php

namespace App\Http\Requests\Branch\Rooms;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchRoomUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_rooms_update',
			'branch_rooms_view',
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [
			'color'  => 'required',
			'hidden' => 'required|in:0,1',
			'name'   => 'required|string',
		];
	}
}
