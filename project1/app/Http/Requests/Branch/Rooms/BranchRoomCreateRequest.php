<?php

namespace App\Http\Requests\Branch\Rooms;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchRoomCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_rooms_create',
			'branch_rooms_view',
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required|string',
			'name'  => 'required|string',
		];
	}
}
