<?php

namespace App\Http\Requests\Branch\Rooms;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class BranchRoomUpdateOrderRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'branch_rooms_order',
			'branch_rooms_view',
			'branch_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
