<?php

namespace App\Http\Requests\Service\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceSettingsDeleteRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_delete',
			'service_settings_view',
			'service_view',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
