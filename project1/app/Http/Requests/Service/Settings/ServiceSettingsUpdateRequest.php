<?php

namespace App\Http\Requests\Service\Settings;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceSettingsUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_delete',
			'service_settings_update',
			'service_settings_view',
		]);
	}

	public function rules(): array
	{
		return [
			'color' => 'required',
			'name'  => 'required|string',
		];
	}
}
