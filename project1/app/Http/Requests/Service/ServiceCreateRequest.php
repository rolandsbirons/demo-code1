<?php

namespace App\Http\Requests\Service;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_create',
			'service_view',
		]);
	}

	public function rules(): array
	{
		return [
			'name' => 'required|string',
		];
	}
}
