<?php

namespace App\Http\Requests\Service\Item;

use App\Enums\ProductUnitTypes;
use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceItemCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_item_create',
			'service_view',
		]);
	}

	public function rules(): array
	{
		return [
			'costs'              => 'required|regex:/^\d*(\.\d{1,2})?$/',
			'service_name'       => 'required|string',
			'service_price_from' => 'required|regex:/^\d*(\.\d{1,2})?$/',
			'service_price_to'   => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
			'service_tax'        => 'nullable|digits_between:0,3|integer|min:0|max:100',
			'service_time'       => 'required|string|date_format:H:i',
			'service_unit'       => 'required|in:' . implode(',', ProductUnitTypes::PRODUCT_UNITS_SHORT),
			'sku'                => '',
		];
	}
}