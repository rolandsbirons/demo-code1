<?php

namespace App\Http\Requests\Service\Item;

use App\Enums\ProductUnitTypes;
use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceItemUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_view',
			'service_item_update',
		]);
	}

	public function rules(): array
	{
		return [
			'costs'              => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
			'service_unit'       => 'nullable|in:' . implode(',', ProductUnitTypes::PRODUCT_UNITS_SHORT),
			'service_name'       => 'nullable|string',
			'service_price_from' => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
			'service_price_to'   => 'nullable|regex:/^\d*(\.\d{1,2})?$/',
			'service_tax'        => 'nullable|digits_between:0,3|integer|min:0|max:100',
			'service_time'       => 'required|string|date_format:H:i',
			'sku'                => '',
		];
	}
}
