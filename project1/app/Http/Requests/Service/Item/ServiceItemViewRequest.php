<?php

namespace App\Http\Requests\Service\Item;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class ServiceItemViewRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'service_item_delete',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}