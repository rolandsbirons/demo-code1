<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarViewEventsResourcesRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('calendar_view');
	}

	public function rules(): array
	{
		return [
			'user_uuid' => '',
			'branch'    => 'required',
			'date'      => 'required|date_format:Y-m-d',
		];
	}
}
