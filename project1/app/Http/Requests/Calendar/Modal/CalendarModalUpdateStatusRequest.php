<?php

namespace App\Http\Requests\Calendar\Modal;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarModalUpdateStatusRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('calendar_view');
	}

	public function rules(): array
	{
		return [
			'status' => 'required|integer|min:0|max:10',
		];
	}
}
