<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarDeleteAppointmentRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'calendar_view',
			'calendar_delete',
		]);
	}

	public function rules(): array
	{
		return [];
	}
}
