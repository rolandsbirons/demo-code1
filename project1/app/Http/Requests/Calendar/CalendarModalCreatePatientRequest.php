<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarModalCreatePatientRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('calendar_view');
	}

	public function rules(): array
	{
		return [
			'branch'        => 'required|uuid',
			'dob'           => 'nullable|date_format:Y-m-d',
			'first_name'    => 'required|string',
			'last_name'     => 'required|string',
			'personal_code' => '',
			'phone'         => 'required|string',
		];
	}
}
