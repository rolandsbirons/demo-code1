<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarAppointmentCreateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'calendar_create',
			'calendar_view',
		]);
	}

	public function rules(): array
	{
		return [
			'assistant_uuid'  => 'nullable|uuid',
			'description'     => 'nullable',
			'event_date'      => 'required|date_format:Y-m-d',
			'event_time_from' => 'required|date_format:H:i',
			'event_time_to'   => 'required|date_format:H:i',
			'patient_uuid'    => 'uuid',
			'provider_uuid'   => 'uuid',
			'visit_uuid'      => 'required',
		];
	}
}
