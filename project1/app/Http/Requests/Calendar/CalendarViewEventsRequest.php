<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarViewEventsRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('calendar_view');
	}

	public function rules(): array
	{
		return [
			'branch_uuid' => 'uuid',
			'date'        => 'required|date_format:Y-m-d',
			'page'        => 'integer',
			'single'      => 'nullable',
			'user_uuid'   => 'nullable|uuid',
			'view_type'   => 'required|in:day,week,month',
		];
	}
}
