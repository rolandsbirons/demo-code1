<?php

namespace App\Http\Requests\Calendar;

use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarModalViewPatientRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction('calendar_view');
	}

	public function rules(): array
	{
		return [];
	}
}
