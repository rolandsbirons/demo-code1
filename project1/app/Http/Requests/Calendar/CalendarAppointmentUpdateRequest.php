<?php

namespace App\Http\Requests\Calendar;

use App\Enums\AppointmentTypes;
use App\Traits\PermissionTrait;
use Illuminate\Foundation\Http\FormRequest;

class CalendarAppointmentUpdateRequest extends FormRequest
{
	use PermissionTrait;

	public function authorize(): bool
	{
		return $this->canAccessFunction([
			'calendar_view',
			'calendar_update',
		]);
	}

	public function rules(): array
	{
		return [
			'assistant_uuid'  => 'nullable|uuid',
			'description'     => 'nullable',
			'event_time_from' => 'required|date_format:H:i',
			'event_time_to'   => 'required|date_format:H:i',
			'patient_uuid'    => 'required|uuid',
			'price'           => '',
			'provider_uuid'   => 'required|uuid',
			'status'          => 'required|integer|in:' . strtolower(implode(',', array_keys(AppointmentTypes::APPOINTMENT_COLORS))),
			'visit_uuid'      => 'required|uuid',
		];
	}
}
