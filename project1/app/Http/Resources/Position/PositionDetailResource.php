<?php

namespace App\Http\Resources\Position;

use App\Helpers\PermissionHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class PositionDetailResource extends JsonResource
{
	public function toArray($request): array
	{
		$this->load(['permissions']);

		return [
			'deletable'   => $this->deletable ?? null,
			'name'        => $this->name ?? null,
			'permissions' => PermissionHelper::drawPermissionTree($this->permissions ?? null),
			'type'        => $this->type ?? null,
			'uuid'        => $this->uuid ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
