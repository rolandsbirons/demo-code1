<?php

namespace App\Http\Resources\Position;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class PositionSettingResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'color'     => $this->color ?? null,
			'deletable' => $this->deletable ?? null,
			'name'      => $this->name ?? null,
			'type'      => $this->type ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
