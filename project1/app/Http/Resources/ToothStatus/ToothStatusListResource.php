<?php

namespace App\Http\Resources\ToothStatus;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothStatusListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->name ?? null,
			'style' => [
				'color' => $this->color ?? '#00000',
			],
		];
	}
}
