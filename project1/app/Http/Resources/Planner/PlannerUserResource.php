<?php

namespace App\Http\Resources\Planner;

use App\Http\Resources\Meta\MetaWorkingHoursResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PlannerUserResource extends JsonResource
{
	public function toArray($request): array
	{
		$schedules = !empty($this->schedules) ? $this->schedules->count() > 0 : false;

		return [
			'color'         => optional($this)->color,
			'name'          => optional($this)->full_name,
			'has_schedules' => $schedules,
			'working_times' => MetaWorkingHoursResource::collection($this->workTimes),
			'uuid'          => optional($this)->uuid,
		];
	}
}
