<?php

namespace App\Http\Resources\Planner;

use App\Enums\DateTypes;
use Illuminate\Http\Resources\Json\JsonResource;

class PlannerViewEventsResource extends JsonResource
{
	public function toArray($request): array
	{
		$protected = $this->protected ? '(P) ' : '';

		$scheduleStart = $this->schedule_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $this->time_from;
		$scheduleEnd = $this->schedule_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $this->time_to;

		return [
			'appointment' => optional(optional($this)->appointment)->uuid,
			'color'       => $this->user->color,
			'editable'    => !$this->protected,
			'end'         => $scheduleEnd,
			'id'          => $this->uuid,
			'order'       => $this->type,
			'protected'   => $this->protected,
			'resourceId'  => $this->room->uuid,
			'start'       => $scheduleStart,
			'title'       => $protected . optional($this->user)->full_name_short,
		];
	}
}
