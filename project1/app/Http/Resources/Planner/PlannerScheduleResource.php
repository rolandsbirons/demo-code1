<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\JsonResource;

class PlannerScheduleResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray($request): array
	{
		return [
			'appointment'   => optional(optional($this)->appointment)->uuid,
			'group'         => isset($this->group) ? [
				'date_from'     => $this->group->date_from->toDateString(),
				'date_to'       => $this->group->date_to->toDateString(),
				'repeat_after'  => $this->group->repeat_after,
				'repeat_values' => $this->group->repeat_values,
				'type'          => $this->group->type,
			] : null,
			'schedule_date' => $this->schedule_date->toDateString(),
			'time_from'     => $this->time_from,
			'time_to'       => $this->time_to,
			'user'          => $this->user->full_name,
			'uuid'          => $this->uuid,
		];
	}


	public function with($request): array
	{
		return [
			'meta' => [
				'month_weeks' => collect(range(1, 30))->map(function ($key) {
					return [
						'key'   => $key,
						'value' => $key,
					];
				}),
			],
		];
	}
}
