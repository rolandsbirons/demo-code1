<?php

namespace App\Http\Resources\Planner;

use App\Enums\DateTypes;
use App\Helpers\FullCalendarHelper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PlannerViewRoomsResource extends JsonResource
{
	public function toArray($request): array
	{
		$scheduleBackground = $this->workTimes->transform(static function ($s) {
			$timeFrom = Carbon::parse($s->time_from);
			$timeTo = Carbon::parse($s->time_to);

			return [
				'daysOfWeek' => [$s->day],
				'startTime'  => $timeFrom->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME),
				'endTime'    => $timeTo->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME),
			];
		});

		$roomName = FullCalendarHelper::generateResourceCircleTitle($this->name, $this->color);

		return [
			'id'            => $this->uuid,
			'title'         => $roomName,
			'businessHours' => $scheduleBackground->isEmpty() ? [
				'start' => '23:59:58',
				'end'   => '23:59:59',
			] : $scheduleBackground,
		];
	}
}
