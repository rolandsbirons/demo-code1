<?php

namespace App\Http\Resources\Test;

use Illuminate\Http\Resources\Json\Resource;

class TestDetailsResource extends Resource
{
	public function toArray($request): array
	{
		$questions = collect(TestQuestionResource::collection($this->questions ?? []))
			->where('parent_id', 0)
			->toArray();

		return [
			'questions' => $questions,
		];
	}
}
