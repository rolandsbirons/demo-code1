<?php

namespace App\Http\Resources\Test;

use Illuminate\Http\Resources\Json\JsonResource;

class TestCategoryResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'name'  => $this->name ?? null,
			'uuid'  => $this->uuid ?? null,
			'style' => [
				'color' => $this->color ?? null,
			],
		];
	}
}
