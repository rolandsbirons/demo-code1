<?php

namespace App\Http\Resources\Test;

use Illuminate\Http\Resources\Json\Resource;

class TestQuestionResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'comment'           => $this->comment ?? null,
			'parent'            => self::collection($this->parent ?? []),
			'parent_id'         => $this->parent_id ?? null,
			'question_category' => $this->question_category ?? null,
			'test_answer'       => ($this->test_type ?? null) === 1 ? false : null,
			'test_type'         => $this->test_type ?? null,
			'test_value'        => $this->test_value ?? null,
			'trigger'           => $this->trigger ?? null,
			'uuid'              => $this->uuid ?? null,
		];
	}
}
