<?php

namespace App\Http\Resources\Test;

use Illuminate\Http\Resources\Json\JsonResource;

class TestSettingsResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'color' => $this->color ?? null,
			'name'  => $this->name ?? null,
		];
	}
}
