<?php

namespace App\Http\Resources\Meta;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MetaWorkingHoursResource extends JsonResource
{
	public function toArray($request): array
	{
		$duration = Carbon::parse($this->time_from)->diffInSeconds(Carbon::parse($this->time_to));

		return [
			'day'       => optional($this)->day,
			'time_from' => optional($this)->time_from,
			'time_to'   => optional($this)->time_to,
			'active'    => optional($this)->active,
			'break'     => optional($this)->break,
			'duration'  => gmdate('H:i', $duration),
		];
	}
}
