<?php

namespace App\Http\Resources\ToothCustomStatus;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothCustomStatusListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->name ?? null,
		];
	}
}
