<?php

namespace App\Http\Resources\ToothCustomStatus;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothCustomStatusItemDetailResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'active'          => $this->active ?? null,
			'deletable'       => $this->deletable ?? null,
			'display_type'    => optional($this->display ?? null)->uuid,
			'has_surface'     => $this->has_surface ?? null,
			'icon_text'       => $this->icon_text ?? null,
			'name_cure'       => $this->name_cure ?? null,
			'name_diagnostic' => $this->name_diagnostic ?? null,
			'name_scheduled'  => $this->name_scheduled ?? null,
			'uuid'            => $this->uuid ?? null,
		];
	}
}
