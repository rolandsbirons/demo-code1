<?php

namespace App\Http\Resources\ToothCustomStatus;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothCustomStatusDetailResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'name'     => $this->name ?? null,
			'statuses' => $this->statuses->transform(static function ($status) {
				return [
					'display'         => $status->display->configuration,
					'icon_text'       => $status->icon_text ?? null,
					'name_cure'       => $status->name_cure ?? null,
					'name_diagnostic' => $status->name_diagnostic ?? null,
					'name_scheduled'  => $status->name_scheduled ?? null,
					'uuid'            => $status->uuid ?? null,
				];
			}),
			'uuid'     => $this->uuid ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
