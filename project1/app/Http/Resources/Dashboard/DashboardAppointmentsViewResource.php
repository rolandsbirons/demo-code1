<?php

namespace App\Http\Resources\Dashboard;

use App\Enums\AppointmentTypes;
use App\Helpers\DateFormatHelper;
use Illuminate\Http\Resources\Json\Resource;

class DashboardAppointmentsViewResource extends Resource
{
	public function toArray($request): array
	{
		if (!$this->patient->dob) {
			$patientAge = DateFormatHelper::calculateAgeFromPersonalCode($this->patient->personal_code);
		} else {
			$patientAge = $this->patient->dob->age;
		}

		return [
			'description'     => $this->description ?? null,
			'event_date'      => $this->event_date->toDateString(),
			'event_time_from' => $this->event_time_from ?? null,
			'event_time_to'   => $this->event_time_to ?? null,
			'price'           => $this->price ?? null,
			'status'          => [
				'id'    => $this->status,
				'color' => AppointmentTypes::APPOINTMENT_COLORS[$this->status],
			],
			'patient'         => [
				'age'              => $patientAge,
				'client_card'      => $this->patient->client_card,
				'has_appointments' => $this->patient->hasAppointmentsSuccessful ? true : false,
				'name'             => $this->patient->full_name,
				'phone'            => $this->patient->phone,
				'uuid'             => $this->patient->uuid,
			],
			'doctor'          => [
				'color' => optional($this->user)->color,
				'name'  => optional($this->user)->full_name_short,
				'uuid'  => optional($this->user)->uuid,
			],
			'assistant'       => [
				'color' => optional($this->assistant)->color,
				'name'  => optional($this->assistant)->full_name_short,
				'uuid'  => optional($this->assistant)->uuid,
			],
			'room'            => [
				'name'  => optional($this->room)->name,
				'color' => optional($this->room)->color,
			],
			'visit'           => [
				'visit_number' => optional($this->visit)->visit_number,
				'plan'         => [
					'name' => optional(optional($this->visit)->plan)->name,
				],
			],
			'uuid'            => $this->uuid,
		];
	}
}
