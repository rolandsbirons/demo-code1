<?php

namespace App\Http\Resources\Dashboard;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DashboardAppointmentsViewCollection extends ResourceCollection
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		$dashboardAppointmentCollection = collect(DashboardAppointmentsViewResource::collection($this->collection));

		return [
			'data' => $dashboardAppointmentCollection->groupBy('event_date'),
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'users' => $this->getUsersByType([1, 2]),
			],
		];
	}
}
