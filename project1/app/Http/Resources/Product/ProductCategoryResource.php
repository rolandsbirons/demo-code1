<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategoryResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'name'  => $this->name ?? null,
			'uuid'  => $this->uuid ?? null,
			'style' => [
				'color' => $this->color ?? null,
			],
		];
	}
}
