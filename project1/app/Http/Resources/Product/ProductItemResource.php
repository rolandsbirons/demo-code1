<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductItemResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'costs'            => $this->costs ?? null,
			'product_category' => $this->product_category ?? null,
			'product_name'     => $this->product_name ?? null,
			'product_price'    => $this->product_price ?? null,
			'product_tax'      => $this->product_tax ?? null,
			'product_time'     => $this->product_time ?? null,
			'product_unit'     => $this->product_unit ?? null,
			'sku'              => $this->sku ?? null,
			'surface'          => $this->surface ?? null,
			'uuid'             => $this->uuid ?? null,
		];
	}
}
