<?php

namespace App\Http\Resources\Product;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'products' => $this->products ?? [],
			'uuid'     => $this->uuid ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'tax_list'  => $this->getMetaTaxes(),
				'title'     => $this->name ?? null,
				'unit_list' => ['CM', 'M', 'KG', 'PC', 'PK'],
			],
		];
	}
}
