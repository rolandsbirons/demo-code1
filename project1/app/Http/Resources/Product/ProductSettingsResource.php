<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductSettingsResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'color' => $this->color ?? null,
			'name'  => $this->name ?? null,
			'uuid'  => $this->uuid ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
