<?php

namespace App\Http\Resources\Calendar;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarViewWeekCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		return [
			'data' => CalendarViewWeekResource::collection($this->collection),
		];
	}
}
