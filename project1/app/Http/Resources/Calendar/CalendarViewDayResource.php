<?php

namespace App\Http\Resources\Calendar;

use App\Enums\AppointmentTypes;
use App\Enums\DateTypes;
use App\Helpers\DateFormatHelper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class CalendarViewDayResource extends Resource
{
	public function toArray($request)
	{
		$eventsObject = collect([]);
		$appointmentColors = AppointmentTypes::APPOINTMENT_COLORS;

		$events = $this['appointments']->transform(static function ($appointment) use ($appointmentColors) {
			$suffixName = '';

			if (!optional($appointment->patient)->dob) {
				$patientAge = DateFormatHelper::calculateAgeFromPersonalCode($appointment->patient->personal_code);
			} else {
				$patientAge = $appointment->patient->dob->age;
			}

			if ($patientAge < 18 && $patientAge) {
				$suffixName .= ' (' . $patientAge . ')';
			}

			if (optional($appointment)->description) {
				$charCountOfDescription = strlen(trim($appointment->description));

				if ($charCountOfDescription >= 1) {
					$suffixName .= ' (K)';
				}
			}

			$hasAppointments = $appointment->patient->hasAppointmentsSuccessful ? true : false;

			if (!$hasAppointments) {
				$suffixName .= ' (J)';
			}

			$timeFrom = Carbon::parse($appointment->event_date->toDateString() . ' ' . $appointment->event_time_from);
			$timeTo = Carbon::parse($appointment->event_date->toDateString() . ' ' . $appointment->event_time_to);
			$appointmentMinutes = $timeFrom->diffInMinutes($timeTo);

			return [
				'id'            => $appointment->uuid,
				'title'         => optional($appointment->patient)->full_name . $suffixName,
				'resourceId'    => $appointment->room->uuid,
				'event_minutes' => $appointmentMinutes,
				'start'         => $appointment->event_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $appointment->event_time_from,
				'end'           => $appointment->event_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $appointment->event_time_to,
				'color'         => $appointmentColors[$appointment->status],
			];
		})->toArray();

		$roomWorkers = $this['schedules']->transform(static function ($r) {
			return [
				'id'         => $r['uuid'],
				'title'      => '(' . $r['time_from'] . ' - ' . $r['time_to'] . ') ' . optional($r['user'])->full_name_short,
				'resourceId' => $r['room']->uuid,
				'start'      => $r['schedule_date']->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $r['time_to'],
				'end'        => $r['schedule_date']->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $r['time_from'],
				'color'      => $r['user']->color,
				'textColor'  => '#fff',
				'allDay'     => true,
			];
		})->toArray();

		$eventsObject = $eventsObject->merge($events);
		$eventsObject = $eventsObject->merge($roomWorkers);

		return $eventsObject;
	}
}
