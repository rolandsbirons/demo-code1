<?php

namespace App\Http\Resources\Calendar;

use Illuminate\Http\Resources\Json\Resource;

class CalendarViewWeekWorkingResource extends Resource
{
	public function toArray($request): array
	{
		$dayOfWeek = $this->schedule_date->dayOfWeek;

		if ($dayOfWeek === 0) {
			$dayOfWeek = 7;
		}

		return [
			'daysOfWeek' => [$dayOfWeek],
			'startTime'  => $this->time_from,
			'endTime'    => $this->time_to,
		];
	}
}
