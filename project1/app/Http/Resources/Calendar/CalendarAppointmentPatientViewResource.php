<?php

namespace App\Http\Resources\Calendar;

use App\Enums\TeethMapTypes;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarAppointmentPatientViewResource extends JsonResource
{
	public function toArray($request): array
	{
		$this->load([
			'toothPlan',
			'toothPlan.visits',
		]);

		return [
			'client_card'   => optional($this)->client_card,
			'dob'           => $this->dob ? $this->dob->toDateString() : null,
			'first_name'    => optional($this)->first_name,
			'last_name'     => optional($this)->last_name,
			'personal_code' => optional($this)->personal_code,
			'phone'         => optional($this)->phone,
			'uuid'          => optional($this)->uuid,
			'tooth_plans'   => $this->toothPlan->transform(static function ($plan) {
				$visits = $plan->visits->transform(static function ($visit) use ($plan) {
					$name = $plan->is_default ?
						TeethMapTypes::DEFAULT_PLAN_NAME :
						$plan->name . ' [' . $visit->visit_number . ']';

					return [
						'disabled' => $visit->disabled,
						'name'     => $name,
						'uuid'     => $visit->uuid,
					];
				});

				return [
					'name'   => optional($plan)->name,
					'uuid'   => optional($plan)->uuid,
					'visits' => $visits,
				];
			}),
		];
	}
}

