<?php

namespace App\Http\Resources\Calendar;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarViewRoomsCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		return [
			'data' => CalendarViewRoomsResource::collection($this->collection),
		];
	}
}
