<?php

namespace App\Http\Resources\Calendar;

use App\Enums\DateTypes;
use App\Helpers\DateFormatHelper;
use Illuminate\Http\Resources\Json\Resource;

class CalendarViewMonthResource extends Resource
{
	public function toArray($request): array
	{
		$workingTimes = '';
		$recordsByRooms = $this->resource->groupBy('room_id');
		$lastRoomObject = null;

		foreach ($recordsByRooms as $recordsByRoom) {
			$lastRoomObject = $recordsByRoom->first();

			$workingTimes .= '<strong>' . $lastRoomObject->room->name . '</strong><br />';

			$schedules = $recordsByRoom->where('protected', false);

			$scheduleTimes = (clone $schedules)->transform(static function ($t) {
				return [
					'time_from' => $t->time_from,
					'time_to'   => $t->time_to,
				];
			});
			$appointmentTimes = $recordsByRoom->where('protected', true)->transform(static function ($t) {
				return [
					'time_from' => $t->time_from,
					'time_to'   => $t->time_to,
				];
			});

			$freeTimes = DateFormatHelper::generateFreeTimesFromPeriod($scheduleTimes, $appointmentTimes);
			$filteredFreeTimes = $freeTimes->filter(static function ($time) {
				return $time['start'] !== $time['end'];
			});

			$lastProtected = null;
			foreach ($schedules as $schedule) {
				$prefix = '';
				if ($lastProtected !== $schedule->protected) {
					$lastProtected = $schedule->protected;

					if (!$lastProtected) {
						$prefix = 'WORKING_HOURS<br />';
					}
				}

				$workingTimes .= $prefix . $schedule->time_from . ' - ' . $schedule->time_to . '<br />';
			}

			if ($filteredFreeTimes->isNotEmpty()) {
				$workingTimes .= 'FREE_TIMES<br />';
			}
			foreach ($filteredFreeTimes as $freeTime) {
				$workingTimes .= $freeTime['start'] . ' - ' . $freeTime['end'] . '<br />';
			}

			if ($schedules->count() >= 1) {
				$workingTimes .= '<br />';
			}
		}

		return [
			'id'    => $lastRoomObject->uuid,
			'title' => $workingTimes,
			'start' => $lastRoomObject->schedule_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $lastRoomObject->time_from,
			'end'   => $lastRoomObject->schedule_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $lastRoomObject->time_to,
		];
	}
}
