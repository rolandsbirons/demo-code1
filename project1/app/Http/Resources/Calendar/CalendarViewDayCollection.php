<?php

namespace App\Http\Resources\Calendar;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarViewDayCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		$eventCollection = collect(CalendarViewDayResource::collection($this->collection))
			->flatten(1)
			->toArray();

		return [
			'data' => $eventCollection,
		];
	}
}
