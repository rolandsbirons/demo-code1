<?php

namespace App\Http\Resources\Calendar;

use App\Enums\DateTypes;
use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarAppointmentResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		$this->load([
			'author',
			'patient',
			'room',
			'visit',
		]);

		return [
			'assistant_uuid' => optional($this->assistant)->uuid,
			'description'    => $this->description,
			'doctor_uuid'    => optional($this->user)->uuid,
			'event_date'     => $this->event_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE),
			'patient_uuid'   => optional($this->patient)->uuid,
			'price'          => $this->price,
			'room_uuid'      => optional($this->room)->uuid,
			'status'         => $this->status,
			'time_from'      => $this->event_time_from,
			'time_to'        => $this->event_time_to,
			'visit_uuid'     => optional($this->visit)->uuid,
			'user'           => [
				'created_at' => $this->created_at->format(DateTypes::ACTION_LOGS_FORMAT_DATE_TIME),
				'name'       => optional($this->author)->full_name,
			],
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'assistant_list' => $this->getUsersInBranch($this->room->branch_id, [2]),
				'doctor_list'    => $this->getUsersInBranch($this->room->branch_id, [1]),
			],
		];
	}

}
