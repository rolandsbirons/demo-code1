<?php

namespace App\Http\Resources\Calendar;

use App\Enums\DateTypes;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarViewMonthCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		$collection = collect($this->collection)->groupBy(static function ($item) {
			return $item['schedule_date']->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE);
		});

		return [
			'data' => collect(CalendarViewMonthResource::collection($collection))->values(),
		];
	}
}
