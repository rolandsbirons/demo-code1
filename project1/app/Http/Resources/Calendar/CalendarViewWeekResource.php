<?php

namespace App\Http\Resources\Calendar;

use App\Enums\AppointmentTypes;
use App\Enums\DateTypes;
use App\Helpers\DateFormatHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarViewWeekResource extends JsonResource
{
	public function toArray($request): array
	{
		$suffixName = '';

		if (!optional($this->patient)->dob) {
			$patientAge = DateFormatHelper::calculateAgeFromPersonalCode($this->patient->personal_code);
		} else {
			$patientAge = $this->patient->dob->age;
		}

		if ($patientAge < 18 && $patientAge) {
			$suffixName .= ' (' . $patientAge . ')';
		}

		if (optional($this)->description) {
			$charCountOfDescription = strlen(trim($this->description));

			if ($charCountOfDescription >= 1) {
				$suffixName .= ' (K)';
			}
		}

		$hasAppointments = $this->patient->hasAppointmentsSuccessful ? true : false;

		if (!$hasAppointments) {
			$suffixName .= ' (J)';
		}

		return [
			'color' => AppointmentTypes::APPOINTMENT_COLORS[$this->status],
			'end'   => $this->event_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $this->event_time_to,
			'id'    => $this->uuid,
			'start' => $this->event_date->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) . 'T' . $this->event_time_from,
			'title' => $this->patient->full_name . $suffixName,
		];
	}
}
