<?php

namespace App\Http\Resources\Calendar;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarAppointmentPatientsResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'client_card'   => optional($this)->client_card,
			'first_name'    => optional($this)->first_name,
			'last_name'     => optional($this)->last_name,
			'personal_code' => optional($this)->personal_code,
			'uuid'          => optional($this)->uuid,
		];
	}
}
