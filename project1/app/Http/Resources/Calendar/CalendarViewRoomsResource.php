<?php

namespace App\Http\Resources\Calendar;

use App\Enums\DateTypes;
use App\Helpers\FullCalendarHelper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CalendarViewRoomsResource extends JsonResource
{
	public function toArray($request): array
	{
		$scheduleBackground = $this->schedules->transform(static function ($s) {
			$timeFrom = Carbon::parse($s->time_from);
			$timeTo = Carbon::parse($s->time_to);

			$dayOfWeek = $s->schedule_date->dayOfWeek;

			if ($dayOfWeek === 0) {
				$dayOfWeek = 7;
			}

			return [
				'daysOfWeek' => [$dayOfWeek],
				'startTime'  => $timeFrom->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME),
				'endTime'    => $timeTo->format(DateTypes::CARBON_DEFAULT_FORMAT_TIME),
			];
		});

		$roomName = FullCalendarHelper::generateResourceCircleTitle($this->name ?? null, $this->color ?? '#000');

		return [
			'id'            => $this->uuid ?? null,
			'title'         => $roomName,
			'businessHours' => (\count($scheduleBackground) === 0) ? [
				'startTime' => '23:59',
				'endTime'   => '23:59',
			] : $scheduleBackground,
		];
	}
}
