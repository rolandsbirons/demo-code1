<?php

namespace App\Http\Resources\Calendar;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CalendarViewWeekWorkingCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		$workingCollection = collect(CalendarViewWeekWorkingResource::collection($this->collection));

		if ($workingCollection->isEmpty()) {
			$workingCollection = [
				'daysOfWeek' => [1, 2, 3, 4, 5, 6, 7],
				'startTime'  => '00:01:00',
				'endTime'    => '00:02:00',
			];
		}

		return [
			'meta' => [
				'business_hours' => $workingCollection,
			],
		];
	}
}
