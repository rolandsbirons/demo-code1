<?php

namespace App\Http\Resources\ToothStatusConfig;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothStatusConfigListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid' => $this->uuid ?? null,
			'name' => $this->name ?? null,
		];
	}
}
