<?php

namespace App\Http\Resources\ToothStatusConfig;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothStatusConfigSettingsResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid' => $this->uuid ?? null,
			'name' => $this->name ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
