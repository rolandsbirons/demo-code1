<?php

namespace App\Http\Resources\ToothStatusConfig;

use Illuminate\Http\Resources\Json\JsonResource;

class ToothStatusConfigDetailResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'          => $this->uuid ?? null,
			'configuration' => $this->configuration ?? [],
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
