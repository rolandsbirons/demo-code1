<?php

namespace App\Http\Resources\Patient;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class PatientViewResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		$doctor = optional($this)->doctor ? optional($this->doctor ?? null)->first() : null;
		$assistants = optional($this)->assistants ? optional($this->assistants ?? null)->pluck('uuid') : [];

		return [
			'address'       => optional($this)->address,
			'assistants'    => $assistants,
			'branches'      => optional($this->branches ?? collect([]))->pluck('uuid'),
			'client_card'   => optional($this)->client_card,
			'comment'       => optional($this)->description,
			'dob'           => optional($this)->dob ? $this->dob->toDateString() : null,
			'doctor'        => optional($doctor)->uuid,
			'email'         => optional($this)->email,
			'first_name'    => optional($this)->first_name,
			'gender'        => optional($this)->gender,
			'insurance'     => optional($this)->insurance,
			'job'           => optional($this)->job,
			'last_name'     => optional($this)->last_name,
			'personal_code' => optional($this)->personal_code,
			'phone'         => optional($this)->phone,
			'phone2'        => optional($this)->phone2,
			'user_status'   => optional($this)->user_status,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'assistants' => $this->getUsersByType([1, 2]),
				'branches'   => $this->getMetaBranches(),
				'doctors'    => $this->getUsersByType([1]),
				'insurances' => $this->getMetaInsurance(),
				'jobs'       => $this->getMetaJobStatuses(),
				'title'      => optional($this)->full_name,
			],
		];
	}
}
