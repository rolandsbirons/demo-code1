<?php

namespace App\Http\Resources\Patient;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientTableResource extends JsonResource
{
	public function toArray($request): array
	{
		$secondPhone = strlen($this->phone2) > 3 ? (', ' . optional($this)->phone2) : '';
		$branches = $this->branches->pluck('name')->implode(', ');

		$doctorText = null;
		$doctor = optional($this)->doctor;
		if ($doctor->count() > 0) {
			$doctorText = $doctor->first()->full_name;
		}

		return [
			'branches'      => $branches,
			'client_card'   => optional($this)->client_card,
			'comment'       => optional($this)->description,
			'doctor'        => $doctorText,
			'email'         => optional($this)->email,
			'first_name'    => optional($this)->first_name,
			'gender'        => optional($this)->gender,
			'insurance'     => optional($this)->insurance,
			'last_name'     => optional($this)->last_name,
			'personal_code' => optional($this)->personal_code,
			'phone'         => optional($this)->phone . '' . $secondPhone,
			'user_status'   => optional($this)->user_status,
			'uuid'          => optional($this)->uuid,
		];
	}
}
