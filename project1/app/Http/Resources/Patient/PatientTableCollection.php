<?php

namespace App\Http\Resources\Patient;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PatientTableCollection extends ResourceCollection
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'data' => PatientTableResource::collection($this->collection),
			'meta' => [
				'branch_list'         => $this->getMetaBranches(),
				'insurance_companies' => $this->getMetaInsurance(),
				'users'               => $this->getUsersByType([1]),
			],
		];
	}
}
