<?php

namespace App\Http\Resources\Patient\Plans;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PatientToothPlanCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		return [
			'data' => PatientToothPlanResource::collection($this->collection),
		];
	}
}
