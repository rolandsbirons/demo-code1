<?php

namespace App\Http\Resources\Patient\Plans;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientToothPlanResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'appointment_id' => $this->appointment_id ?? null,
			'approved'       => $this->approved ?? null,
			'can_delete'     => $this->can_delete ?? null,
			'finished_at'    => $this->finished_at ?? null,
			'has_tooths'     => $this->teeth()->count() > 0,
			'is_default'     => $this->is_default ?? null,
			'name'           => $this->name ?? null,
			'order'          => $this->order ?? null,
			'uuid'           => $this->uuid ?? null,
		];
	}
}
