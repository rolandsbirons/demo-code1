<?php

namespace App\Http\Resources\Patient\TeethMap;

use App\Enums\TeethMapTypes;
use App\Helpers\DateFormatHelper;
use App\Helpers\TeethMapHelper;
use Illuminate\Http\Resources\Json\Resource;

class PatientTeethMapViewResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'teeth_map_done'    => $this->tooth_map_done ?? false,
			'teeth_procedures'  => $this->teeth->transform(static function ($s) {
				$hasPlan = $s->approved_date ? true : false;

				return [
					'plans'            => !empty($s->plans) ? $s->plans : [],
					'status'           => [
						'name'        => optional($s->status)->name_diagnostic,
						'uuid'        => optional($s->status)->uuid,
						'display'     => optional($s->status)->display_type,
						'icon'        => optional($s->status)->icon_text,
						'has_surface' => optional($s->status)->has_surface,
						'name_object' => [
							'treatment' => optional($s->status)->name_diagnostic,
							'planned'   => optional($s->status)->name_scheduled,
							'cured'     => optional($s->status)->name_cure,
						],
						'style'       => TeethMapHelper::filterInActiveConfiguration(optional(optional($s->status)->display)->configuration),
					],
					'done_before'      => $s->done_before,
					'done_after'       => $s->done_after,
					'surface_problems' => [
						1 => $s->surface1,
						2 => $s->surface2,
						3 => $s->surface3,
						4 => $s->surface4,
						5 => $s->surface5,
						6 => $s->surface6,
						7 => $s->surface7,
						8 => $s->surface8,
					],
					'tooth_number'     => $s->tooth_number,
					'uuid'             => $s->uuid,
					'services'         => $s->toothServices->transform(static function ($s) use ($hasPlan) {
						$time = DateFormatHelper::date2min($s->time ?? 0);

						return [
							'done'           => $s->done,
							'paid'           => $s->invoice_id ? true : false,
							'uuid'           => $s->uuid,
							'visit'          => $s->visit,
							'time'           => $time,
							'time_options'   => TeethMapHelper::generateTimeOptions($time),
							'price'          => $s->price,
							'price_options'  => TeethMapHelper::generatePriceOptions($s),
							'history'        => [],
							'service'        => [
								'name'  => $s->service->service_name ?? null,
								'sku'   => $s->service->sku ?? null,
								'style' => [
									'color' => optional(optional($s->service)->category)->color,
								],
							],
							'plan_has_added' => $hasPlan,
						];
					}),
					'has_plan'         => $hasPlan,
					'tooth_finished'   => $s->finished,
				];
			}),
			'teeth_status_list' => $this->teethStatuses->transform(static function ($q) {
				return [
					'tooth_id'     => $q->tooth_id,
					'tooth_status' => $q->tooth_status,
					'place_id'     => TeethMapTypes::TEETH_PLACES[$q->tooth_id] ?? null,
				];
			}),
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->full_name ?? null,
			],
		];
	}
}
