<?php

namespace App\Http\Resources\Patient\Tests;

use Illuminate\Http\Resources\Json\Resource;

class PatientTestViewResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'name'        => optional($this->question)->name,
			'questions'   => collect(PatientTestQuestionsCollection::collection($this->answers))
				->groupBy('question_group')->toArray(),
			'approved_at' => $this->approved_at ? $this->approved_at->format('d-m-Y H:m') : null,
		];
	}
}
