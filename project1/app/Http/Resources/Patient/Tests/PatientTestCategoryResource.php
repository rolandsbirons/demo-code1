<?php

namespace App\Http\Resources\Patient\Tests;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientTestCategoryResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->name ?? null,
			'style' => [
				'color' => $this->color ?? null,
			],
		];
	}
}
