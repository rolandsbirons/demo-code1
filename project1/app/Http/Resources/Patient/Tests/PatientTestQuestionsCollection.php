<?php

namespace App\Http\Resources\Patient\Tests;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientTestQuestionsCollection extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'answer'         => ($this->question->test_type === 1) ? (bool)$this->question_answer : $this->question_answer,
			'comment'        => $this->question->comment,
			'is_parent'      => $this->question->parent_id > 0,
			'name'           => $this->question->test_value,
			'parent_id'      => $this->question->parent_id,
			'question_group' => $this->question->parent_id > 0 ? $this->question->parent_id : $this->question->id,
			'question_type'  => $this->question->test_type,
			'trigger'        => $this->question->trigger,
			'order'          => $this->question->order,
			'uuid'           => $this->uuid,
		];
	}
}
