<?php

namespace App\Http\Resources\Patient\Header;

use App\Helpers\DateFormatHelper;
use App\Helpers\TeethMapHelper;
use Illuminate\Http\Resources\Json\Resource;

class PatientHeaderPaymentServiceResource extends Resource
{
	public function toArray($request): array
	{
		$time = DateFormatHelper::date2min($this->time ?? 0);

		return [
			'discount'         => 0,
			'active'          => true,
			'discount_strict' => $this->service->category->group->discount,
			'name'            => $this->service->service_name ?? null,
			'price'           => $this->price,
			'price_options'   => TeethMapHelper::generatePriceOptions($this),
			'sku'             => $this->service->sku ?? null,
			'time'            => $time,
			'time_options'    => TeethMapHelper::generateTimeOptions($time),
			'tooth_number'    => $this->tooth->tooth_number, //because of javascript helper class
			'uuid'            => $this->uuid ?? null,
			'surface_problems' => [
				1 => optional($this->tooth)->surface1,
				2 => optional($this->tooth)->surface2,
				3 => optional($this->tooth)->surface3,
				4 => optional($this->tooth)->surface4,
				5 => optional($this->tooth)->surface5,
				6 => optional($this->tooth)->surface6,
				7 => optional($this->tooth)->surface7,
				8 => optional($this->tooth)->surface8,
			],
		];
	}
}
