<?php

namespace App\Http\Resources\Patient\Header;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PatientHeaderPaymentServiceCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		return [
			'data' => PatientHeaderPaymentServiceResource::collection($this->collection),
		];
	}
}
