<?php

namespace App\Http\Resources\Patient\Dashboard;

use App\Enums\AppointmentTypes;
use Illuminate\Http\Resources\Json\Resource;

class PatientDashboardAppointmentResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'description'     => $this->description,
			'event_date'      => $this->event_date->toDateString(),
			'event_time_from' => $this->event_time_from,
			'event_time_to'   => $this->event_time_to,
			'price'           => $this->price,
			'status'          => [
				'id'    => $this->status,
				'color' => AppointmentTypes::APPOINTMENT_COLORS[$this->status],
			],
			'doctor'          => [
				'color' => optional($this->user)->color,
				'name'  => optional($this->user)->full_name_short,
				'uuid'  => optional($this->user)->uuid,
			],
			'assistant'       => [
				'color' => optional($this->assistant)->color,
				'name'  => optional($this->assistant)->full_name_short,
				'uuid'  => optional($this->assistant)->uuid,
			],
			'room'            => [
				'name'  => optional($this->room)->name,
				'color' => optional($this->room)->color,
			],
			'visit'           => [
				'visit_number' => optional($this->visit)->visit_number,
				'plan'         => [
					'name' => optional(optional($this->visit)->plan)->name,
				],
			],
			'uuid'            => $this->uuid,
		];
	}
}
