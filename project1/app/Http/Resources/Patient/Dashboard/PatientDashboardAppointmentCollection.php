<?php

namespace App\Http\Resources\Patient\Dashboard;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PatientDashboardAppointmentCollection extends ResourceCollection
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'data' => PatientDashboardAppointmentResource::collection($this->collection),
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'users' => $this->getUsersByType([1, 2]),
			],
		];
	}
}
