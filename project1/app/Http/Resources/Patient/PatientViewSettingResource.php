<?php

namespace App\Http\Resources\Patient;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class PatientViewSettingResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'user_status' => optional($this)->user_status,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => optional($this)->full_name,
			],
		];
	}
}
