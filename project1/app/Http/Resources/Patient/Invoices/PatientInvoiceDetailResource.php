<?php

namespace App\Http\Resources\Patient\Invoices;

use App\Enums\DateTypes;
use App\Enums\InvoiceTypes;
use App\Helpers\DateFormatHelper;
use App\Helpers\TeethMapHelper;
use Illuminate\Http\Resources\Json\JsonResource;

class PatientInvoiceDetailResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'created_at'  => $this->created_at ? $this->created_at->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) : null,
			'invoice_due' => $this->invoice_due ? $this->invoice_due->format(DateTypes::CARBON_DEFAULT_FORMAT_DATE) : null,
			'invoice_id'  => $this->invoice_id ?? null,
			'uuid'        => $this->uuid ?? null,
			'locked'      => $this->locked ?? true,
			'services'    => $this->items->where('type', InvoiceTypes::INVOICE_TYPE_SERVICE)->transform(static function ($item) {
				return [
					'group'           => $item->service->category->group->uuid,
					'description'     => $item->description ?? null,
					'discount'        => $item->discount ?? null,
					'discount_strict' => $item->discount_strict ?? null,
					'name'            => $item->name ?? null,
					'price'           => $item->price ?? null,
					'price_insurance' => $item->discount_strict ? $item->price : $item->price_insurance,
					'price_original'  => $item->price_original ?? null,
					'price_options'   => TeethMapHelper::generatePriceOptions($item->teethService),
					'saved'           => true,
					'sku'             => $item->sku ?? null,
					'sku_custom'      => $item->sku_custom ?? null,
					'sku_description' => $item->sku_description ?? null,
					'tax'             => $item->tax ?? null,
					'uuid'            => $item->uuid ?? null,
				];
			}),
			'products'    => $this->items->where('type', InvoiceTypes::INVOICE_TYPE_PRODUCT)->transform(static function ($item) {
				return [
					'count' => $item->count ?? null,
					'name'  => $item->name ?? null,
					'price' => $item->price ?? null,
					'sku'   => $item->sku ?? null,
					'tax'   => $item->tax ?? null,
					'uuid'  => $item->uuid ?? null,
					'saved' => true,
				];
			}),
		];
	}

}
