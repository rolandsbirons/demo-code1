<?php

namespace App\Http\Resources\Patient\Invoices\Item;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceItemMethodResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'amount'      => $this->amount ?? 0,
			'amount_paid' => $this->amount_paid ?? 0,
			'invoice_sku' => $this->invoice_sku ?? null,
			'status'      => $this->status ?? null,
			'status_db'   => $this->status ?? null,
			'type'        => $this->type ?? null,
			'uuid'        => $this->uuid ?? null,
		];
	}

}
