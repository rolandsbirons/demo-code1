<?php

namespace App\Http\Resources\Patient\Invoices\Item;

use Illuminate\Http\Resources\Json\Resource;

class InvoiceItemSearchServicesResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'name'  => $this->service_name ?? null,
			'sku'   => $this->sku ?? null,
			'uuid'  => $this->uuid ?? null,
			'price' => $this->price ?? null,
		];
	}
}
