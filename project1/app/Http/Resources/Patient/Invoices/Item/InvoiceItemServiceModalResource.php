<?php

namespace App\Http\Resources\Patient\Invoices\Item;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceItemServiceModalResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'     => $this->uuid ?? null,
			'services' => $this->items->where('type', 1)->transform(static function ($item) {
				return [
					'description'     => $item->description ?? null,
					'discount'        => $item->discount ?? null,
					'discount_strict' => $item->discount_strict ?? null,
					'locked'          => $item->method_id ? true : false,
					'name'            => $item->name ?? null,
					'price'           => $item->price ?? null,
					'price_insurance' => $item->price_insurance ?? null,
					'price_original'  => $item->price_original ?? null,
					'sku'             => $item->sku ?? null,
					'sku_custom'      => $item->sku_custom ?? null,
					'sku_description' => $item->sku_description ?? null,
					'tax'             => $item->tax ?? null,
					'uuid'            => $item->uuid ?? null,
				];
			}),
			'products' => $this->items->where('type', 2)->transform(static function ($item) {
				return [
					'count'           => $item->count ?? 0,
					'discount'        => $item->discount ?? null,
					'discount_strict' => $item->discount_strict ?? null,
					'locked'          => $item->method_id ? true : false,
					'name'            => $item->name ?? null,
					'price'           => $item->price ?? null,
					'price_original'  => $item->price_original ?? null,
					'sku'             => $item->sku ?? null,
					'tax'             => $item->tax ?? null,
					'uuid'            => $item->uuid ?? null,
				];
			}),
		];
	}

}
