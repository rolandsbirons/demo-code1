<?php

namespace App\Http\Resources\Patient\Invoices;

use Illuminate\Http\Resources\Json\Resource;

class PatientInvoiceViewResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->invoice_id ?? null,
			'style' => [
				'color' => null,
			],
		];
	}
}
