<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserDetailsResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'certificate'      => $this->certificate ?? null,
			'certificate_date' => $this->certificate_date ?? null,
			'email'            => $this->email ?? null,
			'personal_code'    => $this->personal_code ?? null,
			'phone'            => $this->phone ?? null,
			'sanitary_book'    => $this->sanitary_book ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->full_name ?? null,
			],
		];
	}
}
