<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\Resource;

class UserWorkTimeResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'active'    => $this->active ?? null,
			'breaks'    => $this->breaks()->transform(function ($break) {
				return [
					'active'    => $break->active ?? null,
					'day'       => $break->day ?? null,
					'id'        => $break->id ?? null,
					'time_from' => $break->time_from ?? null,
					'time_to'   => $break->time_to ?? null,
				];
			}),
			'day'       => $this->day ?? null,
			'id'        => $this->id ?? null,
			'time_from' => $this->time_from ?? null,
			'time_to'   => $this->time_to ?? null,
		];
	}
}
