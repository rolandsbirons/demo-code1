<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class UserListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->full_name ?? null,
			'style' => [
				'color' => $this->color ?? '#00000',
			],
		];
	}
}
