<?php

namespace App\Http\Resources\User;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSettingResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		$this->load([
			'branches',
			'positions',
		]);

		return [
			'branches'    => $this->branches->pluck('uuid'),
			'color'       => $this->color ?? false,
			'deletable'   => $this->deletable ?? false,
			'first_name'  => $this->first_name ?? null,
			'last_name'   => $this->last_name ?? null,
			'positions'   => $this->positions->pluck('uuid'),
			'user_status' => (int)$this->user_status,
			'username'    => $this->username ?? false,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'branches'  => $this->getMetaBranches(),
				'positions' => $this->getMetaPositions(),
				'title'     => $this->full_name ?? null,
			],
		];
	}
}
