<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserWorkTimeCollection extends JsonResource
{
	public function toArray($request): array
	{
		$this->load(['workTimes']);

		return [
			'work_times' => UserWorkTimeResource::collection($this->workTimes ?? []),
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->full_name ?? null,
			],
		];
	}
}
