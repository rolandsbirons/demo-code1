<?php

namespace App\Http\Resources\Service;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceGroupResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'name' => $this->name ?? null,
			'uuid' => $this->uuid ?? null,
		];
	}
}
