<?php

namespace App\Http\Resources\Service;

use App\Enums\ProductUnitTypes;
use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceDetailResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'services' => $this->services->transform(static function($service){
				return [
					'costs'              => $service->costs ?? null,
					'service_name'       => $service->service_name ?? null,
					'service_price_from' => $service->service_price_from ?? null,
					'service_price_to'   => $service->service_price_to ?? null,
					'service_time'       => $service->service_time ?? null,
					'sku'                => $service->sku ?? null,
					'uuid'               => $service->uuid ?? null,
				];
			}),
			'uuid'     => $this->uuid ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'tax_list'  => $this->getMetaTaxes(),
				'title'     => $this->name ?? null,
				'unit_list' => ProductUnitTypes::PRODUCT_UNITS_SHORT,
			],
		];
	}
}
