<?php

namespace App\Http\Resources\Service;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceItemResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'costs'              => $this->costs ?? null,
			'service_category'   => $this->service_category ?? null,
			'service_name'       => $this->service_name ?? null,
			'service_price_from' => $this->service_price_from ?? null,
			'service_price_to'   => $this->service_price_to ?? null,
			'service_tax'        => $this->service_tax ?? null,
			'service_time'       => $this->service_time ?? null,
			'service_unit'       => $this->service_unit ?? null,
			'sku'                => $this->sku ?? null,
			'surface'            => $this->surface ?? null,
			'uuid'               => $this->uuid ?? null,
		];
	}
}
