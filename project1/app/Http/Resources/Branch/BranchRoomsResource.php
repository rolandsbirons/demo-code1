<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchRoomsResource extends JsonResource
{
	public function toArray($request): array
	{
		$this->load(['rooms']);

		$rooms = $this->rooms ?? collect([]);

		return $rooms->transform(function ($room) {
			return [
				'color' => $room->color ?? null,
				'name'  => $room->name ?? null,
				'order' => $room->order ?? null,
				'uuid'  => $room->uuid ?? null,
			];
		})->toArray();
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
