<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchDetailsResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'address_actual'   => $this->address_actual ?? null,
			'address_declared' => $this->address_declared ?? null,
			'bank_code'        => $this->bank_code ?? null,
			'bank_name'        => $this->bank_name ?? null,
			'bank_swift'       => $this->bank_swift ?? null,
			'color'            => $this->color ?? null,
			'company_name'     => $this->company_name ?? null,
			'company_number'   => $this->company_number ?? null,
			'email'            => $this->email ?? null,
			'id'               => $this->id ?? null,
			'logo'             => $this->logo ?? null,
			'phone'            => $this->phone ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
