<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchRoomsListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'color'  => $this->color ?? null,
			'hidden' => $this->hidden ?? false,
			'name'   => $this->name ?? null,
			'uuid'   => $this->uuid ?? null,
		];
	}
}
