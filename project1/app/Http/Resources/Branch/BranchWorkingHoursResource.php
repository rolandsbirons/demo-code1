<?php

namespace App\Http\Resources\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchWorkingHoursResource extends JsonResource
{
	public function toArray($request): array
	{
		$this->load(['workTimes']);

		$workingHours = $this->workTimes ?? collect([]);

		return $workingHours->transform(function ($workingTime) {
			return [
				'active'    => $workingTime->active ?? null,
				'day'       => $workingTime->day ?? null,
				'id'        => $workingTime->id ?? null,
				'time_from' => $workingTime->time_from ?? null,
				'time_to'   => $workingTime->time_to ?? null,
			];
		})->toArray();
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
