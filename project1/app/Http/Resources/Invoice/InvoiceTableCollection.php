<?php

namespace App\Http\Resources\Invoice;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceTableCollection extends ResourceCollection
{
	public function toArray($request): array
	{
		return [
			'data' => InvoiceTableResource::collection($this->collection),
			'meta' => [
			],
		];
	}
}
