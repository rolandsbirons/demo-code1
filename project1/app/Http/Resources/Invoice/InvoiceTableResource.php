<?php

namespace App\Http\Resources\Invoice;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceTableResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'invoice_uuid' => $this->uuid ?? null,
			'invoice_id'   => $this->invoice_id ?? null,
//			'uuid'         => optional($this)->uuid,
			'amount'       => number_format(optional($this->method)->amount, 2) ?? 0.00 . '€',
			'amount_paid'  => number_format(optional($this->method)->amount_paid, 2) ?? 0.00 . '€',
//			'amount_full'  => optional($this->method)->amount_paid,
			'patient_name' => $this->patient->full_name,
			'patient_uuid' => $this->patient->uuid,
			'uuid'         => $this->patient->uuid,
			'created_at'   => $this->created_at->toDateTimeString(),
			'updated_at'   => $this->updated_at->toDateTimeString(),
		];
	}
}
