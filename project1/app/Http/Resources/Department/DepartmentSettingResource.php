<?php

namespace App\Http\Resources\Department;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentSettingResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'color' => $this->color ?? null,
			'name'  => $this->name ?? null,
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title' => $this->name ?? null,
			],
		];
	}
}
