<?php

namespace App\Http\Resources\Department;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentDetailsBranchListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid' => $this->uuid ?? null,
		];
	}
}
