<?php

namespace App\Http\Resources\Department;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentListResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'uuid'  => $this->uuid ?? null,
			'name'  => $this->name ?? null,
			'style' => [
				'color' => $this->color ?? '#00000',
			],
		];
	}
}
