<?php

namespace App\Http\Resources\Department;

use App\Traits\MetaFieldsTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentDetailsResource extends JsonResource
{
	use MetaFieldsTrait;

	public function toArray($request): array
	{
		return [
			'branches' => DepartmentDetailsBranchListResource::collection($this->branches ?? []),
		];
	}

	public function with($request): array
	{
		return [
			'meta' => [
				'title'       => $this->name ?? null,
				'branch_list' => $this->getMetaBranches(),
			],
		];
	}
}
