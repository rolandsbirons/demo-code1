<?php

namespace App\Http\Resources\Component\ToothMap;

use App\Helpers\TeethMapHelper;
use Illuminate\Http\Resources\Json\Resource;

class ToothMapCategoryServicesResource extends Resource
{
	public function toArray($request): array
	{
		//discount_strict, price_options and group is used for invoices
		return [
			'discount_strict' => $this->category->group->discount,
			'group'           => $this->category->group->uuid,
			'name'            => $this->service_name ?? null,
			'price'           => $this->service_price_avg ?? null,
			'price_options'   => TeethMapHelper::generatePriceOptions($this),
			'sku'             => $this->sku ?? null,
			'tax'             => $this->service_tax ?? null,
			'time'            => $this->service_time ?? null,
			'uuid'            => $this->uuid ?? null,
		];
	}
}
