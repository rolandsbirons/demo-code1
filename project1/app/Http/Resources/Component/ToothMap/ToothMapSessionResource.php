<?php

namespace App\Http\Resources\Component\ToothMap;

use Illuminate\Http\Resources\Json\Resource;

class ToothMapSessionResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'end'          => $this->session_end ? $this->session_end->toDateString() : null,
			'start'        => $this->session_start ? $this->session_start->toDateString() : null,
			'status'       => $this->status,
			'patient_uuid' => $this->patient->uuid,
			'uuid'         => $this->uuid,
		];
	}
}
