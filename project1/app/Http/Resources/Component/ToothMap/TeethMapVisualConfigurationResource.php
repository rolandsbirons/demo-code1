<?php

namespace App\Http\Resources\Component\ToothMap;

use App\Helpers\TeethMapHelper;
use Illuminate\Http\Resources\Json\Resource;

class TeethMapVisualConfigurationResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'configuration' => TeethMapHelper::filterInActiveConfiguration($this->configuration ?? []),
			'display_id'    => $this->id ?? null,
			'text'          => $this->name ?? null,
			'uuid'          => $this->uuid ?? null,
		];
	}
}
