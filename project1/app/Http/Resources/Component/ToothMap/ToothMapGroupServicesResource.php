<?php

namespace App\Http\Resources\Component\ToothMap;

use Illuminate\Support\Collection;

class ToothMapGroupServicesResource extends Collection
{
	public function toArray(): array
	{
		return [
			'color' => $this->color ?? null,
			'name'  => $this->name ?? null,
			'uuid'  => $this->uuid ?? null,
		];
	}
}
