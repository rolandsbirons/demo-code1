<?php

namespace App\Http\Resources\Component\ToothMap;

use Illuminate\Http\Resources\Json\Resource;

class ToothMapSearchServicesResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'name' => $this->service_name ?? null,
			'sku'  => $this->sku ?? null,
			'uuid' => $this->uuid ?? null,
		];
	}
}
