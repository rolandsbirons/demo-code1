<?php

namespace App\Http\Resources\Component\ToothMap;

use Illuminate\Http\Resources\Json\Resource;

class ToothMapSearchProcedureResource extends Resource
{
	public function toArray($request): array
	{
		return [
			'name_object' => [
				'treatment' => $this->name_diagnostic ?? null,
				'planned'   => $this->name_scheduled ?? null,
				'cured'     => $this->name_cure ?? null,
			],
			'sku'         => $this->sku ?? null,
			'uuid'        => $this->uuid ?? null,
		];
	}
}
