<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$languageKey = 'Content-Language';

		$local = $request->hasHeader($languageKey) ? $request->header($languageKey) : config('app.locale');

		// set laravel localization
		app()->setLocale($local);

		// continue request
		return $next($request);
	}
}
