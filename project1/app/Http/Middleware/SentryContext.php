<?php

namespace App\Http\Middleware;

use Closure;
use Sentry;
use Sentry\State\Scope;

class SentryContext
{
	public function handle($request, Closure $next)
	{
		if (auth()->check() && app()->bound('sentry')) {
			Sentry\configureScope(static function (Scope $scope): void {
				$user = auth()->user();

				$scope->setUser([
					'id'         => $user->id ?? null,
					'company_id' => $user->company_id ?? null,
					'uuid'       => $user->uuid ?? null,
					'email'      => $user->email ?? null,
				]);
			});
		}

		return $next($request);
	}
}