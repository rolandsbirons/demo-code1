<?php

namespace App\Http\Middleware;

use App\Traits\AdminTrait;
use Closure;

class AdminDebugMiddleware
{
	use AdminTrait;

	public function handle($request, Closure $next)
	{
		if (!$this->hasAdminDebug()) {
			return abort(403);
		}

		return $next($request);
	}
}