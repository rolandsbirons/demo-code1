<?php

namespace App\Http\Middleware;

use App\Traits\AdminTrait;
use Closure;

class AdminMiddleware
{
	use AdminTrait;

	public function handle($request, Closure $next)
	{
		if (!$this->isSystemAdmin()) {
			return abort(403);
		}

		return $next($request);
	}
}