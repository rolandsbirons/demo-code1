<?php

namespace App\Traits;

trait CompanyTrait
{

	public function generateCompanyInvoice(): string
	{
		return now()->format('d/m/Y') . '-' . random_int(1, 123443);
	}

}