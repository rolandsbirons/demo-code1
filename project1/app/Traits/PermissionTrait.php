<?php

namespace App\Traits;

use Illuminate\Http\Response;
use JWTAuth;

trait PermissionTrait
{
	public function canAccessFunction($functions, $response = 'boolean')
	{
		if (app()->runningInConsole()) {
			return true;
		}

		$user = JWTAuth::parseToken()->authenticate();

		$userCanAccess = $user->canAction($functions);

		if ($response === 'abort') {
			if (!$userCanAccess) {
				return abort(Response::HTTP_UNAUTHORIZED);
			}

			return true;
		}

		return $userCanAccess;
	}
}