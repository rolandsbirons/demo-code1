<?php

namespace App\Traits;

use App\Enums\UserSettingsTypes;
use App\Models\User\UserSetting;
use Auth;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

trait UserSettingsTrait
{
	public function updateUserSettings($settingsKey, $fieldValue)
	{
		try {
			$settingsAllowedTypes = UserSettingsTypes::toArray();

			$settingsKeyParsed = $this->parseSettingsKey($settingsKey);

			if (isset($settingsAllowedTypes[$settingsKeyParsed])) {
				$filterObject = $settingsAllowedTypes[$settingsKeyParsed];
				$metaType = $filterObject['type'];
				$value = null;

				if ($metaType === 'array') {
					$value = $this->parseUserSettingArray($fieldValue, [
						'inputKey'            => $filterObject['inputKey'],
						'collectionFunctions' => $filterObject['collectionFunctions'] ?? '',
					]);

					$value = json_encode($value);
				}

				if ($metaType === 'boolean') {
					$value = $this->parseUserSettingsBoolean($fieldValue);
				}

				if ($metaType === 'integer') {
					$value = $this->parseUserSettingsInteger($fieldValue);
				}

				return $this->updateUserSettingsDatabase($settingsKey, $value);
			}

			return abort(Response::HTTP_BAD_REQUEST);
		} catch (Exception $e) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function updateUserMultipleSettings($settings)
	{
		try {
			$settingsAllowedTypes = UserSettingsTypes::toArray();
			$settingsAllowedCollection = collect($settingsAllowedTypes);
			foreach ($settings as $settingKey => $settingValue) {
				$databaseKey = $settingsAllowedCollection->filter(static function ($setting) use ($settingKey) {
					return isset($setting['returnVariable']) && $setting['returnVariable'] === $settingKey;
				})->keys()->first();

				$this->updateUserSettings($databaseKey, $settingValue);
			}

			return response(null, Response::HTTP_OK);
		} catch (Exception $exception) {
			return abort(Response::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function getUserSettings($settingsInput)
	{
		$result = null;
		$settingsAllowedTypes = UserSettingsTypes::toArray();

		if (!is_array($settingsInput)) {
			$settingsKeyParsed = $this->parseSettingsKey($settingsInput);
		} else {
			$settingsKeyParsed = $settingsInput;
		}

		if (is_array($settingsKeyParsed)) {
			$arraySettings = [];

			foreach ($settingsInput as $settingsInputSingle) {
				$settingsKeyParsed = $this->parseSettingsKey($settingsInputSingle);
				$singleValue = $this->setSettingValue($settingsInputSingle, $settingsAllowedTypes, $settingsKeyParsed);

				$arraySettings[$singleValue['returnVariable']] = $singleValue['returnValue'];
			}

			$result = $arraySettings;
		} else if (isset($settingsAllowedTypes[$settingsKeyParsed])) {
			$singleValue = $this->setSettingValue($settingsInput, $settingsAllowedTypes, $settingsKeyParsed);

			$result = $singleValue['returnValue'];
		}

		return $result;
	}

	private function setSettingValue($settingsInput, $settingsAllowedTypes, $settingsKeyParsed): array
	{
		$result = [
			'returnValue'    => null,
			'returnVariable' => null,
		];

		$settings = UserSetting::where([
			'setting_key' => $settingsInput,
			'user_id'     => optional(Auth::user())->id,
		])->first();

		if ($settings) {
			$filterType = $settingsAllowedTypes[$settingsKeyParsed]['type'];

			if ($filterType === 'array') {
				$result['returnValue'] = json_decode(optional($settings)->setting_value);
			}

			if ($filterType === 'boolean') {
				$result['returnValue'] = (bool)optional($settings)->setting_value;
			}

			if ($filterType === 'integer') {
				$result['returnValue'] = (int)optional($settings)->setting_value;
			}
		} else {
			$result['returnValue'] = $settingsAllowedTypes[$settingsKeyParsed]['defaultValue'] ?? null;
		}

		$result['returnVariable'] = $settingsAllowedTypes[$settingsKeyParsed]['returnVariable'] ?? null;

		return $result;
	}

	private function updateUserSettingsDatabase($key, $updateValue): bool
	{
		$result = false;

		try {
			if (Auth::check()) {
				$userId = optional(Auth::user())->id;

				$currentUserSettings = UserSetting::where([
					'setting_key' => $key,
					'user_id'     => $userId,
				]);

				if ($currentUserSettings->count() > 0) {
					$currentUserSettings->update([
						'setting_value' => $updateValue,
					]);
				} else {
					$currentUserSettings = new UserSetting;
					$currentUserSettings->user_id = $userId;
					$currentUserSettings->setting_key = $key;
					$currentUserSettings->setting_value = $updateValue;
					$currentUserSettings->save();
				}

				$result = true;
			}
		} catch (Exception $exception) {
			$result = false;
		}

		return $result;
	}

	private function parseUserSettingArray($inputArray, array $filters)
	{
		$inputArray = collect($inputArray);
		$parseFilters = $this->getFilterValues($filters['collectionFunctions']);

		foreach ($parseFilters as $parseFilter) {
			if ($parseFilter['name'] === 'filter') {
				$inputArray = $inputArray->filter(static function ($value) use ($parseFilter) {
					return $value[$parseFilter['value']];
				});
			}

			if ($parseFilter['name'] === 'pluck') {
				$inputArray = $inputArray->pluck($parseFilter['value']);
			}

			if ($parseFilter['name'] === 'unique') {
				$inputArray = $inputArray->unique();
			}

			if ($parseFilter['name'] === 'flatten') {
				$inputArray = $inputArray->flatten();
			}
		}

		return $inputArray;
	}

	private function parseUserSettingsBoolean($fieldValue): bool
	{
		return (bool)$fieldValue;
	}

	private function parseUserSettingsInteger($fieldValue): int
	{
		return (int)$fieldValue;
	}

	private function parseSettingsKey($settingsKey): string
	{
		$settingsKeyParsed = $settingsKey;

		if (Str::contains($settingsKey, '_LOCAL')) {
			$settingsKeyParsed = Str::before($settingsKey, '_LOCAL');
		}

		if (Str::contains($settingsKey, '_GLOBAL')) {
			$settingsKeyParsed = Str::before($settingsKey, '_GLOBAL');
		}

		return $settingsKeyParsed;
	}

	private function getFilterValues($filters): array
	{
		$filterOutput = [];
		$filterList = explode('|', $filters);

		foreach ($filterList as $filter) {
			if (Str::contains($filter, ':')) {
				$filterParts = explode(':', $filter);
				$filterOutput[] = [
					'name'  => $filterParts[0],
					'value' => $filterParts[1],
				];
			} else {
				$filterOutput[] = [
					'name' => $filter,
				];
			}
		}

		return $filterOutput;
	}
}