<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Collection;

trait ReportsTrait
{
	public static function generateReportsTotalFromData(Collection $data, $type = 'sum', array $ignoreFields = [], array $filters = []): Collection
	{
		$result = collect([]);

		if ($data->isNotEmpty()) {
			$dataKeys = array_keys($data->first());
			$calculationData = [];

			foreach ($dataKeys as $dataKey) {
				$calculation = "total_$type";

				if (!in_array($dataKey, $ignoreFields, true)) {
					$calculation = self::getCalculation($data, $dataKey, $type, $filters);
				}

				$calculationData[$dataKey] = $calculation;
			}

			return $result->merge([$calculationData]);
		}

		return $result;
	}

	private static function getCalculation(Collection $data, string $key, string $type, array $filters)
	{
		if (in_array('empty_records_ignored', $filters, true)) {
			$data = $data->filter(static function ($record) use ($key) {
				return $record[$key] !== '';
			});
		}

		if ($type === 'sum') {
			return $data->sum($key);
		}

		if ($type === 'avg') {
			return $data->avg($key);
		}

		return collect([]);
	}

	public static function prepareData(Collection $data, array $formats = [])
	{
		return $data->transform(static function ($record) use ($formats) {
			if (count($formats) > 0) {

				$recordArray = [];

				foreach ($record as $itemKey => $itemValue) {
					if (!empty($itemValue)) {
						if (array_key_exists('currency', $formats) && in_array($itemKey, $formats['currency'], true)) {
							$recordArray[$itemKey] = number_format($itemValue, 2);
						} else if (array_key_exists('round', $formats) && in_array($itemKey, $formats['round'], true)) {
							$recordArray[$itemKey] = round($itemValue);
						} else if (array_key_exists('ratio', $formats) && in_array($itemKey, $formats['ratio'], true)) {
							$recordArray[$itemKey] = number_format($itemValue, 1);
						} else {
							$recordArray[$itemKey] = $itemValue;
						}
					} else {
						$recordArray[$itemKey] = $itemValue;
					}
				}

				return $recordArray;
			}

			return $record;
		});
	}

	public static function getDateByType($viewType, $date): array
	{
		$date = Carbon::parse($date);

		$result = [
			'from' => '',
			'to'   => '',
		];

		if ($viewType === 'day') {
			$result = [
				'from' => $date->toDateString(),
				'to'   => $date->toDateString(),
			];
		}

		if ($viewType === 'week') {
			$result = [
				'from' => $date->startOfWeek()->toDateString(),
				'to'   => $date->endOfWeek()->toDateString(),
			];
		}

		if ($viewType === 'month') {
			$result = [
				'from' => $date->startOfMonth()->toDateString(),
				'to'   => $date->endOfMonth()->toDateString(),
			];
		}

		return $result;
	}
}