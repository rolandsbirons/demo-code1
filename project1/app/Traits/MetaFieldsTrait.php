<?php

namespace App\Traits;

use App\Enums\InvoiceTypes;
use App\Models\Branch\Branch;
use App\Models\System\Insurance;
use App\Models\System\Profession;
use App\Models\User\Position;
use App\Models\User\User;
use Auth;
use Exception;
use Illuminate\Support\Collection;

trait MetaFieldsTrait
{
	public function getMetaUsersSystemGroup(Branch $branch): array
	{
		$groupObject = [
			'doctors'       => [
				'group_index' => 1,
				'users'       => [],
			],
			'assistant'     => [
				'group_index' => 2,
				'users'       => [],
			],
			'administrator' => [
				'group_index' => 3,
				'users'       => [],
			],
			'staff'         => [
				'group_index' => 0,
				'users'       => [],
			],
		];

		foreach ($branch->users->sortBy('full_name') as $branchUser) {
			$userPositions = $branchUser->positions->pluck('type')->unique();

			$userObject = [
				'uuid' => $branchUser->uuid,
				'name' => $branchUser->full_name,
			];

			if ($userPositions->contains(1) || ($userPositions->contains(1) && $userPositions->contains(0))) {
				//doctor OR doctor+owner

				$groupObject['doctors']['users'][] = $userObject;
			} else if ($userPositions->contains(2) || ($userPositions->contains(2) && $userPositions->contains(3))) {
				//assistant OR admin + assistant

				$groupObject['assistant']['users'][] = $userObject;
			} else if ($userPositions->contains(3)) {
				$groupObject['administrator']['users'][] = $userObject;
			} else if ($userPositions->contains(0)) {

				$groupObject['staff']['users'][] = $userObject;
			}
		}

		return $groupObject;
	}

	public function getUsersByType(array $userTypes = [], $branchId = null, $shortName = false): Collection
	{
		try {
			$users = User::whereHas('positions', static function ($q) use ($userTypes) {
				$q->whereIn('type', $userTypes);
			});

			if ($branchId) {
				$users->whereHas('branches', static function ($q) use ($branchId) {
					$q->where('id', $branchId);
				});
			}

			return $users->get()->transform(static function ($user) use ($shortName) {
				return [
					'uuid' => $user->uuid,
					'name' => $shortName ? $user->full_name_short : $user->full_name,
				];
			});
		} catch (Exception $e) {
			return collect([]);
		}
	}

	public function getUsersInBranch($branchId, array $userTypes = [1, 2, 3]): Collection
	{
		try {
			return $this->getUsersByType($userTypes, $branchId);
		} catch (Exception $e) {
			return collect([]);
		}
	}

	public function getMetaBranches()
	{
		return Branch::select('uuid', 'name', 'color', 'id')
			->with([
				'users' => static function ($q) {
					$q->select('id');
				},
			])
			->get()
			->transform(static function ($branch) {
				$userFound = $branch->users->filter(static function ($user) {
					return $user->id === Auth::id();
				})->isNotEmpty();

				return [
					'color'  => $branch->color,
					'name'   => $branch->name,
					'uuid'   => $branch->uuid,
					'active' => $userFound || !Auth::user()->deletable,
				];
			})->filter(static function ($branch) {
				return $branch['active'];
			})->values();
	}

	public function getMetaInsurance(): array
	{
		$insurance = Insurance::get(['id', 'name'])->sortBy('name')->transform(static function ($insurance) {
			return [
				'uuid' => $insurance->id,
				'name' => $insurance->name,
			];
		})->toArray();

		return array_merge([
			[
				'id'   => 0,
				'uuid' => 0,
				'name' => '---',
			],
		], $insurance);
	}

	public function getMetaPositions()
	{
		$position = Position::get(['uuid', 'name'])->transform(static function ($position) {
			return [
				'uuid' => $position->uuid,
				'text' => $position->name,
			];
		});

		return $position;
	}

	public function getMetaTaxes($countryCode = 'lv')
	{
		return InvoiceTypes::INVOICE_TAXES[$countryCode];
	}

	public function getMetaJobStatuses()
	{
		return Profession::select('name')
			->get()
			->sortBy('name')
			->pluck('name');
	}
}
