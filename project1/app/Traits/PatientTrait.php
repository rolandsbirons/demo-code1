<?php

namespace App\Traits;

use App\Models\Patient\Patient;
use App\Models\User\User;

trait PatientTrait
{
	public static function addHelperIfNotExists(Patient $patient, $data): void
	{
		if (is_array($data)) {
			foreach ($data as $user) {
				self::processHelperIfDoesNotExist($patient, $user);
			}
		} else {
			self::processHelperIfDoesNotExist($patient, $data);
		}
	}

	private static function processHelperIfDoesNotExist(Patient $patient, int $userId): void
	{
		$users = $patient->users()
			->pluck('id');

		if (!$users->contains($userId)) {
			$patient->users()->attach((array)$userId, ['type' => 2]);
		}
	}

	public static function attachPatientUsers(Patient $patient, $request): void
	{
		$doctorListUnique = collect($request->get('assistants', []))
			->unique()
			->filter(static function ($value) use ($request) {
				return $value !== $request->get('doctor');
			});

		$doctorList = User::select('id')->whereIn('uuid', $doctorListUnique)
			->get()
			->pluck('id')
			->toArray();

		$doctorObject = array_fill(0, \count($doctorList), ['type' => 2]);
		$doctorSyncObject = array_combine($doctorList, $doctorObject);

		$patient->users()->sync($doctorSyncObject);

		$mainDoctor = User::select('id')->whereUuid($request->get('doctor'))->firstOrFail();
		$patient->users()->attach((array)$mainDoctor->id);
	}

}