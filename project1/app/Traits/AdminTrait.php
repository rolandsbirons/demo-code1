<?php

namespace App\Traits;

use App\Enums\AdminEnums;

trait AdminTrait
{
	public function hasAdminDebug(): bool
	{
		$adminIps = config('admin.ip_list');
		$adminIPList = explode(',', $adminIps);

		$ip = request()->ip();

		return in_array($ip, $adminIPList, true);
	}

	public function isSystemAdmin(): bool
	{
		return auth()->check() && in_array(auth()->user()->id, AdminEnums::ADMIN_ID_LIST, true);
	}
}