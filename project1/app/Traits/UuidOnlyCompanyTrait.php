<?php

namespace App\Traits;

use App\Scopes\CompanyScope;
use Illuminate\Support\Str;

trait UuidOnlyCompanyTrait
{
	protected static function boot(): void
	{
		parent::boot();

		static::addGlobalScope(new CompanyScope);

		static::creating(static function ($model) {
			$model->uuid = (string)Str::uuid();
		});
	}
}