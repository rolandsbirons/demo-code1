<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Collection;

trait SearchTrait
{
	public function getUniversalSearchResults($eloquentModel, array $allowedSearchStrings): Collection
	{
		try {
			$whereFields = $this->getSearchFiltered($allowedSearchStrings);
			$fieldCount = \count($whereFields);

			if ($fieldCount > 0) {
				if ($fieldCount === 1) {
					$eloquentModel->where($whereFields);
				}

				if ($fieldCount > 1) {
					$whereFirst = $whereFields[0];
					unset($whereFields[0]);

					$eloquentModel->where($whereFirst[0], $whereFirst[1], $whereFirst[2])
						->orWhere($whereFields);
				}
			}

			return $eloquentModel->get();
		} catch (Exception $e) {
			return collect([]);
		}
	}

	private function getSearchFiltered(array $allowedSearchStrings): array
	{
		$searchFields = [];
		$requestInputStrings = request('search');

		if (!empty($requestInputStrings)) {
			parse_str($requestInputStrings, $explodedSearchInputs);

			$allowedSearchKeys = array_keys($allowedSearchStrings);
			$filteredSearchCollection = collect($explodedSearchInputs)->only($allowedSearchKeys);

			if ($filteredSearchCollection->count() > 0) {
				foreach ($filteredSearchCollection as $filteredIndex => $filteredValue) {
					$fieldNameDatabase = $allowedSearchStrings[$filteredIndex];

					$searchFields[] = [
						$fieldNameDatabase,
						'like',
						'%' . $filteredValue . '%',
					];
				}
			}
		}

		return $searchFields;
	}
}