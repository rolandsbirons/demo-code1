<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UuidOnlyTrait
{
	protected static function boot(): void
	{
		parent::boot();

		static::creating(static function ($model) {
			$model->uuid = (string)Str::uuid();
		});
	}
}